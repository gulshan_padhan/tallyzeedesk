package com.java.tallyzee.Database;

import com.java.tallyzee.LoginCntr.UserInfo;
import org.apache.derby.iapi.db.Database;

import java.sql.*;

public class UserConnection {

static Connection connection=DatabaseConfig.getConnection();
    private String name;
    private String email;
    private String phone;
    private String password;
    private String confirmpassword;
    private String createdAt;
    private String createdBy;
    private String userID;
    private String updatedAt;
    private String updatedBy;
    private String accountType;
    private String suscriptionDate;
    private String profileUri;
    private boolean isLogin;
    private String userType;
UserInfo userInfo;
    public static void createTable(Connection connection)
    {
        try{
            String sql1 = "CREATE TABLE IF NOT EXISTS CrUser (\n"
                    + "	id integer PRIMARY KEY,\n"
                    + "	name text,\n"
                    + "	email text NOT NULL,\n"
                    + "	phone text,\n"
                    + "	password text,\n"
                    + "confirmpassword text,\n"
                    + "	createdAt text,\n"
                    + "	createdBy text,\n"
                    + "	userID text,\n"
                    + "	updatedAt text,\n"
                    + "	updatedBy text,\n"
                    + "	accountType text,\n"
                    + "	suscriptionDate text,\n"
                    + "	profileUri text,\n"
                    + "	isLogin text,\n"
                    + "	userType text\n"
                    + ");";

            try (Connection conn = connection;
                 Statement stmt = conn.createStatement()) {
                // create a new table
                stmt.execute(sql1);
                System.out.println(stmt);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }catch (Exception e)
        {

        }
    }

    public static void insert(UserInfo userInfo,Connection connection) {
        String sql = "INSERT INTO CrUser(name,\n" +
                " email,\n" +
                "  phone,\n" +
                "  password,\n" +
                "  confirmpassword,\n" +
                "  createdAt,\n" +
                "  createdBy,\n" +
                "  userID,\n" +
                "  updatedAt,\n" +
                "  updatedBy,\n" +
                "  accountType,\n" +
                "  suscriptionDate,\n" +
                "  profileUri,\n" +
                "   isLogin,\n" +
                "  userType) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";



        try (Connection conn = connection;
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, userInfo.getName());
            pstmt.setString(2, userInfo.getEmail());
            pstmt.setString(3, userInfo.getPhone());
            pstmt.setString(4, userInfo.getPassword());
            pstmt.setString(5, userInfo.getConfirmpassword());
            pstmt.setString(6, userInfo.getCreatedAt());
            pstmt.setString(7, userInfo.getCreatedBy());
            pstmt.setString(8, userInfo.getUserID());
            pstmt.setString(9, userInfo.getUpdatedAt());
            pstmt.setString(10, userInfo.getUpdatedBy());
            pstmt.setString(11, userInfo.getAccountType());
            pstmt.setString(12, userInfo.getSuscriptionDate());
            pstmt.setString(13, userInfo.getProfileUri());
            pstmt.setString(14, String.valueOf(userInfo.isLogin()));
            pstmt.setString(15, userInfo.getUserType());

//            name;
//            email;
//            phone;
//            password;
//            confirmpassword;
//            createdAt;
//            createdBy;
//            userID;
//            updatedAt;
//            updatedBy;
//            accountType;
//            suscriptionDate;
//            profileUri;
//            isLogin;
//            userType;
            pstmt.executeUpdate();
            System.out.println("update");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public static void update(UserInfo userInfo) {
        String sql = "INSERT INTO CrUser(name,\n" +
                " email,\n" +
                "  phone,\n" +
                "  password,\n" +
                "  confirmpassword,\n" +
                "  createdAt,\n" +
                "  createdBy,\n" +
                "  userID,\n" +
                "  updatedAt,\n" +
                "  updatedBy,\n" +
                "  accountType,\n" +
                "  suscriptionDate,\n" +
                "  profileUri,\n" +
                "   isLogin,\n" +
                "  userType) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


        try (Connection conn = connection;
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, userInfo.getName());
            pstmt.setString(2, userInfo.getEmail());
            pstmt.setString(3, userInfo.getPhone());
            pstmt.setString(4, userInfo.getPassword());
            pstmt.setString(5, userInfo.getConfirmpassword());
            pstmt.setString(6, userInfo.getCreatedAt());
            pstmt.setString(7, userInfo.getCreatedBy());
            pstmt.setString(8, userInfo.getUserID());
            pstmt.setString(9, userInfo.getUpdatedAt());
            pstmt.setString(10, userInfo.getUpdatedBy());
            pstmt.setString(11, userInfo.getAccountType());
            pstmt.setString(12, userInfo.getSuscriptionDate());
            pstmt.setString(13, userInfo.getProfileUri());
            pstmt.setString(14, String.valueOf(userInfo.isLogin()));
            pstmt.setString(15, userInfo.getUserType());

            pstmt.executeUpdate();
            System.out.println("update");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public static UserInfo selectAll(Connection connection){
        UserInfo userInfo=null;
        String sql = "SELECT name,\n" +
                " email,\n" +
                "  phone,\n" +
                "  password,\n" +
                "  confirmpassword,\n" +
                "  createdAt,\n" +
                "  createdBy,\n" +
                "  userID,\n" +
                "  updatedAt,\n" +
                "  updatedBy,\n" +
                "  accountType,\n" +
                "  suscriptionDate,\n" +
                "  profileUri,\n" +
                "  isLogin,\n" +
                "  userType FROM CrUser";

        try (Connection conn = connection;
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

            // loop through the result set
            while (rs.next()) {
                userInfo=new UserInfo(rs.getString("name"), rs.getString("email"), rs.getString("phone"), rs.getString("password"), rs.getString("confirmpassword"), rs.getString("createdAt"), rs.getString("createdBy"), rs.getString("userID"), rs.getString("updatedAt"), rs.getString("updatedBy"), rs.getString("accountType"), rs.getString("suscriptionDate"), rs.getString("profileUri"), Boolean.valueOf(rs.getString("isLogin")), rs.getString("userType"));
                System.out.println(
                        rs.getString("name") + "\t"
                       );
            }
            System.out.println(" ok2update");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return userInfo;
    }
    public static UserInfo getCurrentUser(Connection conn){
    connection=conn;
//createTable(conn);
        return  selectAll(conn);
    }

    public static void dropTable(Connection connection)
    {
        UserInfo userInfo=null;
        String sql = "DROP TABLE CrUser";

        try (Connection conn = connection;
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){


            System.out.println(" ok2update");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
