package com.java.tallyzee.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConfig {
    public static Connection getConnection()
    {
        Connection conn = null;
        Connection con = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:/home/gulshan/Downloads/chinook.db";
            // create a connection to the database
            con = DriverManager.getConnection("jdbc:sqlite::memory:");
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
}
