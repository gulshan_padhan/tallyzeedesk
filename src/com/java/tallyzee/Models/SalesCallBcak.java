package com.java.tallyzee.Models;

import java.util.ArrayList;

public interface SalesCallBcak {
    void getSalesResponse(ArrayList<SalesVoucher> list);
    void getRecieptResponse(ArrayList<PaymentVoucher> list);
    void getPurchaseResponse(ArrayList<SalesVoucher> list);
    void getPurchaseOrderResponse(ArrayList<SalesVoucher> list);
    void getSalesOrderResponse(ArrayList<SalesOrderVoucher> list);
    void getDebitNoteResponse(ArrayList<SalesVoucher> list);
    void getCreditNoteResponse(ArrayList<SalesVoucher> list);
    void getPaymentResponse(ArrayList<PaymentVoucher> list);
    void getStockResponse(ArrayList<StockItem> list);
    void getContraResponse(ArrayList<ContraVoucher> list);
}
