package com.java.tallyzee.Models;

public class ProfinAndLoss {
    public String ledgerName;
    public String ledgerNameParent;
    public String ledgerAmount;

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getLedgerNameParent() {
        return ledgerNameParent;
    }

    public void setLedgerNameParent(String ledgerNameParent) {
        this.ledgerNameParent = ledgerNameParent;
    }

    public String getLedgerAmount() {
        return ledgerAmount;
    }

    public void setLedgerAmount(String ledgerAmount) {
        this.ledgerAmount = ledgerAmount;
    }

    public String getLedgerNameGroup() {
        return ledgerNameGroup;
    }

    public void setLedgerNameGroup(String ledgerNameGroup) {
        this.ledgerNameGroup = ledgerNameGroup;
    }
public ProfinAndLoss()
{

}
    public ProfinAndLoss(String ledgerName, String ledgerNameParent, String ledgerAmount, String ledgerNameGroup) {
        this.ledgerName = ledgerName;
        this.ledgerNameParent = ledgerNameParent;
        this.ledgerAmount = ledgerAmount;
        this.ledgerNameGroup = ledgerNameGroup;
    }

    public String ledgerNameGroup;
}
