package com.java.tallyzee.Models;

import java.io.Serializable;

public class GstRates implements Serializable {
    String GSTRATEPERUNIT;

    public String getGSTRATEPERUNIT() {
        return GSTRATEPERUNIT;
    }

    public void setGSTRATEPERUNIT(String GSTRATEPERUNIT) {
        this.GSTRATEPERUNIT = GSTRATEPERUNIT;
    }

    public String getGSTRATEDUTYHEAD() {
        return GSTRATEDUTYHEAD;
    }

    public void setGSTRATEDUTYHEAD(String GSTRATEDUTYHEAD) {
        this.GSTRATEDUTYHEAD = GSTRATEDUTYHEAD;
    }

    public String getGSTRATEVALUATIONTYPE() {
        return GSTRATEVALUATIONTYPE;
    }

    public void setGSTRATEVALUATIONTYPE(String GSTRATEVALUATIONTYPE) {
        this.GSTRATEVALUATIONTYPE = GSTRATEVALUATIONTYPE;
    }

    public String getGSTRAT() {
        return GSTRAT;
    }

    public void setGSTRAT(String GSTRAT) {
        this.GSTRAT = GSTRAT;
    }

    public GstRates(String GSTRATEPERUNIT, String GSTRATEDUTYHEAD, String GSTRATEVALUATIONTYPE, String GSTRAT) {
        this.GSTRATEPERUNIT = GSTRATEPERUNIT;
        this.GSTRATEDUTYHEAD = GSTRATEDUTYHEAD;
        this.GSTRATEVALUATIONTYPE = GSTRATEVALUATIONTYPE;
        this.GSTRAT = GSTRAT;
    }

    String GSTRATEDUTYHEAD;
    String GSTRATEVALUATIONTYPE;
   String GSTRAT;
}
