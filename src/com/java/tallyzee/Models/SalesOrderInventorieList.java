package com.java.tallyzee.Models;

import java.io.Serializable;
import java.util.List;

public class SalesOrderInventorieList implements Serializable {
    String stockItemName;
    String basicUserDesc;
    String rate;

    public String getStockItemName() {
        return stockItemName;
    }

    public void setStockItemName(String stockItemName) {
        this.stockItemName = stockItemName;
    }

    public String getBasicUserDesc() {
        return basicUserDesc;
    }

    public void setBasicUserDesc(String basicUserDesc) {
        this.basicUserDesc = basicUserDesc;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getEdqty() {
        return edqty;
    }

    public void setEdqty(String edqty) {
        this.edqty = edqty;
    }

    public String getActualQty() {
        return actualQty;
    }

    public void setActualQty(String actualQty) {
        this.actualQty = actualQty;
    }

    public List<SalesOrderBatchAlloction> getBatchAllocationList() {
        return batchAllocationList;
    }

    public void setBatchAllocationList(List<SalesOrderBatchAlloction> batchAllocationList) {
        this.batchAllocationList = batchAllocationList;
    }

    public List<AccountAllocation> getAccountAllocations() {
        return accountAllocations;
    }

    public void setAccountAllocations(List<AccountAllocation> accountAllocations) {
        this.accountAllocations = accountAllocations;
    }



    String amount;
    String edqty;
    String actualQty;
    List<SalesOrderBatchAlloction> batchAllocationList;
    List<AccountAllocation> accountAllocations;

    public SalesOrderInventorieList(String stockItemName, String basicUserDesc, String rate, String amount, String edqty, String actualQty, List<SalesOrderBatchAlloction> batchAllocationList, List<AccountAllocation> accountAllocations) {
        this.stockItemName = stockItemName;
        this.basicUserDesc = basicUserDesc;
        this.rate = rate;
        this.amount = amount;
        this.edqty = edqty;
        this.actualQty = actualQty;
        this.batchAllocationList = batchAllocationList;
        this.accountAllocations = accountAllocations;
    }
}
