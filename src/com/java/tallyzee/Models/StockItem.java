package com.java.tallyzee.Models;

import java.io.Serializable;
import java.util.List;

public class StockItem implements Serializable {
    public String getStockItemName() {
        return stockItemName;
    }

    public void setStockItemName(String stockItemName) {
        this.stockItemName = stockItemName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(String closingBalance) {
        this.closingBalance = closingBalance;
    }

    public String getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(String openingBalance) {
        this.openingBalance = openingBalance;
    }

    public String getOpeningValue() {
        return openingValue;
    }

    public void setOpeningValue(String openingValue) {
        this.openingValue = openingValue;
    }

    public String getReorderValue() {
        return reorderValue;
    }

    public void setReorderValue(String reorderValue) {
        this.reorderValue = reorderValue;
    }

    public String getMinimumOrderBase() {
        return minimumOrderBase;
    }

    public void setMinimumOrderBase(String minimumOrderBase) {
        this.minimumOrderBase = minimumOrderBase;
    }

    public String getOpeningRate() {
        return openingRate;
    }

    public void setOpeningRate(String openingRate) {
        this.openingRate = openingRate;
    }
public StockItem()
{

}
    public StockItem(String stockItemName, String parentName, String closingBalance, String openingBalance, String openingValue, String reorderValue, String minimumOrderBase, String openingRate) {
        this.stockItemName = stockItemName;
        this.parentName = parentName;
        this.closingBalance = closingBalance;
        this.openingBalance = openingBalance;
        this.openingValue = openingValue;
        this.reorderValue = reorderValue;
        this.minimumOrderBase = minimumOrderBase;
        this.openingRate = openingRate;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getHsnCode() {
        return HsnCode;
    }

    public void setHsnCode(String hsnCode) {
        HsnCode = hsnCode;
    }

    public List<GstRates> getGstRatesList() {
        return gstRatesList;
    }

    public void setGstRatesList(List<GstRates> gstRatesList) {
        this.gstRatesList = gstRatesList;
    }



    String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public StockItem(String type, String date, String rate, String hsnCode, String stockItemName, String parentName, String closingBalance, String openingBalance, String openingValue, String reorderValue, String minimumOrderBase, String openingRate, List<GstRates> gstRatesList) {
        this.type = type;
        this.date = date;
        this.rate = rate;
        HsnCode = hsnCode;
        this.stockItemName = stockItemName;
        this.parentName = parentName;
        this.closingBalance = closingBalance;
        this.openingBalance = openingBalance;
        this.openingValue = openingValue;
        this.reorderValue = reorderValue;
        this.minimumOrderBase = minimumOrderBase;
        this.openingRate = openingRate;
        this.gstRatesList = gstRatesList;
    }

    String date;
    String rate;
    String HsnCode;
    String stockItemName;
    String parentName;
    String closingBalance;
    String openingBalance;
    String openingValue;
    String reorderValue;
    String minimumOrderBase;
    String openingRate;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public StockItem(String type, String date, String rate, String hsnCode, String stockItemName, String parentName, String closingBalance, String openingBalance, String openingValue, String reorderValue, String minimumOrderBase, String openingRate, String category, List<GstRates> gstRatesList) {
        this.type = type;
        this.date = date;
        this.rate = rate;
        HsnCode = hsnCode;
        this.stockItemName = stockItemName;
        this.parentName = parentName;
        this.closingBalance = closingBalance;
        this.openingBalance = openingBalance;
        this.openingValue = openingValue;
        this.reorderValue = reorderValue;
        this.minimumOrderBase = minimumOrderBase;
        this.openingRate = openingRate;
        this.category = category;
        this.gstRatesList = gstRatesList;
    }

    String category;
    List<GstRates> gstRatesList;

    public String toString()
    {
        //date+" "+ rate+" "+  hsnCode+" "+ stockItemName+" "+parentName+" "+ closingBalance+" "+openingBalance+" "+ openingValue+" "+ reorderValue+" "+ minimumOrderBase+" "+ openingRate+" "+ gstRatesList
        return date+" "+ rate+" "+  HsnCode+" "+ stockItemName+" "+parentName+" "+ closingBalance+" "+openingBalance+" "+ openingValue+" "+ reorderValue+" "+ minimumOrderBase+" "+ openingRate+" "+ gstRatesList;
    }

}
