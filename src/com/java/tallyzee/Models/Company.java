package com.java.tallyzee.Models;



import java.io.Serializable;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class Company implements Serializable{

    int id;
    String companyDate;
    String name;
public Company()
{

}
    public Company(int id,String companyDate, String name, String address, String stateName, String userId, String cuntryName, String phoneNumber, String mobileNumber, String email, String cin, String pan, String gstn, List<SalesVoucher> salelist, List<PaymentVoucher> paymentList, List<SalesVoucher> purchaseOrderVouchersList, List<SalesOrderVoucher> salesOrderVouchersList, List<SalesVoucher> creditNoteVouchersList, List<SalesVoucher> debitNoteVouchersList, List<StockItem> soItemsList, List<PaymentVoucher> receiptVouchersList) {
        this.id=id;
        this.companyDate = companyDate;
        this.name = name;
        this.address = address;
        this.stateName = stateName;
        this.userId = userId;
        this.cuntryName = cuntryName;
        this.phoneNumber = phoneNumber;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.cin = cin;
        this.pan = pan;
        this.gstn = gstn;
        Salelist = salelist;
        this.paymentList = paymentList;
        this.purchaseOrderVouchersList = purchaseOrderVouchersList;
        this.salesOrderVouchersList = salesOrderVouchersList;
        this.creditNoteVouchersList = creditNoteVouchersList;
        this.debitNoteVouchersList = debitNoteVouchersList;
        this.soItemsList = soItemsList;
        this.receiptVouchersList = receiptVouchersList;
    }

    String address;
    String stateName;
    String userId;
    String cuntryName;
    String phoneNumber;
    String mobileNumber;
    String email;
    private static AtomicInteger ID_GENERATOR = new AtomicInteger(1000);


        // id=ID_GENERATOR.getAndIncrement();

    String cin;
    String pan;
    String gstn;

    List<SalesVoucher> Salelist;
    List<PaymentVoucher> paymentList;
    List<SalesVoucher> purchaseOrderVouchersList;
    List<SalesOrderVoucher> salesOrderVouchersList;
    List<SalesVoucher> creditNoteVouchersList;
    List<SalesVoucher> debitNoteVouchersList;
    List<StockItem> soItemsList;
    List<PaymentVoucher> receiptVouchersList;
List<SalesVoucher> purchaseVoucher;

    public List<SalesVoucher> getPurchaseVoucher() {
        return purchaseVoucher;
    }

    public void setPurchaseVoucher(List<SalesVoucher> purchaseVoucher) {
        this.purchaseVoucher = purchaseVoucher;
    }

    public List<ContraVoucher> getContraList() {
        return contraList;
    }

    public void setContraList(List<ContraVoucher> contraList) {
        this.contraList = contraList;
    }

    public Company(int id, String companyDate, String name, String address, String stateName, String userId, String cuntryName, String phoneNumber, String mobileNumber, String email, String cin, String pan, String gstn, List<SalesVoucher> salelist, List<PaymentVoucher> paymentList, List<SalesVoucher> purchaseOrderVouchersList, List<SalesOrderVoucher> salesOrderVouchersList, List<SalesVoucher> creditNoteVouchersList, List<SalesVoucher> debitNoteVouchersList, List<StockItem> soItemsList, List<PaymentVoucher> receiptVouchersList, List<SalesVoucher> purchaseVouchers, List<ContraVoucher> contraVouchers) {
        this.id = id;
        this.companyDate = companyDate;
        this.name = name;
        this.address = address;
        this.stateName = stateName;
        this.userId = userId;
        this.cuntryName = cuntryName;
        this.phoneNumber = phoneNumber;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.cin = cin;
        this.pan = pan;
        this.gstn = gstn;
        Salelist = salelist;
        this.paymentList = paymentList;
        this.purchaseOrderVouchersList = purchaseOrderVouchersList;
        this.salesOrderVouchersList = salesOrderVouchersList;
        this.creditNoteVouchersList = creditNoteVouchersList;
        this.debitNoteVouchersList = debitNoteVouchersList;
        this.soItemsList = soItemsList;
        this.receiptVouchersList = receiptVouchersList;
        this.purchaseVoucher = purchaseVouchers;
        this.contraList = contraVouchers;
    }

    List<ContraVoucher> contraList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyDate() {
        return companyDate;
    }

    public void setCompanyDate(String companyDate) {
        this.companyDate = companyDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCuntryName() {
        return cuntryName;
    }

    public void setCuntryName(String cuntryName) {
        this.cuntryName = cuntryName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static AtomicInteger getIdGenerator() {
        return ID_GENERATOR;
    }

    public static void setIdGenerator(AtomicInteger idGenerator) {
        ID_GENERATOR = idGenerator;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getGstn() {
        return gstn;
    }

    public void setGstn(String gstn) {
        this.gstn = gstn;
    }

    public List<SalesVoucher> getSalelist() {
        return Salelist;
    }

    public void setSalelist(List<SalesVoucher> salelist) {
        Salelist = salelist;
    }

    public List<PaymentVoucher> getPaymentList() {
        return paymentList;
    }

    public void setPaymentList(List<PaymentVoucher> paymentList) {
        this.paymentList = paymentList;
    }

    public List<SalesVoucher> getPurchaseOrderVouchersList() {
        return purchaseOrderVouchersList;
    }

    public void setPurchaseOrderVouchersList(List<SalesVoucher> purchaseOrderVouchersList) {
        this.purchaseOrderVouchersList = purchaseOrderVouchersList;
    }

    public List<SalesOrderVoucher> getSalesOrderVouchersList() {
        return salesOrderVouchersList;
    }

    public void setSalesOrderVouchersList(List<SalesOrderVoucher> salesOrderVouchersList) {
        this.salesOrderVouchersList = salesOrderVouchersList;
    }

    public List<SalesVoucher> getCreditNoteVouchersList() {
        return creditNoteVouchersList;
    }

    public void setCreditNoteVouchersList(List<SalesVoucher> creditNoteVouchersList) {
        this.creditNoteVouchersList = creditNoteVouchersList;
    }

    public List<SalesVoucher> getDebitNoteVouchersList() {
        return debitNoteVouchersList;
    }

    public void setDebitNoteVouchersList(List<SalesVoucher> debitNoteVouchersList) {
        this.debitNoteVouchersList = debitNoteVouchersList;
    }

    public List<StockItem> getSoItemsList() {
        return soItemsList;
    }

    public void setSoItemsList(List<StockItem> soItemsList) {
        this.soItemsList = soItemsList;
    }

    public List<PaymentVoucher> getReceiptVouchersList() {
        return receiptVouchersList;
    }

    public void setReceiptVouchersList(List<PaymentVoucher> receiptVouchersList) {
        this.receiptVouchersList = receiptVouchersList;
    }
}
