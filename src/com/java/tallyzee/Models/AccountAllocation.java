package com.java.tallyzee.Models;

import java.io.Serializable;
import java.util.List;

public class AccountAllocation implements Serializable {
    String ledgerName;
    String amount;
    List<BankItem> banklist;

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public List<BankItem> getBanklist() {
        return banklist;
    }

    public void setBanklist(List<BankItem> banklist) {
        this.banklist = banklist;
    }

    public List<Item> getBillItem() {
        return billItem;
    }

    public void setBillItem(List<Item> billItem) {
        this.billItem = billItem;
    }

    List<Item> billItem;

    public AccountAllocation(String ledgerName, String amount, List<BankItem> banklist, List<Item> billItem) {
        this.ledgerName = ledgerName;
        this.amount = amount;
        this.banklist = banklist;
        this.billItem = billItem;
    }
}
