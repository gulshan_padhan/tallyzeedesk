package com.java.tallyzee.Models;

import java.io.Serializable;

public class StockGroup implements Serializable {
    String stockItemName;
    String parentName;
    String closingBalance;
    String openingBalance;
    String openingValue;
    String reorderValue;
    String minimumOrderBase;
    String openingRate;

    public String getStockItemName() {
        return stockItemName;
    }

    public void setStockItemName(String stockItemName) {
        this.stockItemName = stockItemName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(String closingBalance) {
        this.closingBalance = closingBalance;
    }

    public String getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(String openingBalance) {
        this.openingBalance = openingBalance;
    }

    public String getOpeningValue() {
        return openingValue;
    }

    public void setOpeningValue(String openingValue) {
        this.openingValue = openingValue;
    }

    public String getReorderValue() {
        return reorderValue;
    }

    public void setReorderValue(String reorderValue) {
        this.reorderValue = reorderValue;
    }

    public String getMinimumOrderBase() {
        return minimumOrderBase;
    }

    public void setMinimumOrderBase(String minimumOrderBase) {
        this.minimumOrderBase = minimumOrderBase;
    }

    public String getOpeningRate() {
        return openingRate;
    }

    public void setOpeningRate(String openingRate) {
        this.openingRate = openingRate;
    }

    public StockGroup(String stockItemName, String parentName, String closingBalance, String openingBalance, String openingValue, String reorderValue, String minimumOrderBase, String openingRate) {
        this.stockItemName = stockItemName;
        this.parentName = parentName;
        this.closingBalance = closingBalance;
        this.openingBalance = openingBalance;
        this.openingValue = openingValue;
        this.reorderValue = reorderValue;
        this.minimumOrderBase = minimumOrderBase;
        this.openingRate = openingRate;
    }
}
