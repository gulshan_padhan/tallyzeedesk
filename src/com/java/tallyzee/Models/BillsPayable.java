package com.java.tallyzee.Models;

import java.io.Serializable;

public class BillsPayable implements Serializable {
    String billDate;

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillRef() {
        return billRef;
    }

    public void setBillRef(String billRef) {
        this.billRef = billRef;
    }

    public String getBillParty() {
        return billParty;
    }

    public void setBillParty(String billParty) {
        this.billParty = billParty;
    }

    public String getBillCl() {
        return billCl;
    }

    public void setBillCl(String billCl) {
        this.billCl = billCl;
    }

    public String getBillDue() {
        return billDue;
    }

    public void setBillDue(String billDue) {
        this.billDue = billDue;
    }

    public String getBillOverDue() {
        return billOverDue;
    }

    public void setBillOverDue(String billOverDue) {
        this.billOverDue = billOverDue;
    }

    public BillsPayable(String billDate, String billRef, String billParty, String billCl, String billDue, String billOverDue) {
        this.billDate = billDate;
        this.billRef = billRef;
        this.billParty = billParty;
        this.billCl = billCl;
        this.billDue = billDue;
        this.billOverDue = billOverDue;
    }

    String billRef;
    String billParty;
    String billCl;
    String billDue;
    String billOverDue;
}
