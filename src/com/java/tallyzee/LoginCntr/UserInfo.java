package com.java.tallyzee.LoginCntr;


public class UserInfo {


    public String getUsername() {
        return name;
    }

    public void setUsername(String name) {
        this.name = name;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getSuscriptionDate() {
        return suscriptionDate;
    }

    public void setSuscriptionDate(String suscriptionDate) {
        this.suscriptionDate = suscriptionDate;
    }

    public UserInfo(String username, String email, String phone, String password, String confirmpassword, String createdAt, String createdBy, String updatedAt, String updatedBy, String accountType, String suscriptionDate) {
        this.name = username;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.confirmpassword = confirmpassword;
        this.createdAt = createdAt;
        this.createdBy = createdBy;

        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
        this.accountType = accountType;
        this.suscriptionDate = suscriptionDate;
    }
    private String name;
    private String email;
    private String phone;
    private String password;
    private String confirmpassword;
    private String createdAt;
    private String createdBy;
    private String userID;
    private String updatedAt;
    private String updatedBy;
    private String accountType;
    private String suscriptionDate;
    private String profileUri;
    private boolean isLogin;
    private String userType;


    public UserInfo(String name, String email, String phone, String password, String confirmpassword, String createdAt, String createdBy, String userID, String updatedAt, String updatedBy, String accountType, String suscriptionDate, String profileUri, boolean isLogin, String userType) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.confirmpassword = confirmpassword;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.userID = userID;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
        this.accountType = accountType;
        this.suscriptionDate = suscriptionDate;
        this.profileUri = profileUri;
        this.isLogin = isLogin;
        this.userType = userType;
    }

    public String getProfileUri() {
        return profileUri;
    }

    public void setProfileUri(String profileUri) {
        this.profileUri = profileUri;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }


    public UserInfo(String name, String email, String phone, String password, String confirmpassword, String createdAt, String createdBy, String updatedAt, String updatedBy, String accountType, String suscriptionDate, boolean isLogin, String userType) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.confirmpassword = confirmpassword;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
        this.accountType = accountType;
        this.suscriptionDate = suscriptionDate;
        this.isLogin = isLogin;
        this.userType = userType;
    }


    public UserInfo()
    {

    }

    public UserInfo(String name, String email, String phone, String password, String confirmpassword) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.confirmpassword = confirmpassword;
    }

    public UserInfo(String username,String password)
    {
        this.name=username;
        this.password=password;
    }


    public String getName() {
        return name;
    }

    public void setName(String username) {
        this.name = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmpassword() {
        return confirmpassword;
    }

    public void setConfirmpassword(String confirmpassword) {
        this.confirmpassword = confirmpassword;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
