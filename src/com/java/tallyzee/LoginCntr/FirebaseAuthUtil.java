package com.java.tallyzee.LoginCntr;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.*;
import com.java.tallyzee.DashBoard.Tabs.Profile.profileInterface;
import com.java.tallyzee.Database.DatabaseConfig;
import com.java.tallyzee.Database.UserConnection;
import com.java.tallyzee.Network.NetworkCall;
import javafx.application.Platform;
import javafx.concurrent.Task;
import com.java.tallyzee.DashBoard.Tabs.Profile.ProfileTab;
import com.java.tallyzee.Network.ResponseType;
import com.java.tallyzee.Session.Session;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

import java.io.FileInputStream;
import java.util.ArrayList;

public class FirebaseAuthUtil extends Task<String> {

FirebaseAuthUtils firebaseAuthUtils;
profileInterface profileInterface;
    private static String response = null;
    static String emailId;
    static boolean entry;
    static FirebaseApp inistance;
    private UserInfo userInfo;

    FirebaseAuthUtil()
    {

    }

    public  static  FirebaseAuthUtil getObject()
    {
        return  new FirebaseAuthUtil();
    }
    public static FirebaseApp getInstance() {
        return inistance;
    }
    public  ResponseType getInisilized() {
        ResponseType responseType=null;
        try {
//            ClassLoader classLoader = getObject().getClass().getClassLoader();
// File fi = new File(String.valueOf(FirebaseAuthUtil.class.getResourceAsStream("C:\\TallyZeeAppliCationResource\\admin-sdk.json")));
//ResourceController resourceController=new ResourceController();
//            URL resource = getClass().getResource("/src/com/java/tallyzee/LoginCntr/admin-sdk.json");
//            String path=Paths.get(resource.toURI()).toFile().getAbsolutePath().toString();
////String resourceFile="";
//try{
//    URL resource = ResourceController.class.getResource("admin-sdk.json");
//    resourceFile=Paths.get(resource.toURI()).toFile().getAbsolutePath().toString();
//}catch (Exception e)
//{res/Close.png
//
//}

            FileInputStream serviceAccount = new FileInputStream("C:\\TallyZeeAppliCationResource\\admin-sdk.json");
            // Initialize the app with a service account, granting admin privileges
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://tallyfy-364a0.firebaseio.com/")
                    .build();
            inistance = FirebaseApp.initializeApp(options);
            responseType= ResponseType.RESPONSEOK;
        } catch (Exception e)
        {
            responseType=  ResponseType.RESPONSENETWORKISSUE;
        }
       return responseType;
    }
    public FirebaseAuthUtil(ProfileTab context)
    {
        profileInterface=context;
    }
    public FirebaseAuthUtil(LoginController context, UserInfo userInfo) {
firebaseAuthUtils=context;this.userInfo=userInfo;
    }

    public void getAuthResponse(UserInfo userInfo){
        emailId = String.valueOf(userInfo.getName().hashCode());
        NetworkCall networkCall=new NetworkCall();
       UserInfo user= networkCall.getUserAuth(userInfo.getName(),userInfo.getPassword());
       UserConnection.createTable(DatabaseConfig.getConnection());
        UserConnection.insert(user,DatabaseConfig.getConnection());

       try {
           System.out.println("user name:"+user.getEmail());
           if(user.getEmail().equalsIgnoreCase(userInfo.getName()))
           {
               firebaseAuthUtils.getResponse(ResponseType.RESPONSEOK);
           }else
           {
               firebaseAuthUtils.getResponse(ResponseType.RESPONSENOTFOUND);
           }

       }catch(Exception e)
       {
firebaseAuthUtils.getResponse(ResponseType.NETWORKERROR);
       }


//         emailID = userInfo.getName().replaceAll("[._@]","%2");
//        try {
//            DatabaseReference db = FirebaseDatabase.getInstance().getReferenceFromUrl("https://tallyfy-364a0.firebaseio.com/Users/" + emailId);
//            db.addListenerForSingleValueEvent(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//
//                    Platform.runLater(new Runnable() {
//                        @Override public void run() {
//                            String resPassword=dataSnapshot.child("password").getValue(String.class);
//                            if(resPassword!=null)
//                            {
//                                if (resPassword.equals(userInfo.getPassword())) {
//                                    System.out.println("TRUE");
//                                    firebaseAuthUtils.getResponse(ResponseType.RESPONSEOK);
//                                } else {
//                                    System.out.println("FALSE");
//                                    firebaseAuthUtils.getResponse(ResponseType.RESPONSENOTFOUND);
//
//                                }
//                            }
//                            else {
//                                System.out.println("FALSE");
//                                firebaseAuthUtils.getResponse(ResponseType.RESPONSENOTFOUND);
//                            }
//
//                        }
//                    });
//                }
//                @Override
//                public void onCancelled(DatabaseError databaseError)
//                {
//                    Platform.runLater(new Runnable() {
//                        @Override public void run() {
//
//
//                            System.out.println("FALSE");
//                            firebaseAuthUtils.getResponse(ResponseType.RESPONSENETWORKISSUE);
//                            // loginController.getFalseResult();
//
//                        }
//                    });
//                    // System.out.println(databaseError.getMessage());
//                }
//            });
//        }catch (Exception e)
//        {
//            Platform.runLater(new Runnable() {
//                @Override public void run() {
//
//
//                    System.out.println("FALSE");
//                    firebaseAuthUtils.getResponse(ResponseType.RESPONSENETWORKISSUE);
//                    // loginController.getFalseResult();
//
//                }
//            });
//        }


    }
    public  void getData()
    {
        String getUserByEmail = LoginController.verifyEmail();
        ArrayList<String> arrayList = new ArrayList<>();
NetworkCall networkCall=new
        NetworkCall();
UserInfo user=UserConnection.getCurrentUser(DatabaseConfig.getConnection());
Session.setProfileUri(user.getProfileUri());
        String name =user.getName();
        String email = user.getEmail();
        String phone = user.getPhone();
        String userID = user.getUserID();
        arrayList.add(name);
        arrayList.add(email);
        arrayList.add(phone);
        arrayList.add(userID);
        arrayList.add(user.getProfileUri());
        profileInterface.getProfileResponse(arrayList);
//                DatabaseReference db = FirebaseDatabase.getInstance()
//                        .getReferenceFromUrl("https://tallyfy-364a0.firebaseio.com/Users/"+ Session.getEmail());
//                db.addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        Platform.runLater(new Runnable() {
//                            @Override
//                            public void run() {
//                                String name = dataSnapshot.child("name").getValue(String.class);
//                                String email = dataSnapshot.child("email").getValue(String.class);
//                                String phone = dataSnapshot.child("phone").getValue(String.class);
//                                String userID = dataSnapshot.child("userID").getValue(String.class);
//                                arrayList.add(name);
//                                arrayList.add(email);
//                                arrayList.add(phone);
//                                arrayList.add(userID);
//                                profileInterface.getProfileResponse(arrayList);
//                            }
//                        });
//
//
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                });





    }
    @Override
    protected String call() throws Exception {
        getAuthResponse(userInfo);
        return null;
    }
}
