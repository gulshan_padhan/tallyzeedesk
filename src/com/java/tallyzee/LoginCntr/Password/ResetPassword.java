package com.java.tallyzee.LoginCntr.Password;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.File;
import java.net.MalformedURLException;

public class ResetPassword {


    @FXML
    Label minimizeLabel;

    @FXML
    Label topLogo;

    @FXML
    Label closeLabel;

    @FXML
    ImageView backImageView;

    @FXML
    ImageView questionMark;

    public void initialize() throws MalformedURLException {

        //MINIMIZE IMAGE
        Image minimizeImage = new Image(new File("/resources/Minimize.png").toURL().toString());
        ImageView minimizeImageView = new ImageView(minimizeImage);
        minimizeImageView.setFitHeight(15);
        minimizeImageView.setFitWidth(15);
        minimizeLabel.setGraphic(minimizeImageView);



        //TOP LOGO
        Image logoImage = new Image(new File("/resources/stageLogo.png").toURL().toString());
        ImageView logoImageView = new ImageView(logoImage);
        logoImageView.setFitWidth(20);
        logoImageView.setFitHeight(20);
        topLogo.setGraphic(logoImageView);

        //CLOSE LOGO
        Image image1 = new Image(new File("/resources/Close.png").toURL().toString());
        ImageView imageView1 = new ImageView(image1);
        imageView1.setFitHeight(13);
        imageView1.setFitWidth(13);
        closeLabel.setGraphic(imageView1);


        //Back Button
        Image back_image = new Image(new File("/resources/backButton.png").toURL().toString());
        backImageView.setImage(back_image);

        //LOGO
        Image image = new Image(new File("/resources/passwrodreset.png").toURL().toString());
        questionMark.setImage(image);

    }

    @FXML
    public void minimizeStage(Event event)
    {
        ((Stage) ((Label)event.getSource()).getScene().getWindow()).setIconified(true);
    }


    @FXML
    public void closeStage(Event event)
    {
        ((Stage) ((Label) event.getSource()).getScene().getWindow()).close();
    }

}
