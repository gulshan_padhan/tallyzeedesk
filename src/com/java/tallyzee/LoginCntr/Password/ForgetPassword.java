package com.java.tallyzee.LoginCntr.Password;

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Random;

public class ForgetPassword {

    @FXML
    Label topLogo;

    @FXML
    ImageView backImageView;

    @FXML
    ImageView questionMark;

    @FXML
    TextField phoneNumber;

    @FXML
    Label minimizeLabel;

    @FXML
    Label closeLabel;

    @FXML
    Button SendOtp;


    public void initialize() throws MalformedURLException {

        //backButton
        Image image = new Image(new File("/resources/backButton.png").toURL().toString());
        backImageView.setImage(image);

        //Question Mark
        Image questionimage = new Image(new File("/resources/questionMark.png").toURL().toString());
        questionMark.setImage(questionimage);

        //TOP LOGO
        Image logoImage = new Image(new File("/resources/stageLogo.png").toURL().toString());
        ImageView logoImageView = new ImageView(logoImage);
        logoImageView.setFitWidth(20);
        logoImageView.setFitHeight(20);
        topLogo.setGraphic(logoImageView);

        //MINIMIZE LOGO
        Image image2 = new Image(new File("/resources/Minimize.png").toURL().toString());
        ImageView imageView = new ImageView(image2);
        imageView.setFitHeight(15);
        imageView.setFitWidth(15);
        minimizeLabel.setGraphic(imageView);

        //CLOSE LOGO
        Image image1 = new Image(new File("/resources/Close.png").toURL().toString());
        ImageView imageView1 = new ImageView(image1);
        imageView1.setFitHeight(13);
        imageView1.setFitWidth(13);
        closeLabel.setGraphic(imageView1);

        SendOtp.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                sendVerificationCode();
                try
                {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/src/com/java/tallyzee/LoginCntr/Password/resetPassword.fxml"));
                    Stage stage = (Stage) SendOtp.getScene().getWindow();
                    stage.close();
                    Parent root1 = (Parent) loader.load();
                    Stage stage1 = new Stage();
                    stage1.setScene(new Scene(root1,803,597));
                    stage1.setResizable(false);
                    stage1.setIconified(false);
                    stage1.initStyle(StageStyle.UNDECORATED);
                    stage1.show();
                }catch (Exception e)
                {
                System.out.println(e.getMessage());
                }
            }
        });

    }

    private void sendVerificationCode() {
        Random random = new Random();
        String number = "1234567890";
        int password[] = new int[6];
        for (int i = 0; i < 6; i++) {
            password[i] = random.nextInt(number.length());
        }
        System.out.println(Arrays.toString(password));
    }


    @FXML
    public void Minimize(Event event) {
        ((Stage) ((Label) event.getSource()).getScene().getWindow()).setIconified(true);
    }

    @FXML
    public void Close(Event event) {
        ((Stage) ((Label) event.getSource()).getScene().getWindow()).close();
    }


    public void sendSms() throws IOException {
        String phoneNumber = "+919867390561";
        String appKey = "711018753616528484";
        String appSecret = "your-app-secret";
        String message = "Hello, world!";

        URL url = new URL("https://messagingapi.sinch.com/v1/sms/" + phoneNumber);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");

        String userCredentials = "application\\" + appKey + ":" + appSecret;
        byte[] encoded = Base64.encodeBase64(userCredentials.getBytes());
        String basicAuth = "Basic " + new String(encoded);
        connection.setRequestProperty("Authorization", basicAuth);

        String postData = "{\"Message\":\"" + message + "\"}";
        OutputStream os = connection.getOutputStream();
        os.write(postData.getBytes());

        StringBuilder response = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        String line;
        while ((line = br.readLine()) != null)
            response.append(line);

        br.close();
        os.close();

        System.out.println(response.toString());


   }
}
