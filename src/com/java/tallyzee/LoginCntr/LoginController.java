package com.java.tallyzee.LoginCntr;


import com.jfoenix.controls.JFXSnackbar;
import javafx.concurrent.Service;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import com.java.tallyzee.LoginCntr.BottomNetworkStatus.NetworkStatus;
import com.java.tallyzee.Network.ResponseType;
import com.java.tallyzee.Session.Session;
import com.java.tallyzee.StageLoader.AccountStage;
import com.java.tallyzee.StageLoader.CreateStage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

public class LoginController implements FirebaseAuthUtils {

    @FXML
    Label Minimize;

    @FXML
    Label Close;

    @FXML
    Label createAccount;

    private Service<Void> bacgroundServices;

    @FXML
    Button signInButton;

    @FXML
    ImageView Logo;

    @FXML
    TextField userName;

    @FXML
    Button SignIn;

    @FXML
    Label Tally;

    @FXML
    Label logoHeader;
    @FXML
    Label Internet;

    @FXML
    ProgressIndicator progressIndicator = new ProgressIndicator();

    Thread td = null;
    @FXML
    JFXSnackbar snackBar;
    @FXML
    AnchorPane anchorPane;

    @FXML
    ImageView tallyimagestatus;

    @FXML
    ImageView internetimagestatus;


    @FXML
    PasswordField pass;
    private GaussianBlur blur, blur2;
    private boolean Result;
    private static String email;
    private double xOffset = 0;
    private double yOffset = 0;
    @FXML
    public void initialize() throws MalformedURLException {
        Font font = new Font("Roboto", 16);
        //Set Image
        //Status and Images
        NetworkStatus.getNetworkStatus(Tally, Internet, tallyimagestatus, internetimagestatus);

        //Minimize
        Image image2 = new Image(new File("/home/gulshan/Desktop/TallyZeeAppliCationResource/Resource/Minimize.png").toURL().toString());
        ImageView imageView = new ImageView(image2);
        imageView.setFitHeight(15);
        imageView.setFitWidth(15);
        Minimize.setGraphic(imageView);
        //Logo
        Image logoImage = new Image(new File("/home/gulshan/Desktop/TallyZeeAppliCationResource/Resource/Logo.png").toURL().toString());
        Logo.setImage(logoImage);
        //Close
        Image image1 = new Image(new File("/home/gulshan/Desktop/TallyZeeAppliCationResource/Resource/Close.png").toURL().toString());
        ImageView imageView1 = new ImageView(image1);
        imageView1.setFitHeight(13);
        imageView1.setFitWidth(13);
        Close.setGraphic(imageView1);
        //TopLogo
        Image logo_image = new Image(new File("/home/gulshan/Desktop/TallyZeeAppliCationResource/Resource/stageLogo.png").toURL().toString());
        ImageView headerLogo = new ImageView(logo_image);
        headerLogo.setFitHeight(20);
        headerLogo.setFitWidth(20);
        logoHeader.setGraphic(headerLogo);
        DropShadow dropShadow = new DropShadow();
        dropShadow.setRadius(15.0);
        dropShadow.setOffsetX(50.0);
        dropShadow.setOffsetY(50.0);
        dropShadow.setColor(Color.rgb(107, 255, 255));
        //Background and Fonts
        SignIn.setStyle("-fx-background-color: #41B539");
        progressIndicator.setVisible(false);
    }

    @FXML
    public void HandleMinimize(Event event) {
        ((Stage) ((Label) event.getSource()).getScene().getWindow()).setIconified(true);

    }

    @FXML
    public void HandleClose(Event event) {
        ((Stage) ((Label) event.getSource()).getScene().getWindow()).close();
    }


    @FXML
    public void handleAccount() throws IOException {
        CreateStage.loadAccountStage();
        Stage loginStage = (Stage) logoHeader.getScene().getWindow();
        loginStage.close();
    }


    @FXML
    public void forgetPassword() throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/java/tallyzee/LoginCntr/Password/forgotPassword.fxml"));
            Stage stage = (Stage) SignIn.getScene().getWindow();
            stage.close();
            Parent rootnew = (Parent) loader.load();
            Stage new_stage = new Stage();
            new_stage.setScene(new Scene(rootnew, 803, 597));
            new_stage.initStyle(StageStyle.UNDECORATED);
            new_stage.show();
            new_stage.setResizable(false);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    @FXML
    public void HandleSignIn() throws Exception {
        getAuthResult();
//        AccountStage accountStage=new AccountStage();
//        accountStage.loadAccountStage();
//        AccountStage.loadAccountStage();
    }
    public void LoadScene() throws IOException {
        CreateStage.loadAccountStage();
        Stage stage = (Stage) logoHeader.getScene().getWindow();
        stage.close();
    }
    public void getFalseResult() throws IOException {
        System.out.println("LoginCntr UnSuccessful");
        System.out.println("HELLO");
        progressIndicator.setVisible(false);
        anchorPane.setEffect(blur2);
    }
    private void getAuthResult() throws Exception {
        blur = new GaussianBlur(10);
        blur2 = new GaussianBlur(0);
        Window owner = SignIn.getScene().getWindow();
        String getUsername = userName.getText();
        System.out.println(getUsername.hashCode());
        String getPassword = pass.getText();
        if (getUsername.isEmpty()) {
           AlertHelper.showAlert(Alert.AlertType.CONFIRMATION, owner, "Please Enter Valid Email");

        } else if (getPassword.isEmpty()) {
           AlertHelper.showAlert(Alert.AlertType.CONFIRMATION, owner, "Please Enter Valid Password");
//            snackBar.show("Please Enter Valid Password",1000);
        } else {
            progressIndicator.setVisible(true);
            anchorPane.setEffect(blur);
            email = getUsername;
            FirebaseAuthUtil firebaseAuthUtil = new FirebaseAuthUtil(this,new UserInfo(getUsername, getPassword));
            firebaseAuthUtil.call();
           // firebaseAuthUtil.getAuthResponse(new UserInfo(getUsername, getPassword));
        }
    }
    public static String verifyEmail() {
        return email;
    }
    @Override
    public void getResponse(ResponseType response) {
        Window owner = SignIn.getScene().getWindow();
        switch (response)
        {
            case RESPONSEOK:
                System.out.println("process ok" + response);
                progressIndicator.setVisible(false);
                anchorPane.setEffect(blur2);
                System.out.println(Thread.currentThread().getName());
                System.out.println(Thread.currentThread().getName());
                try {
                    //System.out.println( "the company is"+NumberofCompanys.getCurrentCompany());
                    Session.setEmail(email);

                    AccountStage.loadAccountStage();
                    Stage loginStage = (Stage) logoHeader.getScene().getWindow();
                    loginStage.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case RESPONSENOTFOUND:
                System.out.println("process ok" + response);
                progressIndicator.setVisible(false);
                anchorPane.setEffect(blur2);
                System.out.println(Thread.currentThread().getName());
                Alert alert= AlertHelper.showCustomAlert(Alert.AlertType.WARNING, owner, "Please Enter Valid Email and Password");
                final Button ok = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
                ok.addEventFilter(ActionEvent.ACTION, event ->{userName.setText("");
                    pass.setText("");
                    System.out.println("OK was definitely pressed");}
                );
                alert.show();
                break;
            case RESPONSENETWORKISSUE:
                System.out.println("process ok" + response);
                progressIndicator.setVisible(false);
                anchorPane.setEffect(blur2);
                System.out.println(Thread.currentThread().getName());
                Alert alert1= AlertHelper.showCustomAlert(Alert.AlertType.WARNING, owner, "Your network Connetion is Slow! make sure your internet is on");
                final Button ok1 = (Button) alert1.getDialogPane().lookupButton(ButtonType.OK);
                ok1.addEventFilter(ActionEvent.ACTION, event ->{
                    System.out.println("OK was definitely pressed");
                }
                );
               alert1.show();
                break;
            case NETWORKERROR:
                Alert alert11= AlertHelper.showCustomAlert(Alert.AlertType.WARNING, owner, "Your Network Is Not Network !!!!!!!!!!!!");
                final Button ok11 = (Button) alert11.getDialogPane().lookupButton(ButtonType.OK);
                ok11.addEventFilter(ActionEvent.ACTION, event ->{userName.setText("");
                    pass.setText("");
                    System.out.println("OK Was Definitely Pressed...");}
                );
                alert11.show();
                progressIndicator.setVisible(false);
                break;
        }
    }
}
