package com.java.tallyzee.LoginCntr;

import com.java.tallyzee.Database.DatabaseConfig;
import com.java.tallyzee.Database.UserConnection;
import com.java.tallyzee.StageLoader.AccountStage;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import com.java.tallyzee.StageLoader.LoginStage;
import javafx.stage.StageStyle;

public class Login extends Application {
    private static double xOffset = 0;
    private static double yOffset = 0;
    @Override
    public void start(Stage primaryStage) throws Exception {

            //com/java/tallyzee/LoginCntr/login.fxml
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/com/java/tallyzee/LoginCntr/login.fxml"));
            // Parent content = loader.load();
            //   FXMLLoader fxmlLoader = new FXMLLoader(LoginStage.class.getResource("/com/java/tallyzee/LoginCntr/login.fxml"));
            Parent root = loader.load();

            primaryStage.initStyle(StageStyle.TRANSPARENT);
            primaryStage.setResizable(false);
            Scene scene = new Scene(  root, 803, 597);
            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();
                }
            });
            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    primaryStage.setX(event.getScreenX() - xOffset);
                    primaryStage.setY(event.getScreenY() - yOffset);
                }
            });
            primaryStage.getIcons().add(new Image(LoginStage.class.getResource("Logo.png").toExternalForm()));
            primaryStage.setScene(scene);
            if(UserConnection.getCurrentUser(DatabaseConfig.getConnection())!=null)
            {
                AccountStage.loadAccountStage();
                Stage loginStage = (Stage) scene.getWindow();
            }else
            primaryStage.show();
//        FirebaseAuthUtil firebaseAuthUtil = new FirebaseAuthUtil();
//        switch (firebaseAuthUtil.getInisilized()) {
//            case RESPONSENETWORKISSUE:
//                Alert alert1 = AlertHelper.showCustomAlert(Alert.AlertType.WARNING, primaryStage.getOwner(), "Your network Connetion is Slow! make sure your internet is on");
//                final Button ok1 = (Button) alert1.getDialogPane().lookupButton(ButtonType.OK);
//                ok1.addEventFilter(ActionEvent.ACTION, event -> {
//                            System.out.println("OK was definitely pressed");
//                        }
//                );
//                alert1.show();
//                break;
            //}


    }
    public static void main(String[] args) {
        launch(args);
    }
}
