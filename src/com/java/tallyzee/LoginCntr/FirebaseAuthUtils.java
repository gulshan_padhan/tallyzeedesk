package com.java.tallyzee.LoginCntr;


import com.java.tallyzee.Network.ResponseType;

public interface FirebaseAuthUtils {
   void getResponse(ResponseType response);
}
