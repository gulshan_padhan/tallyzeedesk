package com.java.tallyzee.LoginCntr.BottomNetworkStatus;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;
import com.java.tallyzee.Network.NetworkCall;

import java.io.File;
import java.net.MalformedURLException;

public class NetworkStatus {


    public static void getNetworkStatus(Label Tally,Label Internet,ImageView tallyimagestatus,ImageView internetimagestatus) throws MalformedURLException {
        Font font1 = new Font("Segoe UI",14);
        Tally.setFont(font1);
        Internet.setFont(font1);
        Image tallyonlineimage = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\tallyonline.png").toURL().toString());
        Image tallyofflineimage = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\tallyoffline.png").toURL().toString());
        Image internetonlineimage = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\internetonline.png").toURL().toString());
        Image internetofflineimage = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\internetoffline.png").toURL().toString());
        //Width and Height
        tallyimagestatus.setFitHeight(17);
        tallyimagestatus.setFitWidth(17);
        internetimagestatus.setFitHeight(17);
        internetimagestatus.setFitWidth(17);

        if(checkTallyStatus())
        {
            Tally.setText("Tally : Connected");
            tallyimagestatus.setImage(tallyonlineimage);
        }
        else
        {
            Tally.setText("Tally : Not Connected");
            tallyimagestatus.setImage(tallyofflineimage);
        }
        if(checkInternetStatus())
        {
            Internet.setText("Internet : Connected");
            internetimagestatus.setImage(internetonlineimage);
        }
        else
        {
            Internet.setText("Internet : Not Connected");
            internetimagestatus.setImage(internetofflineimage);
        }
    }
    public static boolean checkInternetStatus()
    {
        if(NetworkCall.getInternetStatus())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static boolean checkTallyStatus()
    {
        if(NetworkCall.getTallyStatus())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
