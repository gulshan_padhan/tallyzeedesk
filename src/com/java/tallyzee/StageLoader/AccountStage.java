package com.java.tallyzee.StageLoader;

import javafx.concurrent.Service;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
public class AccountStage {
    private Service<Void> bacgroundServices;
    private static double xOffset = 0;
    private static double yOffset = 0;
    Stage primaryStage;
    public static AccountStage  getInistance()
    {
        return new AccountStage();
    }
    public  static  void loadAccountStage() throws IOException {
try {

    FXMLLoader fxmlLoader = new FXMLLoader();
    fxmlLoader.setLocation(getInistance().getClass().getResource("/com/java/tallyzee/DashBoard/tallyfyaccount.fxml"));
    Parent root = fxmlLoader.load();

    Stage primaryStage = new Stage();

    //LoginController controller = (LoginController) fxmlLoader.getController();
    // controller.LoadScene();
    primaryStage.initStyle(StageStyle.TRANSPARENT);
    primaryStage.setResizable(false);
    Scene scene = new Scene(root, 803, 597);
    root.setOnMousePressed(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            xOffset = event.getSceneX();
            yOffset = event.getSceneY();
        }
    });
    root.setOnMouseDragged(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            primaryStage.setX(event.getScreenX() - xOffset);
            primaryStage.setY(event.getScreenY() - yOffset);
        }
    });
    primaryStage.getIcons().add(new Image(LoginStage.class.getResource("Logo.png").toExternalForm()));
    primaryStage.setScene(scene);

    primaryStage.show();
}catch (Exception e)
{
    e.printStackTrace();
}

    }


}
