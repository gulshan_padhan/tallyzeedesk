package com.java.tallyzee.StageLoader;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class CreateStage {

    private static double xOffset = 0;
    private static double yOffset = 0;
    public static Stage loadAccountStage() throws IOException {
        FXMLLoader loader = new FXMLLoader(CreateStage.class.getResource("/com/java/tallyzee/SignUP/CreateAccount.fxml"));
        Parent root1 = loader.load();
        Stage primaryStage = new Stage();
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setResizable(false);
        Scene scene = new Scene(root1,803,597);
        root1.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root1.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
        primaryStage.getIcons().add(new Image(CreateStage.class.getResource("Logo.png").toExternalForm()));
        primaryStage.setScene(scene);
        primaryStage.show();
        return primaryStage;
    }
}
