package com.java.tallyzee.StageLoader;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class LoginStage {

    private static double xOffset = 0;
    private static double yOffset = 0;
    public static LoginStage  getInistance()
    {
      return new LoginStage();
    }

    public static Stage loadLoginStage() throws IOException,Exception {
        //com/java/tallyzee/LoginCntr/login.fxml
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getInistance().getClass().getResource("/com/java/tallyzee/LoginCntr/login.fxml"));
       // Parent content = loader.load();
     //   FXMLLoader fxmlLoader = new FXMLLoader(LoginStage.class.getResource("/com/java/tallyzee/LoginCntr/login.fxml"));
        Parent root = loader.load();
        Stage primaryStage = new Stage();
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setResizable(false);
        Scene scene = new Scene(  root, 803, 597);
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
        primaryStage.getIcons().add(new Image(LoginStage.class.getResource("Logo.png").toExternalForm()));
        primaryStage.setScene(scene);
        primaryStage.show();
        return primaryStage;

    }
}
