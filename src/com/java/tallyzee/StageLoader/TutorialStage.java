package com.java.tallyzee.StageLoader;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class TutorialStage {

//C:\Users\dev9\IdeaProjects\TallyZeeAppDest\out\production\TallyZeeAppDest
    public static Stage getTutorialStage(Stage parent_stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader((TutorialStage.class.getResource("/com/java/tallyzee/DashBoard/Tabs/Tutorial/tutorialDialog.fxml")));
        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.setIconified(false);
        stage.initStyle(StageStyle.UNDECORATED);
        Scene scene = new Scene(root1, 646, 405);
        stage.setScene(scene);
        stage.initOwner(parent_stage);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.show();
        return stage;
    }
}
