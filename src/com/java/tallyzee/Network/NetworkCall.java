package com.java.tallyzee.Network;


import com.google.gson.Gson;
import com.java.tallyzee.LoginCntr.UserInfo;
import com.java.tallyzee.Models.SalesVoucher;
import com.java.tallyzee.Models.User;
import com.java.tallyzee.Utility.AppParserUtil;
import okhttp3.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;


import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import static org.apache.http.protocol.HTTP.USER_AGENT;


public class NetworkCall {

    public static void sendHttpRequest(String payload) {
        System.out.println("after" + payload);
        StringBuffer response = null;
        try {
            String url = "http://www.tallyzee.com:8887/Company/createCompany";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type",
                    "application/json;charset=utf-8");
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(payload);
            wr.flush();
            wr.close();
            String responseStatus = con.getResponseMessage();
            System.out.println("error" + responseStatus);
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (IOException e) {
            System.out.println("error" + e.getMessage());
        }
    }

    public static String convertGson(ArrayList<SalesVoucher> mylist) {
        ObjectMapper mapper = new ObjectMapper();
        String payload = "";
        /**
         * Write object to file
         */
        try {

            payload = mapper.writeValueAsString(mylist);
            System.out.println(payload + " " + mylist);
            return payload;
            //mapper.writerWithDefaultPrettyPrinter().writeValue(new File("result.json"), carFleet);//Prettified JSON
        } catch (Exception e) {
            e.printStackTrace();
        }

//        String payload=new Gson().toJson(mylist);
//        System.out.println("before"+payload);
        return payload;
    }

    public static String convertGsonSingle(SalesVoucher mylist) {
        ObjectMapper mapper = new ObjectMapper();
        String payload = "";
        /**
         * Write object to file
         */
        try {

            payload = mapper.writeValueAsString(mylist);
            System.out.println(payload + " " + mylist);
            return payload;
            //mapper.writerWithDefaultPrettyPrinter().writeValue(new File("result.json"), carFleet);//Prettified JSON
        } catch (Exception e) {
            e.printStackTrace();
        }

//        String payload=new Gson().toJson(mylist);
//        System.out.println("before"+payload);
        return payload;
    }

    //    public static User convertGsonUser(String json)
//    {
//        ObjectMapper mapper = new ObjectMapper();
//        String payload="";
//        /**
//         * Write object to file
//         */
//        try {
//
//            payload=mapper.writeValueAsString(mylist);
//            System.out.println(payload+" "+mylist);
//            return payload;
//            //mapper.writerWithDefaultPrettyPrinter().writeValue(new File("result.json"), carFleet);//Prettified JSON
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
////        String payload=new Gson().toJson(mylist);
////        System.out.println("before"+payload);
//        return payload;
//    }
    public static boolean getInternetStatus() {


        try {

            URL url = new URL("https://google.com");
            URLConnection connection = url.openConnection();
            connection.connect();
            return true;

        } catch (Exception en) {
            System.out.println(en.getMessage());
        }
        return false;
    }

    public  UserInfo getUserAuth(String userName, String pass) {

        try {
String url="http://www.tallyzee.com:8887/User/getValidateUser/" + userName + "/" + pass;
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(url);

            // add request header
            request.addHeader("User-Agent", USER_AGENT);

            HttpResponse response = client.execute(request);

            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " +
                    response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println("the result:"+result.toString());
            Gson gson=new Gson();
           return gson.fromJson(result.toString(), UserInfo.class);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
    public  UserInfo getUser(String userName) {
        try {
            String url="http://www.tallyzee.com:8887/User/getUser/" + userName + "/" ;
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(url);

            // add request header
            request.addHeader("User-Agent", USER_AGENT);

            HttpResponse response = client.execute(request);

            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " +
                    response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println("the result:"+result.toString());
            Gson gson=new Gson();
            return gson.fromJson(result.toString(), UserInfo.class);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
    public static boolean getTallyStatus() {
        try {
            URL url = new URL("http://"+ AppParserUtil.ipAdress +":9000");
            URLConnection connection = url.openConnection();
            connection.connect();
            connection.getDoOutput();
            return true;
        } catch (Exception en) {
            System.out.println(en.getMessage());
        }
        return false;
    }
}
