package com.java.tallyzee.Network;

import com.java.tallyzee.Utility.AppParserUtil;
import org.json.JSONObject;
import org.json.XML;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NumberofCompanys {

    public static String getResponse(String test) {
        String urlParameters = "<ENVELOPE><HEADER><VERSION>1</VERSION><TALLYREQUEST>Export</TALLYREQUEST><TYPE>Data</TYPE><ID>LedBills</ID></HEADER><BODY><DESC><STATICVARIABLES><EXPLODEFLAG>Yes</EXPLODEFLAG><SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVCURRENTCOMPANY>AIM INFOCOM SERVICES PVT.LTD (15-16)</SVCURRENTCOMPANY></STATICVARIABLES></DESC></BODY></ENVELOPE>";
        String urlt = "<ENVELOPE><HEADER><VERSION>1</VERSION><TALLYREQUEST>Export</TALLYREQUEST><TYPE>Data</TYPE><ID>Ledger OutStandings</ID></HEADER><BODY><DESC><STATICVARIABLES><SVCURRENTCOMPANY>AIM INFOCOM SERVICES PVT.LTD (15-16)</SVCURRENTCOMPANY></STATICVARIABLES></DESC><DATA><PARTYLEDGERNAME>100 Sign & Display</PARTYLEDGERNAME></DATA></BODY></ENVELOPE>";
        String lastUrl = "<ENVELOPE><HEADER><VERSION>1</VERSION><TALLYREQUEST>Export</TALLYREQUEST><TYPE>Collection</TYPE><ID>Collection of Ledgers</ID></HEADER><BODY><DESC><STATICVARIABLES><SVCurrentCompany>AIM INFOCOM SERVICES PVT.LTD (15-16)</SVCurrentCompany></STATICVARIABLES><TDL><TDLMESSAGE><COLLECTION NAME='Collection of Ledgers' ISMODIFY='No'><TYPE>Ledger</TYPE></COLLECTION></TDLMESSAGE></TDL></DESC></BODY></ENVELOPE>";
        String testUrls = "<ENVELOPE><HEADER><VERSION> 1 </VERSION><TALLYREQUEST> Export </TALLYREQUEST><TYPE> DATA </TYPE><ID>Voucher Register</ID></HEADER> <BODY> <DESC> <STATICVARIABLES><SVCurrentCompany>##SVCURRENTCOMPANY</SVCurrentCompany><SVVoucherNumber></SVVoucherNumber><SVEXPORTFORMAT>$$SysName: XML </SVEXPORTFORMAT><SVFROMDATE TYPE='Date'>20170401</SVFROMDATE><SVTODATE TYPE='Date'>20170401</SVTODATE></STATICVARIABLES> <EXPLODEFLAG>Yes</EXPLODEFLAG></DESC></BODY></ENVELOPE>";
        StringBuffer response = null;
        try {
            //System.out.println(AppParserUtil.ipAdress);
            String url = "http://"+ AppParserUtil.ipAdress +":9000";
            System.out.println(AppParserUtil.ipAdress+":9000");
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type",
                    "application/xml;charset=utf-8");
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(test);
            wr.flush();
            wr.close();
            String responseStatus = con.getResponseMessage();
            System.out.println("error" + responseStatus);
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (IOException e) {
            System.out.println("error" + e.getMessage());
        }
        //System.out.println(response);
//        JSONObject xmlJSONObj = XML.toJSONObject(response);
//        String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
//        System.out.println(jsonPrettyPrintString);
        //XMLToJSON.test(response.toString());
        return response.toString();
    }

    public static String getStartDate(String companyName)
    {
        String companyPayload="<ENVELOPE>\n\n<HEADER>\n\n<VERSION>1</VERSION>\n\n<TALLYREQUEST>EXPORT</TALLYREQUEST>\n\n<TYPE>OBJECT</TYPE> <SUBTYPE>Company</SUBTYPE> <ID TYPE=\"Name\">"+companyName+"</ID>\n\n</HEADER>\n\n<BODY>\n\n<DESC>\n\n<STATICVARIABLES><SVFROMDATE TYPE=\"Date\">1-4-2010</SVFROMDATE>   <SVTODATE TYPE=\"Date\">31-3-2022</SVTODATE><SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT></STATICVARIABLES>\n\n<FETCHLIST>\n\n<FETCH>Name</FETCH>\n\n<FETCH>TNetBalance</FETCH>\n\n<FETCH>Parent</FETCH>\n\n</FETCHLIST>\n\n<TDL>\n\n<TDLMESSAGE>\n\n<OBJECT NAME=\"Company\" ISINITIALIZE=\"Yes\">\n\n<LOCALFORMULA>\n\nTNetBalance: $BooksFrom\n\n</LOCALFORMULA>\n\n</OBJECT>\n\n</TDLMESSAGE>\n\n</TDL>\n\n</DESC>\n\n</BODY>\n\n</ENVELOPE>";
                System.out.println(XML.toJSONObject(NetworkServices.getResponse(companyPayload)));
                String json= String.valueOf(XML.toJSONObject(NetworkServices.getResponse(companyPayload)));
                JSONObject jsonObject=new JSONObject(json);

        JSONObject envlop = jsonObject.getJSONObject("ENVELOPE");
                JSONObject jsonObjectbody=envlop.getJSONObject("BODY");
        JSONObject jsonObjectdata=jsonObjectbody.getJSONObject("DATA");
        JSONObject jsonObjecttallymsg=jsonObjectdata.getJSONObject("TALLYMESSAGE");
        JSONObject jsonObjectcomp=jsonObjecttallymsg.getJSONObject("COMPANY");
        JSONObject jsonObjectdate=jsonObjectcomp.getJSONObject("TNETBALANCE");
        String content= String.valueOf(jsonObjectdate.get("content"));
System.out.println("the number is"+content);
               String name= String.valueOf(XML.toJSONObject(NetworkServices.getResponse(companyPayload)));
                return content;
    }
    public static String getCompanyNames()
    {
        String xml = "<ENVELOPE>   \r\n <HEADER>   \r\n <VERSION>1</VERSION>   \r\n <REQVERSION>1</REQVERSION>   \r\n<TALLYREQUEST>Export</TALLYREQUEST>   \r\n<TYPE>DATA</TYPE>   \r\n   <ID>LIST OF COMPANIES</ID>   \r\n</HEADER>   \r\n <BODY>   \r\n<DESC>   \r\n <STATICVARIABLES>   \r\n <SVINCLUDE>Connected</SVINCLUDE>   \r\n</STATICVARIABLES>   \r\n</DESC>   \r\n</BODY>   \r\n</ENVELOPE>";
        return xml;
    }
    public static String getCurrentCompany()
    {
        String companyName="";
        String xml = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">3Jan2017</SVFROMDATE>   <SVTODATE TYPE=\"Date\">3Jan2017</SVTODATE>                 </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL></REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$Is Sales Order:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";
        try{
            //System.out.println(XML.toJSONObject(NetworkServices.getResponse(xml)));
            JSONObject main = new JSONObject(XML.toJSONObject(NetworkServices.getResponse(xml)));
            JSONObject envlop = main.getJSONObject("ENVELOPE");
            JSONObject body = envlop.getJSONObject("BODY");
            JSONObject desc = body.getJSONObject("DESC");
            JSONObject stavar = desc.getJSONObject("STATICVARIABLES");
           companyName =String.valueOf(stavar.get("SVCURRENTCOMPANY"));
        }catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        getStartDate(companyName);
        return companyName;
    }
}
