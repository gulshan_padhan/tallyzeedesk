package com.java.tallyzee.Network;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import net.projectmonkey.object.mapper.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import com.java.tallyzee.Models.*;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NetworkController {
    String companyName;
    String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    String[] monthsCount = {"31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"};
    String[] daysList = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
    ArrayList<SalesVoucher> arrayList = new ArrayList<>();

    public String test(String responseXml) {
        JSONObject obj = null;
        try {
            obj = XML.toJSONObject(responseXml);
            System.out.println("json" + obj.toString());
            //printJson(obj.toString());
        } catch (Exception e) {
            System.out.println(e);
        }
        return obj.toString();
    }

    public String getPayRoll(String fromdate, String todate) {
        String payroll = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">" + fromdate + "</SVFROMDATE>   <SVTODATE TYPE=\"Date\">" + todate + "</SVTODATE>                 </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsSales:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";

        return payroll;
    }

    public void actionPerformed() throws Exception {
        String test = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">20170404</SVFROMDATE>   <SVTODATE TYPE=\"Date\">20170404</SVTODATE>                 </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsSales:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";

        String name = "", email;
        try {
            FileInputStream serviceAccount = new FileInputStream("C:\\Users\\dev9\\Desktop\\admin-sdk.json");
            System.out.println(serviceAccount.getChannel());
            // Initialize the app with a service account, granting admin privileges
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://tallyfy-364a0.firebaseio.com/")
                    .build();
            System.out.println(options.getDatabaseUrl());
            FirebaseApp.initializeApp(options);
        } catch (Exception enn) {
        }

        System.out.println("OUT OF EXCEPTION");


//        SalesVoucher object_sales = new SalesVoucher(address,buyerName,partyName,amount,voucherType,parentVoucherType,partyLedgerName
//                ,reference,basicBasePartyName);
//            System.out.println(object_sales.getAddress());
        ObjectMapper mapper = new ObjectMapper();
        // As an admin, the app has access to read and write all data, regardless of Security Rules
        DatabaseReference ref = FirebaseDatabase.getInstance()
                .getReferenceFromUrl("https://tallyfy-364a0.firebaseio.com/");
        String fromDay="16Nov2017", toDay="16Nov2017";
        String year = "2017";
        int count = 0;
//        for (int days = 10; days < monthsCount.length-1; days++) {
//            for (int j = 15; j < Integer.valueOf(monthsCount[days])-12; j++) {

              //  fromDay = daysList[j] + "" + months[days] + "" + year;
                ArrayList<SalesVoucher> salesVouchers = new ArrayList<>();
        //        System.out.println(getPayRoll(fromDay, fromDay));

                salesVouchers = (ArrayList<SalesVoucher>) printJson(test(NetworkServices.getResponse(getPayRoll(fromDay, fromDay))));
                System.out.println("the" + count + "response size is:" + salesVouchers.size());
                if (salesVouchers.size() > 0) {
//
                    for (int i = 0; i < salesVouchers.size(); i++) {

                        ref.child("TallFy").child("Anys1238ty90").child("Sales_Vouchers").child(String.valueOf(count)).setValue(salesVouchers.get(i), new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            }
                        });
                        count++;
                    }
                }
                try {
                    //  Thread.sleep(1000);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }


            }

      //  }

    //}

    //    public Object InitializeControls()
//    {
//        SalesVoucher sales = new SalesVoucher();
//        String address = sales.getAddress();
//        String buyerName = sales.getBasicBuyerName();
//        String partyName = sales.getPartyName();
//        String amount = sales.getAmount();
//        String voucherType = sales.getVoucherType();
//        String parentVoucherType = sales.getParentVoucherType();
//        String partyLedgerName = sales.getPartyLedgerName();
//        String reference = sales.getReference();
//        String basicBasePartyName = sales.getBasicBasePartyName();
//         object_sales = new SalesVoucher(address,buyerName,partyName,amount,voucherType,parentVoucherType,partyLedgerName
//        ,reference,basicBasePartyName);
//
//        return object_sales;
//    }
    public static class PC
    {
        // Prints a string and waits for consume()
        public void produce()throws InterruptedException
        {
            // synchronized block ensures only one thread
            // running at a time.
            synchronized(this)
            {
                System.out.println("producer thread running");

                // releases the lock on shared resource
                wait();

                // and waits till some other method invokes notify().
                System.out.println("Resumed");
            }
        }

        // Sleeps for some time and waits for a key press. After key
        // is pressed, it notifies produce().
        public void consume()throws InterruptedException
        {
            // this makes the produce thread to run first.
            Thread.sleep(1000);
            // Scanner s = new Scanner(System.in);

            // synchronized block ensures only one thread
            // running at a time.
            synchronized(this)
            {
                System.out.println("Waiting for return key.");
                // s.nextLine();

                System.out.println("Return key pressed");

                // notifies the produce thread that it
                // can wake up.
                notify();

                // Sleep
                Thread.sleep(2000);
            }
        }
    }
    List<SalesVoucher> printJson(String json) {

        String voucherDate;
        String voucherPartyName;
        String voucherNumber;
        String voucherNarration;
        String voucherTypeName;
        String basicUserDesc;
        Double rate;
        String amount;
        int edqty;
        int actualQty;

        String stockName;
        String gowdownName;
        String batchName;
        Double batchamount;
        int batchactualQty;
        int billQty;
        Date orderDueDate;
        List<LedgerItem> items = null;
        List<Item> inventories = null;
        Double basicOrderRate;
        String orderType;
        String basicPurchaseOrderNumber;
        Double orderamount;
        String ledgerName;
        String billName;
        String billAmount;
        String billType;
        String billCreditPeriod;
        List<SalesVoucher> salesVouchers = new ArrayList<>();
        List<InvoiceOrderList> invoiceOrderLists = null;
        List<Inventories> inventoriesList = null;
        try {
            JSONObject main = new JSONObject(json);
            JSONObject envlop = main.getJSONObject("ENVELOPE");

            JSONObject body = envlop.getJSONObject("BODY");
            JSONObject desc = body.getJSONObject("DESC");
            JSONObject stavar = desc.getJSONObject("STATICVARIABLES");
            // System.out.println(stavar.get("SVCURRENTCOMPANY"));
            companyName = String.valueOf(stavar.get("SVCURRENTCOMPANY"));
            JSONObject data = body.getJSONObject("DATA");
            salesVouchers = new ArrayList<>();
            if (!data.equals(null) && data.isNull("DATA")) {
                JSONArray tallymsg = data.getJSONArray("TALLYMESSAGE");
                //System.out.println(tallymsg);
                //System.out.println(json);
                for (int i = 0; i < tallymsg.length(); i++) {
                    invoiceOrderLists = new ArrayList<>();
                    JSONObject tamsg = tallymsg.getJSONObject(i);
                    //System.out.println(tamsg);
                    JSONObject voucher = tamsg.getJSONObject("VOUCHER");
                    inventoriesList = new ArrayList<>();
                    System.out.print("Voucher" + " " + voucher.get("DATE") + " " + voucher.getString("PARTYLEDGERNAME") + " " + voucher.getString("VOUCHERTYPENAME") + " " + voucher.getString("VOUCHERNUMBER"));
                    voucherDate = String.valueOf(voucher.get("DATE"));
                    voucherPartyName = voucher.getString("PARTYLEDGERNAME");
                    voucherNumber = voucher.getString("VOUCHERNUMBER");
                    voucherTypeName = voucher.getString("VOUCHERTYPENAME");
                    voucherNarration = String.valueOf(voucher.get("NARRATION"));
                    //voucherType v;
                    //System.out.print(" " + voucher.get("NARRATION"));
                    JSONObject inventory = voucher.getJSONObject("ALLINVENTORYENTRIES.LIST");
                    JSONObject bsc = inventory.getJSONObject("BASICUSERDESCRIPTION.LIST");
                    System.out.println(bsc.get("BASICUSERDESCRIPTION") + " " + inventory.get("STOCKITEMNAME") + " " + inventory.get("RATE") + " " + inventory.get("AMOUNT") + " " + inventory.get("BILLEDQTY") + " " + inventory.get("ACTUALQTY"));

                    basicUserDesc = String.valueOf(bsc.get("BASICUSERDESCRIPTION"));
                    List<BatchAllocation> batchAllocationList = new ArrayList<>();
                    JSONObject batch = inventory.getJSONObject("BATCHALLOCATIONS.LIST");
                    System.out.println(batch.getString("GODOWNNAME") + " " + batch.get("BATCHNAME") + batch.get("ORDERNO") + batch.get("AMOUNT") + " " + batch.get("ACTUALQTY") + " " + batch.get("BILLEDQTY"));
                    JSONObject overdue = batch.getJSONObject("ORDERDUEDATE");
                    //System.out.println(overdue.get("P"));
                    batchAllocationList.add(new BatchAllocation(String.valueOf(batch.getString("GODOWNNAME")), String.valueOf(batch.get("BATCHNAME")), String.valueOf(batch.get("ORDERNO")), String.valueOf(batch.get("AMOUNT")), String.valueOf(batch.get("ACTUALQTY")), String.valueOf(batch.get("BILLEDQTY")), String.valueOf(overdue.get("P"))));
                    inventoriesList.add(new Inventories(String.valueOf(inventory.get("STOCKITEMNAME")), String.valueOf(bsc.get("BASICUSERDESCRIPTION")), String.valueOf(inventory.get("RATE")), String.valueOf(inventory.get("AMOUNT")), String.valueOf(inventory.get("BILLEDQTY")), String.valueOf(inventory.get("ACTUALQTY")), batchAllocationList));

                    //DashType type=DashType.valueOf(voucher.getString("VOUCHERTYPENAME"));
                    JSONObject invoicebill = voucher.getJSONObject("INVOICEORDERLIST.LIST");
                    System.out.println("invoicebill" + invoicebill.get("BASICORDERDATE") + " " + invoicebill.get("ORDERTYPE") + " " + invoicebill.getString("BASICPURCHASEORDERNO"));
                    JSONArray list = voucher.getJSONArray("LEDGERENTRIES.LIST");
                    items = new ArrayList<>();
                    for (int j = 0; j < list.length(); j++) {
                        JSONObject bill = list.getJSONObject(j);
                        //System.out.println(bill);
                        inventories = new ArrayList<>();
                        amount = String.valueOf(bill.get("AMOUNT"));
                        ledgerName = String.valueOf(bill.getString("LEDGERNAME"));
                        System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");
                        String op = bill.get("BILLALLOCATIONS.LIST").toString();
                        //System.out.println("the String is Object:"+op);
                        if (!op.equals(null) && !op.isEmpty())
                            if (op.charAt(0) == '{') {

                                inventories = new ArrayList<>();
                                // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                //System.out.println("the String is Object:"+op);
                                JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                //System.out.println("ok tasted 1");
                                billAmount = String.valueOf(billl.get("AMOUNT"));
                                billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                                billType = String.valueOf(billl.getString("BILLTYPE"));
                                billName = String.valueOf(billl.get("NAME"));
                                inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                            } else if (op.charAt(0) == '[') {
                                System.out.println("ok tasted 2");
                                // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                inventories = new ArrayList<>();
                                //System.out.println("the String is Array:"+op);
                                String reString = bill.toString();
                                //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                //System.out.println("ok tasted 3");
                                for (int b = 0; b < billlist.length(); b++) {
                                    JSONObject billItems = billlist.getJSONObject(b);
                                    billAmount = String.valueOf(billItems.get("AMOUNT"));
                                    billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                                    billType = String.valueOf(billItems.getString("BILLTYPE"));
                                    billName = String.valueOf(billItems.get("NAME"));
                                    inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                    System.out.println("bill" + " " + billItems.get("NAME") + " " + billItems.get("BILLTYPE") + " " + billItems.get("AMOUNT") + " " + billItems.getString("BILLCREDITPERIOD") + " ");
                                }
                                //System.out.println("ok tasted 4");
                                //e.printStackTrace();
                            } else {
                                items = new ArrayList<>();
                                System.out.println("ok null");
                            }
                        String ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                        //System.out.println("the String is Object:"+op);
                        if (!ba.equals(null) && !ba.isEmpty())
                            if (ba.charAt(0) == '{') {
                                ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                //\System.out.println("the String is Object:"+op);
                                JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                //System.out.println("ok tasted 1");
                            } else if (ba.charAt(0) == '[') {
                                System.out.print("ok tasted 2");
                                ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                //System.out.println("the String is Array:"+op);
                                String reString = bill.toString();
                                //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                //System.out.println("ok tasted 3");
                                for (int b = 0; b < billlist.length(); b++) {
                                    JSONObject billItems = billlist.getJSONObject(b);
                                    System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                                }
                                //System.out.println("ok tasted 4");
                                //e.printStackTrace();
                            } else {
                                System.out.println("ok null");
                            }
                        items.add(new LedgerItem(amount, ledgerName, inventories));
                    }

//
//			switch(type)
//			{
//			case Receipt:
//			{
//			JSONArray list=voucher.getJSONArray("ALLLEDGERENTRIES.LIST");
//
//			for(int j=0;j<list.length();j++)
//			{
//
//				JSONObject bill=list.getJSONObject(j);
//				//System.out.println(bill);
//				System.out.println("bill ty"+bill.getString("AMOUNT")+" "+bill.getString("LEDGERNAME"));
//				String op=bill.getString("BILLALLOCATIONS.LIST").toString();
//				//System.out.println("the String is Object:"+op);
//				if(!op.equals(null)&&!op.isEmpty())
//				if(op.charAt(0)=='{')
//				{
//					op=bill.getString("BILLALLOCATIONS.LIST").toString();
//					//System.out.println("the String is Object:"+op);
//					JSONObject billl=bill.getJSONObject("BILLALLOCATIONS.LIST");
//					System.out.println(billl.get("NAME")+" "+billl.getString("AMOUNT")+" "+billl.getString("BILLTYPE"));
//					//System.out.println("ok tasted 1");
//
//				}
//				else if(op.charAt(0)=='[')
//				{
//					System.out.println("ok tasted 2");
//					op=bill.getString("BILLALLOCATIONS.LIST").toString();
//
//					//System.out.println("the String is Array:"+op);
//					String reString=bill.toString();
//					//System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
//					JSONArray billlist=bill.getJSONArray("BILLALLOCATIONS.LIST");
//					//System.out.println("ok tasted 3");
//					for(int b=0;b<billlist.length();b++)
//					{
//						JSONObject billItems=billlist.getJSONObject(b);
//						System.out.println("bill"+" "+billItems.get("NAME")+" "+billItems.get("BILLTYPE")+" "+billItems.get("AMOUNT")+" ");
//					}
//					//System.out.println("ok tasted 4");
//					//e.printStackTrace();
//
//				}else
//				{
//					System.out.println("ok null");
//				}
//				String ba=bill.getString("BANKALLOCATIONS.LIST").toString();
//				//System.out.println("the String is Object:"+op);
//				if(!ba.equals(null)&&!ba.isEmpty())
//				if(ba.charAt(0)=='{')
//				{
//					ba=bill.getString("BANKALLOCATIONS.LIST").toString();
//					//\System.out.println("the String is Object:"+op);
//					JSONObject billl=bill.getJSONObject("BANKALLOCATIONS.LIST");
//					System.out.println(billl.get("DATE")+" "+billl.getString("TRANSACTIONTYPE")+" "+billl.getString("PAYMENTFAVOURING")+" "+billl.getString("AMOUNT")+" "+billl.getString("PAYMENTFAVOURING")+" "+billl.getString("PAYMENTFAVOURING"));
//					//System.out.println("ok tasted 1");
//
//				}
//				else if(ba.charAt(0)=='[')
//				{
//					System.out.println("ok tasted 2");
//					ba=bill.getString("BANKALLOCATIONS.LIST").toString();
//
//					//System.out.println("the String is Array:"+op);
//					String reString=bill.toString();
//					//System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
//					JSONArray billlist=bill.getJSONArray("BANKALLOCATIONS.LIST");
//					//System.out.println("ok tasted 3");
//					for(int b=0;b<billlist.length();b++)
//					{
//						JSONObject billItems=billlist.getJSONObject(b);
//						System.out.println(billItems.get("DATE")+" "+billItems.getString("TRANSACTIONTYPE")+" "+billItems.getString("PAYMENTFAVOURING")+" "+billItems.getString("AMOUNT")+" "+billItems.getString("PAYMENTFAVOURING")+" "+billItems.getString("PAYMENTFAVOURING"));
//
//						}
//					//System.out.println("ok tasted 4");
//					//e.printStackTrace();
//
//				}else
//				{
//					System.out.println("ok null");
//				}
//
//			}
////
//			}
//			break;
//			case Payment:
//			{
//
//				JSONArray list=voucher.getJSONArray("ALLLEDGERENTRIES.LIST");
//
//				for(int j=0;j<list.length();j++)
//				{
//
//					JSONObject bill=list.getJSONObject(j);
//					//System.out.println(bill);
//					System.out.println(bill.getString("AMOUNT")+" "+bill.getString("LEDGERNAME")+" ");
//					String op=bill.getString("BILLALLOCATIONS.LIST").toString();
//					//System.out.println("the String is Object:"+op);
//					if(!op.equals(null)&&!op.isEmpty())
//					if(op.charAt(0)=='{')
//					{
//						op=bill.getString("BILLALLOCATIONS.LIST").toString();
//						//System.out.println("the String is Object:"+op);
//						JSONObject billl=bill.getJSONObject("BILLALLOCATIONS.LIST");
//						System.out.println(billl.get("NAME")+" "+billl.getString("AMOUNT")+" "+billl.getString("BILLTYPE")+" "+billl.getString("BILLCREDITPERIOD")+" ");
//						//System.out.println("ok tasted 1");
//
//					}
//					else if(op.charAt(0)=='[')
//					{
//						System.out.println("ok tasted 2");
//						op=bill.getString("BILLALLOCATIONS.LIST").toString();
//
//						//System.out.println("the String is Array:"+op);
//						String reString=bill.toString();
//						//System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
//						JSONArray billlist=bill.getJSONArray("BILLALLOCATIONS.LIST");
//						//System.out.println("ok tasted 3");
//						for(int b=0;b<billlist.length();b++)
//						{
//							JSONObject billItems=billlist.getJSONObject(b);
//							System.out.println("bill"+" "+billItems.get("NAME")+" "+billItems.get("BILLTYPE")+" "+billItems.get("AMOUNT")+" "+billItems.getString("BILLCREDITPERIOD")+" ");
//						}
//						//System.out.println("ok tasted 4");
//						//e.printStackTrace();
//
//					}else
//					{
//						System.out.println("ok null");
//					}
//					String ba=bill.getString("BANKALLOCATIONS.LIST").toString();
//					//System.out.println("the String is Object:"+op);
//					if(!ba.equals(null)&&!ba.isEmpty())
//					if(ba.charAt(0)=='{')
//					{
//						ba=bill.getString("BANKALLOCATIONS.LIST").toString();
//						//\System.out.println("the String is Object:"+op);
//						JSONObject billl=bill.getJSONObject("BANKALLOCATIONS.LIST");
//						System.out.println("bank"+" "+billl.get("DATE")+" "+billl.getString("TRANSACTIONTYPE")+" "+billl.getString("PAYMENTFAVOURING")+" "+billl.getString("AMOUNT")+" "+billl.getString("PAYMENTFAVOURING")+" "+billl.getString("PAYMENTFAVOURING"));
//						//System.out.println("ok tasted 1");
//
//					}
//					else if(ba.charAt(0)=='[')
//					{
//						System.out.print("ok tasted 2");
//						ba=bill.getString("BANKALLOCATIONS.LIST").toString();
//
//						//System.out.println("the String is Array:"+op);
//						String reString=bill.toString();
//						//System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
//						JSONArray billlist=bill.getJSONArray("BANKALLOCATIONS.LIST");
//						//System.out.println("ok tasted 3");
//						for(int b=0;b<billlist.length();b++)
//						{
//							JSONObject billItems=billlist.getJSONObject(b);
//							System.out.println("bank"+" "+billItems.get("DATE")+" "+billItems.getString("TRANSACTIONTYPE")+" "+billItems.getString("PAYMENTFAVOURING")+" "+billItems.getString("AMOUNT")+" "+billItems.getString("PAYMENTFAVOURING")+" "+billItems.getString("PAYMENTFAVOURING"));
//
//							}
//						//System.out.println("ok tasted 4");
//						//e.printStackTrace();
//
//					}else
//					{
//						System.out.println("ok null");
//					}
//
//				}
////
//				}
//				break;
//			case Purchase:
//			{
//				JSONArray list=voucher.getJSONArray("LEDGERENTRIES.LIST");
//
//				for(int j=0;j<list.length();j++)
//				{
//
//					JSONObject bill=list.getJSONObject(j);
//					//System.out.println(bill);
//					System.out.println(bill.getString("AMOUNT")+" "+bill.getString("LEDGERNAME")+" ");
//					String op=bill.getString("BILLALLOCATIONS.LIST").toString();
//					//System.out.println("the String is Object:"+op);
//					if(!op.equals(null)&&!op.isEmpty())
//					if(op.charAt(0)=='{')
//					{
//						op=bill.getString("BILLALLOCATIONS.LIST").toString();
//						//System.out.println("the String is Object:"+op);
//						JSONObject billl=bill.getJSONObject("BILLALLOCATIONS.LIST");
//						System.out.println(billl.get("NAME")+" "+billl.getString("AMOUNT")+" "+billl.getString("BILLTYPE")+" "+billl.getString("BILLCREDITPERIOD")+" ");
//						//System.out.println("ok tasted 1");
//
//					}
//					else if(op.charAt(0)=='[')
//					{
//						System.out.println("ok tasted 2");
//						op=bill.getString("BILLALLOCATIONS.LIST").toString();
//
//						//System.out.println("the String is Array:"+op);
//						String reString=bill.toString();
//						//System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
//						JSONArray billlist=bill.getJSONArray("BILLALLOCATIONS.LIST");
//						//System.out.println("ok tasted 3");
//						for(int b=0;b<billlist.length();b++)
//						{
//							JSONObject billItems=billlist.getJSONObject(b);
//							System.out.println("bill"+" "+billItems.get("NAME")+" "+billItems.get("BILLTYPE")+" "+billItems.get("AMOUNT")+" "+billItems.getString("BILLCREDITPERIOD")+" ");
//						}
//						//System.out.println("ok tasted 4");
//						//e.printStackTrace();
//
//					}else
//					{
//						System.out.println("ok null");
//					}
//					String ba=bill.getString("BANKALLOCATIONS.LIST").toString();
//					//System.out.println("the String is Object:"+op);
//					if(!ba.equals(null)&&!ba.isEmpty())
//					if(ba.charAt(0)=='{')
//					{
//						ba=bill.getString("BANKALLOCATIONS.LIST").toString();
//						//\System.out.println("the String is Object:"+op);
//						JSONObject billl=bill.getJSONObject("BANKALLOCATIONS.LIST");
//						System.out.println("bank"+" "+billl.get("DATE")+" "+billl.getString("TRANSACTIONTYPE")+" "+billl.getString("PAYMENTFAVOURING")+" "+billl.getString("AMOUNT")+" "+billl.getString("PAYMENTFAVOURING")+" "+billl.getString("PAYMENTFAVOURING"));
//						//System.out.println("ok tasted 1");
//
//					}
//					else if(ba.charAt(0)=='[')
//					{
//						System.out.print("ok tasted 2");
//						ba=bill.getString("BANKALLOCATIONS.LIST").toString();
//
//						//System.out.println("the String is Array:"+op);
//						String reString=bill.toString();
//						//System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
//						JSONArray billlist=bill.getJSONArray("BANKALLOCATIONS.LIST");
//						//System.out.println("ok tasted 3");
//						for(int b=0;b<billlist.length();b++)
//						{
//							JSONObject billItems=billlist.getJSONObject(b);
//							System.out.println("bank"+" "+billItems.get("DATE")+" "+billItems.getString("TRANSACTIONTYPE")+" "+billItems.getString("PAYMENTFAVOURING")+" "+billItems.getString("AMOUNT")+" "+billItems.getString("PAYMENTFAVOURING")+" "+billItems.getString("PAYMENTFAVOURING"));
//
//							}
//						//System.out.println("ok tasted 4");
//						//e.printStackTrace();
//
//					}else
//					{
//						System.out.println("ok null");
//					}
//
//				}

//				}
//				break;
//			case Sales:
//			{
//				JSONArray list=voucher.getJSONArray("ALLLEDGERENTRIES.LIST");
//
//								for(int j=0;j<list.length();j++)
//								{
//
//									JSONObject bill=list.getJSONObject(j);
//									//System.out.println(bill);
//									System.out.println(bill.getString("AMOUNT")+" "+bill.getString("LEDGERNAME")+" ");
//									String op=bill.getString("BILLALLOCATIONS.LIST").toString();
//									//System.out.println("the String is Object:"+op);
//									if(!op.equals(null)&&!op.isEmpty())
//									if(op.charAt(0)=='{')
//									{
//										op=bill.getString("BILLALLOCATIONS.LIST").toString();
//										//System.out.println("the String is Object:"+op);
//										JSONObject billl=bill.getJSONObject("BILLALLOCATIONS.LIST");
//										System.out.println(billl.get("NAME")+" "+billl.getString("AMOUNT")+" "+billl.getString("BILLTYPE")+" "+billl.getString("BILLCREDITPERIOD")+" ");
//										//System.out.println("ok tasted 1");
//
//									}
//									else if(op.charAt(0)=='[')
//									{
//										System.out.println("ok tasted 2");
//										op=bill.getString("BILLALLOCATIONS.LIST").toString();
//
//										//System.out.println("the String is Array:"+op);
//										String reString=bill.toString();
//										//System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
//										JSONArray billlist=bill.getJSONArray("BILLALLOCATIONS.LIST");
//										//System.out.println("ok tasted 3");
//										for(int b=0;b<billlist.length();b++)
//										{
//											JSONObject billItems=billlist.getJSONObject(b);
//											System.out.println("bill"+" "+billItems.get("NAME")+" "+billItems.get("BILLTYPE")+" "+billItems.get("AMOUNT")+" "+billItems.getString("BILLCREDITPERIOD")+" ");
//										}
//										//System.out.println("ok tasted 4");
//										//e.printStackTrace();
//
//									}else
//									{
//										System.out.println("ok null");
//									}
//									String ba=bill.getString("BANKALLOCATIONS.LIST").toString();
//									//System.out.println("the String is Object:"+op);
//									if(!ba.equals(null)&&!ba.isEmpty())
//									if(ba.charAt(0)=='{')
//									{
//										ba=bill.getString("BANKALLOCATIONS.LIST").toString();
//										//\System.out.println("the String is Object:"+op);
//										JSONObject billl=bill.getJSONObject("BANKALLOCATIONS.LIST");
//										System.out.println("bank"+" "+billl.get("DATE")+" "+billl.getString("TRANSACTIONTYPE")+" "+billl.getString("PAYMENTFAVOURING")+" "+billl.getString("AMOUNT")+" "+billl.getString("PAYMENTFAVOURING")+" "+billl.getString("PAYMENTFAVOURING"));
//										//System.out.println("ok tasted 1");
//
//									}
//									else if(ba.charAt(0)=='[')
//									{
//										System.out.print("ok tasted 2");
//										ba=bill.getString("BANKALLOCATIONS.LIST").toString();
//
//										//System.out.println("the String is Array:"+op);
//										String reString=bill.toString();
//										//System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
//										JSONArray billlist=bill.getJSONArray("BANKALLOCATIONS.LIST");
//										//System.out.println("ok tasted 3");
//										for(int b=0;b<billlist.length();b++)
//										{
//											JSONObject billItems=billlist.getJSONObject(b);
//											System.out.println("bank"+" "+billItems.get("DATE")+" "+billItems.getString("TRANSACTIONTYPE")+" "+billItems.getString("PAYMENTFAVOURING")+" "+billItems.getString("AMOUNT")+" "+billItems.getString("PAYMENTFAVOURING")+" "+billItems.getString("PAYMENTFAVOURING"));
//
//											}
//										//System.out.println("ok tasted 4");
//										//e.printStackTrace();
//
//									}else
//									{
//										System.out.println("ok null");
//									}
//
//								}

//								}
//				break;
                    if (String.valueOf(voucher.get("VOUCHERTYPENAME")).equals("Sales")) {
                        items = getAllLedgerItem(voucher);
                    }
                    salesVouchers.add(new SalesVoucher(voucherDate, voucherPartyName, voucherTypeName, "Sales", voucherNumber, voucherNarration, inventoriesList, null, items));
                }

            }
            synchronized (this)
            {
                notify();
            }

        } catch (Exception e) {
            e.getMessage();
        }
        if (salesVouchers.size() == 0) {
            salesVouchers = new ArrayList<>();
        }
        return salesVouchers;
    }

    List<LedgerItem> getAllLedgerItem(JSONObject voucher) {
        String voucherDate;
        String voucherPartyName;
        String voucherNumber;
        String voucherNarration;
        String voucherTypeName;
        String basicUserDesc;
        Double rate;
        String amount;
        int edqty;
        int actualQty;

        String stockName;
        String gowdownName;
        String batchName;
        Double batchamount;
        int batchactualQty;
        int billQty;
        Date orderDueDate;
        List<LedgerItem> items = null;
        List<Item> inventories = null;
        Double basicOrderRate;
        String orderType;
        String basicPurchaseOrderNumber;
        Double orderamount;
        String ledgerName;
        String billName;
        String billAmount;
        String billType;
        String billCreditPeriod;
        List<SalesVoucher> salesVouchers = new ArrayList<>();
        List<InvoiceOrderList> invoiceOrderLists = null;
        List<Inventories> inventoriesList = null;
        ArrayList<LedgerItem> listItes = new ArrayList<>();
        JSONArray list = voucher.getJSONArray("ALLLEDGERENTRIES.LIST");
        items = new ArrayList<>();
        for (int j = 0; j < list.length(); j++) {
            JSONObject bill = list.getJSONObject(j);
            //System.out.println(bill);
            inventories = new ArrayList<>();
            amount = String.valueOf(bill.get("AMOUNT"));
            ledgerName = String.valueOf(bill.getString("LEDGERNAME"));
            System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");
            String op = bill.get("BILLALLOCATIONS.LIST").toString();
            //System.out.println("the String is Object:"+op);
            if (!op.equals(null) && !op.isEmpty())
                if (op.charAt(0) == '{') {

                    inventories = new ArrayList<>();
                    // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                    //System.out.println("the String is Object:"+op);
                    JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                    System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                    //System.out.println("ok tasted 1");
                    billAmount = String.valueOf(billl.get("AMOUNT"));
                    billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                    billType = String.valueOf(billl.getString("BILLTYPE"));
                    billName = String.valueOf(billl.get("NAME"));
                    inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                } else if (op.charAt(0) == '[') {
                    System.out.println("ok tasted 2");
                    // op=bill.get("BILLALLOCATIONS.LIST").toString();
                    inventories = new ArrayList<>();
                    //System.out.println("the String is Array:"+op);
                    String reString = bill.toString();
                    //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                    JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                    //System.out.println("ok tasted 3");
                    for (int b = 0; b < billlist.length(); b++) {
                        JSONObject billItems = billlist.getJSONObject(b);
                        billAmount = String.valueOf(billItems.get("AMOUNT"));
                        billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                        billType = String.valueOf(billItems.getString("BILLTYPE"));
                        billName = String.valueOf(billItems.get("NAME"));
                        inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                        System.out.println("bill" + " " + billItems.get("NAME") + " " + billItems.get("BILLTYPE") + " " + billItems.get("AMOUNT") + " " + billItems.getString("BILLCREDITPERIOD") + " ");
                    }
                    //System.out.println("ok tasted 4");
                    //e.printStackTrace();

                } else {
                    items = new ArrayList<>();
                    System.out.println("ok null");
                }
            String ba = bill.getString("BANKALLOCATIONS.LIST").toString();
            //System.out.println("the String is Object:"+op);
            if (!ba.equals(null) && !ba.isEmpty())
                if (ba.charAt(0) == '{') {
                    ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                    //\System.out.println("the String is Object:"+op);
                    JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                    System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                    //System.out.println("ok tasted 1");

                } else if (ba.charAt(0) == '[') {
                    System.out.print("ok tasted 2");
                    ba = bill.getString("BANKALLOCATIONS.LIST").toString();

                    //System.out.println("the String is Array:"+op);
                    String reString = bill.toString();
                    //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                    JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                    //System.out.println("ok tasted 3");
                    for (int b = 0; b < billlist.length(); b++) {
                        JSONObject billItems = billlist.getJSONObject(b);
                        System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                    }
                    //System.out.println("ok tasted 4");
                    //e.printStackTrace();

                } else {
                    System.out.println("ok null");
                }
            listItes.add(new LedgerItem(amount, ledgerName, inventories));
        }

        return listItes;
    }





}
