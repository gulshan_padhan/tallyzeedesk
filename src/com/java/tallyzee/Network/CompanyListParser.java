package com.java.tallyzee.Network;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import net.projectmonkey.object.mapper.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import com.java.tallyzee.Utility.AppParserUtil;

import java.util.ArrayList;
import java.util.HashSet;

public class CompanyListParser extends Thread{
    static HashSet<String> companyList=new HashSet<>();
    CompanyListParser(String userName)
    {
        this.userName=userName;
    }
    public static final String  urlParameters= "<ENVELOPE><HEADER><VERSION>1</VERSION><TALLYREQUEST>Export</TALLYREQUEST><TYPE>Data</TYPE><ID>LIST OF COMPANIES</ID></HEADER><BODY><DESC><STATICVARIABLES><EXPLODEFLAG>Yes</EXPLODEFLAG><SVFROMDATE TYPE=\"Date\">1-1-2017</SVFROMDATE>   <SVTODATE TYPE=\"Date\">1-1-2018</SVTODATE><SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT></STATICVARIABLES></DESC></BODY></ENVELOPE>";    String userName,companyName;
//public static void main(String[]args)
//{
//    CompanyListParser cmop=new CompanyListParser("-649514226");
//    cmop.actionPerformed();
//}
    @Override
    public void run() {
        actionPerformed();
    }
    public void actionPerformed() {
        String test = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">20170404</SVFROMDATE>   <SVTODATE TYPE=\"Date\">20170404</SVTODATE>                 </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsSales:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";

        String name = "", email;
        System.out.println("OUT OF EXCEPTION");

//        try {
////            ClassLoader classLoader = getObject().getClass().getClassLoader();
////            File fi = new File(String.valueOf(FirebaseAuthUtil.class.getResourceAsStream("/com/java/tallyzee/Utility/admin-sdk.json")));
//
//            FileInputStream serviceAccount = new FileInputStream("C:\\Users\\dev9\\Desktop\\admin-sdk.json");
//            // Initialize the app with a service account, granting admin privileges
//            FirebaseOptions options = new FirebaseOptions.Builder()
//                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
//                    .setDatabaseUrl("https://tallyfy-364a0.firebaseio.com/")
//                    .build();
//            FirebaseApp.initializeApp(options);
//        } catch (Exception e)
//        {
//
//        }

        ObjectMapper mapper = new ObjectMapper();
//        DatabaseReference ref = FirebaseDatabase.getInstance()
//                .getReferenceFromUrl("https://tallyfy-364a0.firebaseio.com");
        String fromDay, toDay;
        String year = AppParserUtil.getYear();
        int count = 0;
        ArrayList<String> vouchernumber = new ArrayList<>();
        ArrayList<String> vouchernumber1 = new ArrayList<>();

                HashSet<String> salesVouchers = new HashSet<>();
                //System.out.println(getPayRoll(fromDay, fromDay));
                salesVouchers = (HashSet<String>) getList(test(NetworkServices.getResponse(urlParameters)));
                System.out.println("the" + salesVouchers + "response size is:" + salesVouchers.size());

                if (salesVouchers.size() > 0) {
                    System.out.println("the company name is:"+companyName+" "+userName);
                    for (int i = 0; i < salesVouchers.size(); i++) {
                        final int temp = i;

//                        ref.child("Users").child(userName).child(String.valueOf(count)).setValue(salesVouchers.iterator(), new DatabaseReference.CompletionListener() {
//                            @Override
//                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                                vouchernumber.add("ok");
//                            }
//                        });
                        count++;
                    }
                }
                try {
                    //  Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }



        vouchernumber.forEach((response) -> {
            System.out.println(response);
        });
        System.out.println(vouchernumber.size() + " " + vouchernumber1.size());
    }

    public static String test(String responseXml) {
        JSONObject obj = null;
        try {
            obj = XML.toJSONObject(responseXml);
            System.out.println("json" + obj.toString());
            //printJson(obj.toString());
        } catch (Exception e) {
            System.out.println(e);
        }
        return obj.toString();
    }


    public  HashSet<String> getList(String json)
    {
        System.out.println(json);
        JSONObject main=new JSONObject(json);
        JSONObject envlop = main.getJSONObject("ENVELOPE");
        JSONObject body = envlop.getJSONObject("COMPANYNAME.LIST");
        if(String.valueOf(body).contains("COMPANYNAME"))
        {
            String var=String.valueOf(body.get("COMPANYNAME"));
            if(var.length()>0&&var.charAt(0)!='[')
            {
                String cmpname= String.valueOf(body.get("COMPANYNAME"));
                companyList.add(cmpname);
            }
            else if(var.charAt(0)=='[')
            {
                JSONArray ar=body.getJSONArray("COMPANYNAME");
                for(int i=0;i<ar.length();i++)
                {
                    companyList.add(String.valueOf(ar.get(i)));
                }
            }
        }


        return companyList;
    }
}
