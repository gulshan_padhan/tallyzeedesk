package com.java.tallyzee.Network;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.java.tallyzee.Models.SalesCallBcak;
import net.projectmonkey.object.mapper.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import rx.Observable;
import rx.schedulers.Schedulers;
import com.java.tallyzee.Models.GstRates;
import com.java.tallyzee.Models.StockItem;

import java.util.ArrayList;
import java.util.List;

public class Stockparser implements Runnable {
    //String

       String userName, companyName;
SalesCallBcak salesCallBcak;
    Stockparser(String userName, String companyName,ParserController parserController) {
        this.userName = userName;
        this.companyName = companyName;
        salesCallBcak=parserController;
    }
String getPayLoad()
{
    return  "<ENVELOPE><HEADER><VERSION>1</VERSION><TALLYREQUEST>Export</TALLYREQUEST><TYPE>Data</TYPE><ID>stock summary</ID></HEADER><BODY><DESC><STATICVARIABLES><EXPLODEFLAG>Yes</EXPLODEFLAG><SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT> <SVCURRENTCOMPANY>"+companyName+"</SVCURRENTCOMPANY></STATICVARIABLES></DESC></BODY></ENVELOPE>";

}
    Stockparser() {

    }

    @Override
    public void run() {
        salesCallBcak.getStockResponse((ArrayList<StockItem>) pushData());
    }

    public String test(String responseXml) {
        JSONObject obj = null;
        try {
            obj = XML.toJSONObject(responseXml);
            System.out.println("json" + obj.toString());
            //printJson(obj.toString());
        } catch (Exception e) {
            System.out.println(e);
        }
        return obj.toString();
    }

    public List<StockItem> pushData() {
        // System.out.println(getListOFBill(test(NetworkServices.getResponse(xml)))); ;
        // ArrayList<ProfinAndLoss> list=getResponseLedger()
        ArrayList<String> list = (ArrayList<String>) getListOFBill(test(com.java.tallyzee.Network.NetworkServices.getResponse(getPayLoad())));
        ArrayList<StockItem> profinAndLosses = (ArrayList<StockItem>) getResponseLedger(list);
        System.out.println(profinAndLosses);
        System.out.println(profinAndLosses.size());
//        ObjectMapper mapper = new ObjectMapper();
//        DatabaseReference ref = FirebaseDatabase.getInstance()
//                .getReferenceFromUrl("https://tallyfy-364a0.firebaseio.com");
//        for (int i = 0; i < profinAndLosses.size(); i++) {
//            System.out.println(profinAndLosses.get(i).toString());
//            ref.child("Users").child(userName).child("companyListData").child(companyName.replaceAll("[\\-\\+\\.\\^:,]","")).child("StockItem").child(String.valueOf(i)).setValue(profinAndLosses.get(i), new DatabaseReference.CompletionListener() {
////            ref.child("Demo Company").child("StockItem").child(String.valueOf(i)).setValue(profinAndLosses.get(i), new DatabaseReference.CompletionListener() {
//                @Override
//                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                    System.out.println("ok");
//                }
//            });
//
//        }

        return profinAndLosses;
    }

    public static void test(ArrayList<BillsRecevieables> list) {

        //  PublishSubject<BillsRecevieables> list1=PublishSubject.create();
        Observable.from(list).subscribeOn(Schedulers.newThread())
                .subscribe(resultsObject -> {
                    System.out.println("ok");
                });
    }

    public List<StockItem> getResponseLedger(List<String> listt) {
        final ArrayList<String> list = (ArrayList<String>) listt;
        ArrayList<StockItem> prf = new ArrayList();


        StockItem t, t2;

        for (int i = 0; i < list.size(); i++) {
//                System.out.println(test(NetworkServices.getResponse(getLedgerXmlforGroup(list.get(i)))));
//                System.out.println(test(NetworkServices.getResponse(getLedgerXmlforLedger(list.get(i)))));
            t = getLedger(test(NetworkServices.getResponse(getLedgerXmlforGroup(list.get(i)))));
            if (t != null) {
                prf.add(t);
            }
            t2 = getLedger(test(NetworkServices.getResponse(getLedgerXmlforLedger(list.get(i)))));
            if (t2 != null) {
                prf.add(t2);
            }
        }


        return prf;
    }

    public String getLedgerXmlforLedger(String partyName) {
        String xmlforLedgerGroup = "<ENVELOPE>\n\n<HEADER>\n\n<VERSION>1</VERSION>\n\n<TALLYREQUEST>EXPORT</TALLYREQUEST>\n\n<TYPE>OBJECT</TYPE> <SUBTYPE>Stock Group</SUBTYPE> <ID TYPE=\"Name\">" + partyName + "</ID>\n\n</HEADER>\n\n<BODY>\n\n<DESC>\n\n<STATICVARIABLES>\n\t<SVFROMDATE TYPE=\"Date\">1-1-2016</SVFROMDATE>   <SVTODATE TYPE=\"Date\">1-1-2022</SVTODATE>\n\t<SVCURRENTCOMPANY>"+companyName+"</SVCURRENTCOMPANY><SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT></STATICVARIABLES>\n\n<FETCHLIST>\n\n<FETCH>Name</FETCH>\n\n<FETCH>TNetBalance</FETCH>\n<FETCH>closingbalance</FETCH>\n<FETCH>OPENINGBALANCE</FETCH>\n<FETCH>category</FETCH>\n<FETCH>OPENINGVALUE</FETCH>\n<FETCH>REORDERBASE</FETCH>\n<FETCH>MINIMUMORDERBASE</FETCH>\n<FETCH>OPENINGRATE</FETCH>\n<FETCH>Parent</FETCH>\n<FETCH>GSTRATE</FETCH>\n<FETCH>MULTICOMPONENTLIST</FETCH>\n<FETCH>STANDARDPRICELIST</FETCH>\n<FETCH>STANDARDCOSTLIST</FETCH>\n<FETCH>GSTDETAILS</FETCH>\n</FETCHLIST>\n\n<TDL>\n\n<TDLMESSAGE>\n\n<OBJECT NAME=\"Stock Group\" ISINITIALIZE=\"Yes\">\n\n<LOCALFORMULA>\n\nTNetBalance: $$AsPositive:$$AmountSubtract:$ClosingBalance:$OpeningBalance\n\n</LOCALFORMULA>\n\n</OBJECT>\n\n</TDLMESSAGE>\n\n</TDL>\n\n</DESC>\n\n</BODY>\n\n</ENVELOPE>";
        return xmlforLedgerGroup;
    }

    public String getLedgerXmlforGroup(String partyName) {
        String xmlforLedgerOnly = "<ENVELOPE>\n\n<HEADER>\n\n<VERSION>1</VERSION>\n\n<TALLYREQUEST>EXPORT</TALLYREQUEST>\n\n<TYPE>OBJECT</TYPE> <SUBTYPE>Stock Item</SUBTYPE> <ID TYPE=\"Name\">" + partyName + "</ID>\n\n</HEADER>\n\n<BODY>\n\n<DESC>\n\n<STATICVARIABLES>\n\t<SVFROMDATE TYPE=\"Date\">1-1-2016</SVFROMDATE>   <SVTODATE TYPE=\"Date\">1-1-2022</SVTODATE>\n\t <SVCURRENTCOMPANY>"+companyName+"</SVCURRENTCOMPANY><SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT></STATICVARIABLES>\n\n<FETCHLIST>\n\n<FETCH>Name</FETCH>\n\n<FETCH>TNetBalance</FETCH>\n<FETCH>closingbalance</FETCH>\n<FETCH>OPENINGBALANCE</FETCH>\n<FETCH>category</FETCH>\n<FETCH>OPENINGVALUE</FETCH>\n<FETCH>REORDERBASE</FETCH>\n<FETCH>MINIMUMORDERBASE</FETCH>\n<FETCH>OPENINGRATE</FETCH>\n<FETCH>Parent</FETCH>\n<FETCH>GSTRATE</FETCH>\n<FETCH>MULTICOMPONENTLIST</FETCH>\n<FETCH>STANDARDPRICELIST</FETCH>\n<FETCH>STANDARDCOSTLIST</FETCH>\n<FETCH>GSTDETAILS</FETCH>\n</FETCHLIST>\n\n<TDL>\n\n<TDLMESSAGE>\n\n<OBJECT NAME=\"Stock Item\" ISINITIALIZE=\"Yes\">\n\n<LOCALFORMULA>\n\nTNetBalance: $$AsPositive:$$AmountSubtract:$ClosingBalance:$OpeningBalance\n\n</LOCALFORMULA>\n\n</OBJECT>\n\n</TDLMESSAGE>\n\n</TDL>\n\n</DESC>\n\n</BODY>\n\n</ENVELOPE>";
        return xmlforLedgerOnly;
    }

    static StockItem getLedger(String json) {
        String parentName = "";
        String ledgerAmount = "";
        String category="";
        String ledgerName = "";
        String openingBalancevalueRate = "";
        String openingBalancevalue = "";
        String name = "";
        String stockName = "";
        String closingBal = "";
        String rateA = "";
        String dateA = "";
        String reOrderAmount = "0.00";
        String type = "";
        String hsn = "null";
        ArrayList<GstRates> list = new ArrayList();
        System.out.println(json);
        if (json.contains("ENVELOPE")) {
            JSONObject main = new JSONObject(json);
            JSONObject envlop = main.getJSONObject("ENVELOPE");
            JSONObject body = envlop.getJSONObject("BODY");
            JSONObject data = body.getJSONObject("DATA");
            JSONObject tallymsg = data.getJSONObject("TALLYMESSAGE");
            if (String.valueOf(tallymsg).contains("STOCKGROUP")) {
                JSONObject stockGroup = tallymsg.getJSONObject("STOCKGROUP");
                type = "Group";
                JSONObject openingBalance = stockGroup.getJSONObject("OPENINGBALANCE");
                JSONObject openingValue = stockGroup.getJSONObject("OPENINGVALUE");
                // JSONObject group = stockGroup.getJSONObject("GROUP");

                if (String.valueOf(openingBalance).contains("content")) {
                    openingBalancevalueRate = String.valueOf(openingBalance.get("content"));
                } else {
                    openingBalancevalueRate = "0.00";
                }

                if (String.valueOf(openingValue).contains("content")) {
                    openingBalancevalue = String.valueOf(openingValue.get("content"));
                } else {
                    openingBalancevalue = "0.00";
                }

                JSONObject parent = stockGroup.getJSONObject("PARENT");
                parentName = String.valueOf(parent.get("content"));
//                JSONObject categoryObj=stockGroup.getJSONObject("CATEGORY");
//
//                category=String.valueOf(categoryObj.get("content"));
                JSONObject closingBalance = stockGroup.getJSONObject("CLOSINGBALANCE");

                if (String.valueOf(closingBalance).contains("content")) {
                    name = String.valueOf(closingBalance.get("content"));
                } else {
                    name = "null";
                }

                // JSONObject amount = group.getJSONObject("TNETBALANCE");
                // ledgerAmount = String.valueOf(amount.get("content"));
                //stockName = String.valueOf(stockGroup.get("NAME"));
                //ledgerName = String.valueOf(group.get("NAME"));
                //public StockItem(String stockItemName, String parentName, String closingBalance, String openingBalance, String openingValue, String reorderValue, String minimumOrderBase, String openingRate, String sotckType) {
                //StockItem();
                // return  new StockItem(stockName,parentName,closingBalance,openingBalancevalue,openingBalancevalueRate,openingBalancevalue);
                // return new ProfinAndLoss(ledgerName, parentName, ledgerAmount, "");
            } else if (String.valueOf(tallymsg).contains("STOCKITEM")) {
                JSONObject group = tallymsg.getJSONObject("STOCKITEM");
                JSONObject parent=null;
                if(String.valueOf(group).contains("PARENT"))
                {
                    parent = group.getJSONObject("PARENT");
                    parentName = String.valueOf(parent.get("content"));
                }
                else
                {
                    parentName=null;
                }
                JSONObject categoryObj=null;
                if(String.valueOf(group).contains("CATEGORY"))
                {
                    categoryObj=group.getJSONObject("CATEGORY");
                    category=String.valueOf(categoryObj.get("content"));
                }
              else
                {
                    category=null;
                }

                if(String.valueOf(group).contains("OPENINGBALANCE"))
                {
                    JSONObject openingBalance = group.getJSONObject("OPENINGBALANCE");
                    if (String.valueOf(openingBalance).contains("content")) {
                        openingBalancevalueRate = String.valueOf(openingBalance.get("content"));
                    } else {
                        openingBalancevalueRate = "0.00";
                    }
                }else {
                    openingBalancevalueRate="0.00";
                }
                if(String.valueOf(group).contains("OPENINGVALUE"))
                {
                    JSONObject openingValue = group.getJSONObject("OPENINGVALUE");


                    if (String.valueOf(openingValue).contains("content")) {
                        openingBalancevalue = String.valueOf(openingValue.get("content"));
                    } else {
                        openingBalancevalue = "0.00";
                    }
                }else
                {
                    openingBalancevalue = "0.00";
                }

                type = "Item";
                // JSONObject amount = group.getJSONObject("TNETBALANCE");

                if (String.valueOf(group).contains("STANDARDPRICELIST.LIST")) {
                    JSONObject standardcost = group.getJSONObject("STANDARDPRICELIST.LIST");
                    JSONObject date = standardcost.getJSONObject("DATE");
                    JSONObject rate = standardcost.getJSONObject("RATE");
                    rateA = String.valueOf(rate.get("content"));
                    dateA = String.valueOf(date.get("content"));
                    //ledgerAmount = String.valueOf(amount.get("content"));


                }

                if (String.valueOf(group).contains("REORDERBASE")) {
                    JSONObject reOrder = group.getJSONObject("REORDERBASE");
                    if (String.valueOf(reOrder).contains("content"))
                        reOrderAmount = String.valueOf(reOrder.get("content"));
                    else
                        reOrderAmount = "0.00";
                }


                if (String.valueOf(group).contains("GSTDETAILS.LIST")) {
                    if (String.valueOf(group.get("GSTDETAILS.LIST")).charAt(0) == '{') {
                        JSONObject gstDetails = group.getJSONObject("GSTDETAILS.LIST");
                        JSONObject hsnobj = gstDetails.getJSONObject("HSNCODE");
                        if (String.valueOf(hsnobj).contains("content"))
                            hsn = String.valueOf(hsnobj.get("content"));
                        else
                            hsn = "null";

                        if (String.valueOf(gstDetails).contains("STATEWISEDETAILS.LIST")) {
                            JSONObject stawise = gstDetails.getJSONObject("STATEWISEDETAILS.LIST");
                            if (String.valueOf(stawise.get("RATEDETAILS.LIST")).charAt(0) == '{') {

                            } else if (String.valueOf(stawise.get("RATEDETAILS.LIST")).charAt(0) == '[') {
                                JSONArray rate = stawise.getJSONArray("RATEDETAILS.LIST");
                                String gstratePerUnit;
                                String gstRateDutyHead;
                                String gstRateValuetionType;
                                String gstRate;
                                list = new ArrayList();
                                for (int j = 0; j < rate.length(); j++) {
                                    JSONObject single = rate.getJSONObject(j);
                                    JSONObject GSTRATEPERUNIT = single.getJSONObject("GSTRATEPERUNIT");
                                    JSONObject GSTRATEDUTYHEAD = single.getJSONObject("GSTRATEDUTYHEAD");
                                    JSONObject GSTRATEVALUATIONTYPE = single.getJSONObject("GSTRATEVALUATIONTYPE");
                                    JSONObject GSTRAT = single.getJSONObject("GSTRATE");
                                    if (String.valueOf(GSTRATEVALUATIONTYPE).contains("content"))
                                        gstRateValuetionType = String.valueOf(GSTRATEVALUATIONTYPE.get("content"));
                                    else
                                        gstRateValuetionType = "";
                                    if (String.valueOf(GSTRATEDUTYHEAD).contains("content"))
                                        gstRateDutyHead = String.valueOf(GSTRATEDUTYHEAD.get("content"));
                                    else
                                        gstRateDutyHead = "";
                                    if (String.valueOf(GSTRATEPERUNIT).contains("content"))
                                        gstratePerUnit = String.valueOf(GSTRATEPERUNIT.get("content"));
                                    else
                                        gstratePerUnit = "";
                                    if (String.valueOf(GSTRAT).contains("content"))
                                        gstRate = String.valueOf(GSTRAT.get("content"));
                                    else
                                        gstRate = "";
                                    System.out.println(gstRate + " " + gstRateDutyHead + " " + gstratePerUnit + " " + gstRateValuetionType);

                                    //public GstRates(String GSTRATEPERUNIT, String GSTRATEDUTYHEAD, String GSTRATEVALUATIONTYPE, String GSTRAT)
                                    list.add(new GstRates(gstratePerUnit, gstRateDutyHead, gstRateValuetionType, gstRate));
                                }

                            }

                        }
                        //public GstRates(String GSTRATEPERUNIT, String GSTRATEDUTYHEAD, String GSTRATEVALUATIONTYPE, String GSTRAT)
                        // list.add(new GstRates());
                    } else if (String.valueOf(group.get("GSTDETAILS.LIST")).charAt(0) == '[') {

                        JSONArray gstDetails = group.getJSONArray("GSTDETAILS.LIST");
                        for (int i = 0; i < gstDetails.length(); i++) {
                            JSONObject jsonObject = gstDetails.getJSONObject(i);
                            JSONObject hsnobj = jsonObject.getJSONObject("HSNCODE");
                            if (String.valueOf(hsnobj).contains("content"))
                                hsn = String.valueOf(hsnobj.get("content"));
                            else
                                hsn = "null";

                            if (String.valueOf(jsonObject).contains("STATEWISEDETAILS.LIST")) {
                                JSONObject stawise = jsonObject.getJSONObject("STATEWISEDETAILS.LIST");
                                if (String.valueOf(stawise.get("RATEDETAILS.LIST")).charAt(0) == '{') {

                                } else if (String.valueOf(stawise.get("RATEDETAILS.LIST")).charAt(0) == '[') {
                                    JSONArray rate = stawise.getJSONArray("RATEDETAILS.LIST");
                                    String gstratePerUnit;
                                    String gstRateDutyHead;
                                    String gstRateValuetionType;
                                    String gstRate;
                                    list = new ArrayList<>();
                                    for (int j = 0; j < rate.length(); j++) {
                                        JSONObject single = rate.getJSONObject(j);
                                        JSONObject GSTRATEPERUNIT = single.getJSONObject("GSTRATEPERUNIT");
                                        JSONObject GSTRATEDUTYHEAD = single.getJSONObject("GSTRATEDUTYHEAD");
                                        JSONObject GSTRATEVALUATIONTYPE = single.getJSONObject("GSTRATEVALUATIONTYPE");
                                        JSONObject GSTRAT = single.getJSONObject("GSTRATE");
                                        if (String.valueOf(GSTRATEVALUATIONTYPE).contains("content"))
                                            gstRateValuetionType = String.valueOf(GSTRATEVALUATIONTYPE.get("content"));
                                        else
                                            gstRateValuetionType = "";
                                        if (String.valueOf(GSTRATEDUTYHEAD).contains("content"))
                                            gstRateDutyHead = String.valueOf(GSTRATEDUTYHEAD.get("content"));
                                        else
                                            gstRateDutyHead = "";
                                        if (String.valueOf(GSTRATEPERUNIT).contains("content"))
                                            gstratePerUnit = String.valueOf(GSTRATEPERUNIT.get("content"));
                                        else
                                            gstratePerUnit = "";
                                        if (String.valueOf(GSTRAT).contains("content"))
                                            gstRate = String.valueOf(GSTRAT.get("content"));
                                        else
                                            gstRate = "";
                                        System.out.println(gstRate + " " + gstRateDutyHead + " " + gstratePerUnit + " " + gstRateValuetionType);
                                        list.add(new GstRates(gstratePerUnit, gstRateDutyHead, gstRateValuetionType, gstRate));
                                    }

                                }

                            }

                        }
                    }


                }
                try{
                if(String.valueOf(group).contains("NAME"))
                {
                    ledgerName = String.valueOf(group.get("NAME"));
                }
                else
                {
                    ledgerName=null;
                }}catch (Exception e)
                {
                    ledgerName=null;
                }
                System.out.println("The Ledger Value is:" + ledgerAmount + " " + ledgerName + " " + parentName + " " + rateA + " " + dateA + " " + reOrderAmount + " " + hsn);
                JSONObject closingBalance=null;
                if(String.valueOf(group).contains("CLOSINGBALANCE"))
                {
                    closingBalance = group.getJSONObject("CLOSINGBALANCE");
                    if (String.valueOf(closingBalance).contains("content"))
                        closingBal = String.valueOf(closingBalance.get("content"));
                    else
                        closingBal = null;
                }
                else {
                    closingBal=null;
                }

                // return new ProfinAndLoss(ledgerName, parentName, ledgerAmount, "");
            }
            return new StockItem(type, dateA, rateA, hsn, ledgerName, parentName, closingBal, openingBalancevalueRate, openingBalancevalue, reOrderAmount, reOrderAmount, openingBalancevalueRate,category, list);

        }
        return null;
    }

    static List<String> getListOFBill(String json) {
        ArrayList<String> billsRecevieables = new ArrayList<>();
        try {
            JSONObject main = new JSONObject(json);
            JSONObject envlop = main.getJSONObject("ENVELOPE");
            JSONArray billoverdue = envlop.getJSONArray("DSPACCNAME");

            for (int i = 0; i < billoverdue.length(); i++) {
                JSONObject bfv = billoverdue.getJSONObject(i);
                String job = String.valueOf(bfv.get("DSPDISPNAME"));
                System.out.println(job);
                billsRecevieables.add(job);
                // billsRecevieables.add(new BillsRecevieables(String.valueOf(bfv.get("BILLDATE")), String.valueOf(bfv.get("BILLREF")), String.valueOf(bfv.get("BILLPARTY")), String.valueOf(billcl.get(i)), String.valueOf(billdue.get(i)), String.valueOf(billoverdue.get(i))));
            }
//        billsRecevieables.forEach((bill)->{
//            System.out.println(bill.getBillCl()+"   "+bill.getBillDate()+"   "+bill.getBillDue()+"   "+bill.getBillOverDue()+"   "+bill.getBillParty()+"   "+bill.getBillRef());});
//        System.out.println(billsRecevieables.size());
//        test(billsRecevieables);
        }
        catch (Exception e){
            return billsRecevieables;
        }
        return billsRecevieables;
    }

}

