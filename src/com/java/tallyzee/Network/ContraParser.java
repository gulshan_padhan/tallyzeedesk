package com.java.tallyzee.Network;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.java.tallyzee.Utility.AppParserUtil;
import net.projectmonkey.object.mapper.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import com.java.tallyzee.Models.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ContraParser implements Runnable {
    String companyName,userName;
    String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    String[] monthsCount = {"31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"};
    String[] daysList = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
    ArrayList<SalesVoucher> arrayList = new ArrayList<>();
    // static SalesParser n = new SalesParser();
    SalesCallBcak salesCallBcak;
    ContraParser(String userName,String companyName,ParserController parserController)
    {
        salesCallBcak=parserController;
        this.userName=userName;
        this.companyName=companyName;
    }
    @Override
    public void run() {
       salesCallBcak.getContraResponse((ArrayList<ContraVoucher>) actionPerformed());
    }
    public String test(String responseXml) {
        JSONObject obj = null;
        try {
            obj = XML.toJSONObject(responseXml);
            // //System.out.println("json" + obj.toString());
            printJson(obj.toString());
        } catch (Exception e) {
            //System.out.println(e);
        }
        return obj.toString();
    }

    public String getPayRoll(String fromdate, String todate) {
        String payroll = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">" + fromdate + "</SVFROMDATE>   <SVTODATE TYPE=\"Date\">" + todate + "</SVTODATE>     <SVCURRENTCOMPANY>"+companyName+"</SVCURRENTCOMPANY>            </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsContra:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";

        return payroll;
    }

    public List<ContraVoucher> actionPerformed() {
        String test = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">20170404</SVFROMDATE>   <SVTODATE TYPE=\"Date\">20170404</SVTODATE>                 </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsContra:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";

        String name = "", email;

        //System.out.println("OUT OF EXCEPTION");


//        ObjectMapper mapper = new ObjectMapper();
//        // As an admin, the app has access to read and write all data, regardless of Security Rules
//        DatabaseReference ref = FirebaseDatabase.getInstance()
//                .getReferenceFromUrl("https://tallyfy-364a0.firebaseio.com");
        String fromDay, toDay;
        String year = "2016";
        int count = 0;
        ArrayList<String> vouchernumber = new ArrayList<>();
        ArrayList<String> vouchernumber1 = new ArrayList<>();
//        int yr=2016;
//        int crYr;
//        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//        Date date = new Date();
//        //System.out.println();
//        crYr=Integer.valueOf(dateFormat.format(date).substring(0,dateFormat.format(date).indexOf("/")));
//        int lengthYaer=1;
//        if((crYr-yr)>0)
//        {
//            lengthYaer=crYr-yr;
//        }
//        else
//        {
//            lengthYaer=1;
//        }
        List<ContraVoucher> contraList=new ArrayList<>();
//        for(int ped=0;ped<lengthYaer+1;ped++)
//        {
//
//            String rt=String.valueOf(yr);
//            for (int days = 0; days < monthsCount.length; days++) {
//                for (int j = 0; j < Integer.valueOf(monthsCount[days]); j++) {
//
//                    fromDay = daysList[j] + "" + months[days] + "" + rt;
                    ArrayList<ContraVoucher> salesVouchers = new ArrayList<>();
                    // //System.out.println(getPayRoll(fromDay, fromDay));
                    LocalDate bday = LocalDate.of(Integer.valueOf(AppParserUtil.getCompany_date().substring(0,4)), Month.of(Integer.valueOf(AppParserUtil.getCompany_date().substring(4,6))),Integer.valueOf(AppParserUtil.getCompany_date().substring(6,AppParserUtil.getCompany_date().length())));
                    LocalDate today=  LocalDate.now().minusDays(30);
                    for (LocalDate date1 = bday; date1.isBefore(today); date1 = date1.plusDays(30)) {
                        LocalDate localDate=date1.plusDays(30);
                        salesVouchers = (ArrayList<ContraVoucher>) printJson(test(NetworkServices.getResponse(getPayRoll(date1.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy")), localDate.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy"))))));
                        // //System.out.println("the" + count + "response size is:" + salesVouchers.size());
//
                        for (int i = 0; i < salesVouchers.size(); i++) {
                            contraList.add(salesVouchers.get(i));
                            vouchernumber1.add(salesVouchers.get(i).getVoucherNumber());
                            final int temp = i;
                            final ContraVoucher sales = salesVouchers.get(i);

                            count++;

                        }
                    }

                    try {
                        //  Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }



        return contraList;
        //System.out.println(vouchernumber.size() + " " + vouchernumber1.size());
    }

    //    public Object InitializeControls()
//    {
//        SalesVoucher sales = new SalesVoucher();
//        String address = sales.getAddress();
//        String buyerName = sales.getBasicBuyerName();
//        String partyName = sales.getPartyName();
//        String amount = sales.getAmount();
//        String voucherType = sales.getVoucherType();
//        String parentVoucherType = sales.getParentVoucherType();
//        String partyLedgerName = sales.getPartyLedgerName();
//        String reference = sales.getReference();
//        String basicBasePartyName = sales.getBasicBasePartyName();
//         object_sales = new SalesVoucher(address,buyerName,partyName,amount,voucherType,parentVoucherType,partyLedgerName
//        ,reference,basicBasePartyName);
//
//        return object_sales;
//    }
    public static class PC {
        // Prints a string and waits for consume()
        public void produce() throws InterruptedException {
            // synchronized block ensures only one thread
            // running at a time.
            synchronized (this) {
                //System.out.println("producer thread running");

                // releases the lock on shared resource
                wait();

                // and waits till some other method invokes notify().
                //System.out.println("Resumed");
            }
        }

        // Sleeps for some time and waits for a key press. After key
        // is pressed, it notifies produce().
        public void consume() throws InterruptedException {
            // this makes the produce thread to run first.
            Thread.sleep(1000);
            // Scanner s = new Scanner(System.in);

            // synchronized block ensures only one thread
            // running at a time.
            synchronized (this) {
                //System.out.println("Waiting for return key.");
                // s.nextLine();

                //System.out.println("Return key pressed");

                // notifies the produce thread that it
                // can wake up.
                notify();

                // Sleep
                Thread.sleep(2000);
            }
        }
    }

    List<ContraVoucher> printJson(String json) {

        String voucherDate;
        String voucherPartyName;
        String voucherNumber;
        String voucherNarration;
        String voucherTypeName;
        String basicUserDesc;
        String rate;
        String amount;
        int edqty;
        int actualQty;

        String stockName;
        String gowdownName;
        String batchName;
        Double batchamount;
        int batchactualQty;
        int billQty;
        Date orderDueDate;
        List<LedgerItem> items = null;
        List<Item> inventories = null;
        Double basicOrderRate;
        String orderType;
        String basicPurchaseOrderNumber;
        Double orderamount;
        String ledgerName;
        String billName;
        String billAmount;
        String billType;
        String billCreditPeriod;
        List<ContraVoucher> salesVouchers = new ArrayList<>();
        List<InvoiceOrderList> invoiceOrderLists = null;
        List<Inventories> inventoriesList = new ArrayList<>();
        try {
            JSONObject main = new JSONObject(json);
            JSONObject envlop = main.getJSONObject("ENVELOPE");

            JSONObject body = envlop.getJSONObject("BODY");
            JSONObject desc = body.getJSONObject("DESC");
            JSONObject stavar = desc.getJSONObject("STATICVARIABLES");
            // //System.out.println(stavar.get("SVCURRENTCOMPANY"));
            companyName = String.valueOf(stavar.get("SVCURRENTCOMPANY"));
            JSONObject data = body.getJSONObject("DATA");
            salesVouchers = new ArrayList<>();
            if (!data.equals(null) && data.isNull("DATA")) {
                if(String.valueOf(data.get("TALLYMESSAGE")).equalsIgnoreCase(""))
                {

                }
                else if(String.valueOf(data.get("TALLYMESSAGE")).charAt(0)=='{')
                {
                    invoiceOrderLists = new ArrayList<>();
                    JSONObject tamsg = data.getJSONObject("TALLYMESSAGE");
                    ////System.out.println(tamsg);
                    JSONObject voucher = tamsg.getJSONObject("VOUCHER");
                    inventoriesList = new ArrayList<>();
                    //  System.out.print("Voucher" + " " + voucher.get("DATE") + " " + voucher.getString("PARTYLEDGERNAME") + " " + voucher.getString("VOUCHERTYPENAME") + " " + voucher.getString("VOUCHERNUMBER"));
                    //  System.out.print("Voucher" + " " + voucher.get("DATE") + " " + voucher.getString("PARTYLEDGERNAME") + " " + voucher.getString("VOUCHERTYPENAME") + " " + voucher.getString("VOUCHERNUMBER"));
                    if(String.valueOf(voucher).contains("DATE"))
                        voucherDate = String.valueOf(voucher.get("DATE"));
                    else
                        voucherDate=null;
                    if(String.valueOf(voucher).contains("PARTYLEDGERNAME"))
                        voucherPartyName = String.valueOf(voucher.get("PARTYLEDGERNAME"));
                    else
                        voucherPartyName=null;
                    if(String.valueOf(voucher).contains("VOUCHERNUMBER"))
                        voucherNumber = String.valueOf(voucher.get("VOUCHERNUMBER"));
                    else
                        voucherNumber=null;
                    if(String.valueOf(voucher).contains("VOUCHERTYPENAME"))
                        voucherTypeName = String.valueOf(voucher.get("VOUCHERTYPENAME"));
                    else
                        voucherTypeName=null;
                    if(String.valueOf(voucher).contains("NARRATION"))
                        voucherNarration = String.valueOf(voucher.get("NARRATION"));
                    else
                        voucherNarration=null;
                    //System.out.println(voucherPartyName + " " + voucherDate + " " + voucherNumber + " " + voucherTypeName + " " + voucherNarration);
                    //voucherType v;
                    //System.out.print(" " + voucher.get("NARRATION"));
                    items = getAllLedgerItem(voucher);
                    salesVouchers.add(new ContraVoucher(voucherDate, voucherPartyName, voucherTypeName, "Contra", voucherNumber, voucherNarration,  items));

                }
                else if(String.valueOf(data.get("TALLYMESSAGE")).charAt(0)=='[')
                {
                    JSONArray tallymsg = data.getJSONArray("TALLYMESSAGE");
                    ////System.out.println(tallymsg);
                    //System.out.println(tallymsg.length());
                    for (int i = 0; i < tallymsg.length(); i++) {
                        invoiceOrderLists = new ArrayList<>();
                        JSONObject tamsg = tallymsg.getJSONObject(i);
                        ////System.out.println(tamsg);
                        JSONObject voucher = tamsg.getJSONObject("VOUCHER");
                        inventoriesList = new ArrayList<>();
                        //  System.out.print("Voucher" + " " + voucher.get("DATE") + " " + voucher.getString("PARTYLEDGERNAME") + " " + voucher.getString("VOUCHERTYPENAME") + " " + voucher.getString("VOUCHERNUMBER"));
                        //  System.out.print("Voucher" + " " + voucher.get("DATE") + " " + voucher.getString("PARTYLEDGERNAME") + " " + voucher.getString("VOUCHERTYPENAME") + " " + voucher.getString("VOUCHERNUMBER"));
                        if(String.valueOf(voucher).contains("DATE"))
                            voucherDate = String.valueOf(voucher.get("DATE"));
                        else
                            voucherDate=null;
                        if(String.valueOf(voucher).contains("PARTYLEDGERNAME"))
                            voucherPartyName = String.valueOf(voucher.get("PARTYLEDGERNAME"));
                        else
                            voucherPartyName=null;
                        if(String.valueOf(voucher).contains("VOUCHERNUMBER"))
                            voucherNumber = String.valueOf(voucher.get("VOUCHERNUMBER"));
                        else
                            voucherNumber=null;
                        if(String.valueOf(voucher).contains("VOUCHERTYPENAME"))
                            voucherTypeName = String.valueOf(voucher.get("VOUCHERTYPENAME"));
                        else
                            voucherTypeName=null;
                        if(String.valueOf(voucher).contains("NARRATION"))
                            voucherNarration = String.valueOf(voucher.get("NARRATION"));
                        else
                            voucherNarration=null;
                        //System.out.println(voucherPartyName + " " + voucherDate + " " + voucherNumber + " " + voucherTypeName + " " + voucherNarration);
                        //voucherType v;
                        //System.out.print(" " + voucher.get("NARRATION"));
                        items = getAllLedgerItem(voucher);
                        salesVouchers.add(new ContraVoucher(voucherDate, voucherPartyName, voucherTypeName, "Contra", voucherNumber, voucherNarration,  items));
                    }
                }


            }


        } catch (Exception e) {
            e.getMessage();
        }
        if (salesVouchers.size() == 0) {
            salesVouchers = new ArrayList<>();
        }
        return salesVouchers;
    }

    List<LedgerItem> getAllLedgerItem(JSONObject voucher) {
        String voucherDate;
        String voucherPartyName;
        String voucherNumber;
        String voucherNarration;
        String voucherTypeName;
        String basicUserDesc;
        Double rate;
        String amount;
        int edqty;
        int actualQty;

        String stockName;
        String gowdownName;
        String batchName;
        Double batchamount;
        int batchactualQty;
        int billQty;
        Date orderDueDate;
        List<LedgerItem> items = null;
        List<Item> inventories = null;
        Double basicOrderRate;
        String orderType;
        String basicPurchaseOrderNumber;
        Double orderamount;
        String ledgerName;
        String billName;
        String billAmount;
        String billType;
        String billCreditPeriod;
        List<SalesVoucher> salesVouchers = new ArrayList<>();
        List<InvoiceOrderList> invoiceOrderLists = null;
        List<Inventories> inventoriesList = null;
        ArrayList<LedgerItem> listItes = new ArrayList<>();
        JSONArray list = voucher.getJSONArray("ALLLEDGERENTRIES.LIST");
        items = new ArrayList<>();
        for (int j = 0; j < list.length(); j++) {
            JSONObject bill = list.getJSONObject(j);
            ////System.out.println(String.valueOf(list.get(j)));
            ////System.out.println(bill);
            inventories = new ArrayList<>();
            amount = String.valueOf(bill.get("AMOUNT"));
            ledgerName = String.valueOf(bill.get("LEDGERNAME"));
            // //System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");0


            ////System.out.println("the String is Object:"+op);
            if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST")).toString();
                if(op.equalsIgnoreCase(""))
                {

                }
                else if (op.charAt(0) == '{') {

                    inventories = new ArrayList<>();
                    // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                    ////System.out.println("the String is Object:"+op);

                    JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                    // //System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                    ////System.out.println("ok tasted 1");

                    billAmount = String.valueOf(billl.get("AMOUNT"));
                    billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                    billType = String.valueOf(billl.get("BILLTYPE"));
                    billName = String.valueOf(billl.get("NAME"));
                    //  String cpr = String.valueOf(billl.get("P"));
                    //System.out.println(billAmount + " " + billCreditPeriod + " " + billType + " " + billName);
                    inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                } else if (op.charAt(0) == '[') {
                    //System.out.println("ok tasted 2");
                    // op=bill.get("BILLALLOCATIONS.LIST").toString();
                    inventories = new ArrayList<>();
                    ////System.out.println("the String is Array:"+op);
                    String reString = bill.toString();
                    ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                    JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                    ////System.out.println("ok tasted 3");
                    for (int b = 0; b < billlist.length(); b++) {
                        JSONObject billItems = billlist.getJSONObject(b);
                        billAmount = String.valueOf(billItems.get("AMOUNT"));
                        // billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                        billType = String.valueOf(billItems.getString("BILLTYPE"));
                        billName = String.valueOf(billItems.getString("NAME"));

                        inventories.add(new Item(billName, billAmount, billType, ""));
                        //System.out.println("bill" + " " + billName + " " + billType + " " + billAmount + " ");
                    }
                    ////System.out.println("ok tasted 4");
                    //e.printStackTrace();

                } else {
                    items = new ArrayList<>();
                    //System.out.println("ok null");
                }
            }
            ////System.out.println("the String is Object:"+op);
            ArrayList<BankItem> bankItems=new ArrayList<BankItem>();
            if(String.valueOf(bill).contains("BANKALLOCATIONS.LIST")) {
                String ba = String.valueOf(bill.get("BANKALLOCATIONS.LIST")).toString();

                if(ba.equalsIgnoreCase(""))
                {

                }
                else if (ba.charAt(0) == '{') {
                    //  ba =String bill.get("BANKALLOCATIONS.LIST").toString();
                    //\//System.out.println("the String is Object:"+op);
                    JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                    String bankPaqartyName = String.valueOf(billl.get("BANKNAME"));
                    String bankamount = String.valueOf(billl.get("AMOUNT"));
                    String transcationType = String.valueOf(billl.get("TRANSACTIONTYPE"));
                    String date = String.valueOf(billl.get("DATE"));
                    String accNumber = String.valueOf(billl.get("BANKBRANCHNAME"));
                    String email = String.valueOf(billl.get("PAYMENTMODE"));
                    bankItems.add(new BankItem(accNumber, bankPaqartyName, amount, transcationType, date));
                    //System.out.println(bankamount + " " + bankPaqartyName + " " + date + " " + accNumber + " " + email + " " + transcationType);

                    ////System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                    ////System.out.println("ok tasted 1");

                } else if (ba.charAt(0) == '[') {
                    System.out.print("ok tasted 2");
                    //ba = bill.getString("BANKALLOCATIONS.LIST").toString();

                    ////System.out.println("the String is Array:"+op);
                    String reString = bill.toString();
                    ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                    JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                    ////System.out.println("ok tasted 3");
                    for (int b = 0; b < billlist.length(); b++) {
                        JSONObject billl = billlist.getJSONObject(b);
                        String bankPaqartyName = String.valueOf(billl.get("BANKPARTYNAME"));
                        String bankamount = String.valueOf(billl.get("AMOUNT"));
                        String transcationType = String.valueOf(billl.get("TRANSACTIONTYPE"));
                        String date = String.valueOf(billl.get("DATE"));
                        String accNumber = String.valueOf(billl.get("BANKBRANCHNAME"));
                        String email = String.valueOf(billl.get("PAYMENTMODE"));
                        bankItems.add(new BankItem(accNumber, bankPaqartyName, amount, transcationType, date));
                        //System.out.println(bankamount + " " + bankPaqartyName + " " + date + " " + accNumber + " " + email + " " + transcationType);

                        // //System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                    }
                    ////System.out.println("ok tasted 4");
                    //e.printStackTrace();
                } else {
                    //System.out.println("ok null");
                }
            }
            if(String.valueOf(bill.toString()).contains("CATEGORYALLOCATIONS.LIST")) {
                JSONObject bi = bill.getJSONObject("CATEGORYALLOCATIONS.LIST");
                String categ = String.valueOf(bi.get("CATEGORY"));
                //System.out.println("cost categ:" + categ);
                if (String.valueOf(bill.get("CATEGORYALLOCATIONS.LIST")).equalsIgnoreCase("")) {

                }
                else if (String.valueOf(bill.get("CATEGORYALLOCATIONS.LIST")).charAt(0) == '{') {
                    JSONObject obj=bi.getJSONObject("COSTCENTREALLOCATIONS.LIST");
                    String costname= String.valueOf(obj.get("NAME"));
                    String costamount= String.valueOf(obj.get("AMOUNT"));
                    String costbillQty= String.valueOf(obj.get("BILLEDQTY"));
                    String costactualQty= String.valueOf(obj.get("ACTUALQTY"));
                    //System.out.println(costname+" "+costamount+" "+costactualQty+" "+costbillQty+" "+categ);
                }
            }
            listItes.add(new LedgerItem(amount, ledgerName, inventories,bankItems));
        }
        return listItes;
    }
}
