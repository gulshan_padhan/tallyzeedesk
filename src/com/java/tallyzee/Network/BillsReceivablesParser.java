package com.java.tallyzee.Network;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import io.reactivex.Observable;
import net.projectmonkey.object.mapper.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;


import java.util.ArrayList;
import java.util.List;

public class BillsReceivablesParser extends Thread
{

    static String urlParameters="<ENVELOPE><HEADER><VERSION>1</VERSION><TALLYREQUEST>Export</TALLYREQUEST><TYPE>Data</TYPE><ID>BillsReceivable</ID></HEADER><BODY><DESC><STATICVARIABLES><EXPLODEFLAG>Yes</EXPLODEFLAG><SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVCURRENTCOMPANY>##SVCurrentCompany</SVCURRENTCOMPANY></STATICVARIABLES></DESC></BODY></ENVELOPE>";
String userName,companyName;
BillsReceivablesParser(String userName,String companyName)
{
    this.userName=userName;
    this.companyName=companyName;
}
    @Override
    public void run() {
        pushData();
    }
    public String test(String responseXml) {
        JSONObject obj = null;
        try {
            obj = XML.toJSONObject(responseXml);
            System.out.println("json" + obj.toString());
            //printJson(obj.toString());
        } catch (Exception e) {
            System.out.println(e);
        }
        return obj.toString();
    }
    public void pushData()
    {
        ArrayList<BillsRecevieables>   list= (ArrayList<BillsRecevieables>) getListOFBill(test(NetworkServices.getResponse(urlParameters)));
        ObjectMapper mapper = new ObjectMapper();
        DatabaseReference ref = FirebaseDatabase.getInstance()
                .getReferenceFromUrl("https://tallyfy-364a0.firebaseio.com");
       for(int i=0;i<list.size();i++) {
   ref.child("Users").child(userName).child("companyListData").child(companyName.replaceAll("[\\-\\+\\.\\^:,]","")).child("bill_Receivables").child(String.valueOf(i)).setValue(list.get(i), new DatabaseReference.CompletionListener() {
 //               ref.child("Demo Company").child("bill_Receivables").child(String.valueOf(i)).setValue(list.get(i), new DatabaseReference.CompletionListener() {
               @Override
               public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    System.out.println("ok");
               }
           });

       }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void test(ArrayList<BillsRecevieables> list)
    {
//      //  PublishSubject<BillsRecevieables> list1=PublishSubject.create();
//        Observable.from(list).subscribeOn(Schedulers.newThread())
//            .subscribe(resultsObject ->{
//                System.out.println("ok");});
    }

    static List<BillsRecevieables> getListOFBill(String json)
    {
        ArrayList<BillsRecevieables> billsRecevieables=new ArrayList<>();
        JSONObject main = new JSONObject(json);
        JSONObject envlop = main.getJSONObject("ENVELOPE");
        JSONArray billoverdue=envlop.getJSONArray("BILLOVERDUE");
        JSONArray billdue=envlop.getJSONArray("BILLDUE");
        JSONArray billcl=envlop.getJSONArray("BILLCL");
        JSONArray billfixed=envlop.getJSONArray("BILLFIXED");
        for(int i=0;i<billfixed.length();i++)
        {
            JSONObject bfv=billfixed.getJSONObject(i);
            billsRecevieables.add(new BillsRecevieables(String.valueOf(bfv.get("BILLDATE")), String.valueOf(bfv.get("BILLREF")), String.valueOf(bfv.get("BILLPARTY")), String.valueOf(billcl.get(i)), String.valueOf(billdue.get(i)), String.valueOf(billoverdue.get(i))));
        }
        billsRecevieables.forEach((bill)->{
            System.out.println(bill.getBillCl()+"   "+bill.getBillDate()+"   "+bill.getBillDue()+"   "+bill.getBillOverDue()+"   "+bill.getBillParty()+"   "+bill.getBillRef());});
        System.out.println(billsRecevieables.size());
        test(billsRecevieables);
        return  billsRecevieables;
    }

}
