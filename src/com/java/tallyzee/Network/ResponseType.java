package com.java.tallyzee.Network;

public enum ResponseType {
    RESPONSEOK("RESPONSEOK"), RESPONSENOTFOUND("RESPONSENOTFOUND"), RESPONSENETWORKISSUE("RESPONSENETWORKISSUE"),NETWORKERROR("NETWORKERROR");
    private String responseType;
    public String getResponseType() {
        return this.responseType;
    }
    ResponseType(String responseType) {
        this.responseType = responseType;
    }
}
