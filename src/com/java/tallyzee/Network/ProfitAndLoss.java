package com.java.tallyzee.Network;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import net.projectmonkey.object.mapper.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import rx.Observable;
import rx.schedulers.Schedulers;
import com.java.tallyzee.Models.ProfinAndLoss;

import java.util.ArrayList;
import java.util.List;

public class ProfitAndLoss  extends Thread{
    //String
    ProfitAndLoss()
    {

    }
    String userName,companyName;

    ProfitAndLoss(String userName,String companyName)
    {
        this.userName=userName;
        this.companyName=companyName;
    }
    String getPayLoad(){
       return  "<ENVELOPE><HEADER><VERSION>1</VERSION><TALLYREQUEST>Export</TALLYREQUEST><TYPE>Data</TYPE><ID>Profit and Loss</ID></HEADER><BODY><DESC><STATICVARIABLES><EXPLODEFLAG>Yes</EXPLODEFLAG><SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT>" +
                "\t<SVFROMDATE TYPE=\"Date\">1-1-2017</SVFROMDATE>   <SVTODATE TYPE=\"Date\">1-1-2018</SVTODATE><SVCURRENTCOMPANY>"+companyName+"</SVCURRENTCOMPANY></STATICVARIABLES></DESC></BODY></ENVELOPE>";

    }


    @Override
    public void run() {
        pushData();
    }
    public String test(String responseXml) {
        JSONObject obj = null;
        try {
            obj = XML.toJSONObject(responseXml);
            System.out.println("json" + obj.toString());
            //printJson(obj.toString());
        } catch (Exception e) {
            System.out.println(e);
        }
        return obj.toString();
    }
    public void pushData()
    {
       System.out.println(test(NetworkServices.getResponse(getPayLoad()))); ;
      // ArrayList<ProfinAndLoss> list=getResponseLedger()
       ArrayList<String> list= (ArrayList<String>) getListOFBill(test(NetworkServices.getResponse(getPayLoad())));
       ArrayList<ProfinAndLoss> profinAndLosses= (ArrayList<ProfinAndLoss>) getResponseLedger(list);
        System.out.println(profinAndLosses);
        System.out.println(profinAndLosses.size());
        ObjectMapper mapper = new ObjectMapper();
        DatabaseReference ref = FirebaseDatabase.getInstance()
                .getReferenceFromUrl("https://tallyfy-364a0.firebaseio.com");

        for(int i=0;i<profinAndLosses.size();i++) {
          ref.child("Users").child(userName).child("companyListData").child(companyName.replaceAll("[\\-\\+\\.\\^:,]","")).child("ProfitandLoss").child(String.valueOf(i)).setValue(profinAndLosses.get(i), new DatabaseReference.CompletionListener() {
           // ref.child("Demo Company").child("ProfitandLoss").child(String.valueOf(i)).setValue(profinAndLosses.get(i), new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    System.out.println("ok");
                }
            });

        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void test(ArrayList<BillsRecevieables> list)
    {
        //  PublishSubject<BillsRecevieables> list1=PublishSubject.create();
        Observable.from(list).subscribeOn(Schedulers.newThread())
                .subscribe(resultsObject ->{
                    System.out.println("ok");});
    }
public  List<ProfinAndLoss> getResponseLedger(List<String> list)
{
    ArrayList<ProfinAndLoss> prf=new ArrayList();
    ProfinAndLoss t,t2;
    for(int i=0;i<list.size();i++)
    {
        t=getLedger(test(NetworkServices.getResponse(getLedgerXmlforGroup(list.get(i)))));
        if(t!=null)
        {
            prf.add(t);
        }
        t2=getLedger(test(NetworkServices.getResponse(getLedgerXmlforLedger(list.get(i)))));
        if(t2!=null)
        {
            prf.add(t2);
        }
    }
    return prf;
}
public  String getLedgerXmlforLedger(String partyName)
{

    String xmlforLedgerGroup="<ENVELOPE>\n\n<HEADER>\n\n<VERSION>1</VERSION>\n\n<TALLYREQUEST>EXPORT</TALLYREQUEST>\n\n<TYPE>OBJECT</TYPE> <SUBTYPE>Group</SUBTYPE> <ID TYPE=\"Name\">"+partyName+"</ID>\n\n</HEADER>\n\n<BODY>\n\n<DESC>\n\n<STATICVARIABLES>\n\t<SVFROMDATE TYPE=\"Date\">1-1-2017</SVFROMDATE>   <SVTODATE TYPE=\"Date\">1-1-2018</SVTODATE>\n\t<SVCURRENTCOMPANY>"+companyName+"</SVCURRENTCOMPANY><SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT></STATICVARIABLES>\n\n<FETCHLIST>\n\n<FETCH>Name</FETCH>\n\n<FETCH>TNetBalance</FETCH>\n\n<FETCH>Parent</FETCH>\n\n</FETCHLIST>\n\n<TDL>\n\n<TDLMESSAGE>\n\n<OBJECT NAME=\"Group\" ISINITIALIZE=\"Yes\">\n\n<LOCALFORMULA>\n\nTNetBalance: $$AsPositive:$$AmountSubtract:$ClosingBalance:$OpeningBalance\n\n</LOCALFORMULA>\n\n</OBJECT>\n\n</TDLMESSAGE>\n\n</TDL>\n\n</DESC>\n\n</BODY>\n\n</ENVELOPE>";
return xmlforLedgerGroup;
}
public  String getLedgerXmlforGroup(String partyName)
{
    String xmlforLedgerOnly="<ENVELOPE>\n\n<HEADER>\n\n<VERSION>1</VERSION>\n\n<TALLYREQUEST>EXPORT</TALLYREQUEST>\n\n<TYPE>OBJECT</TYPE> <SUBTYPE>Ledger</SUBTYPE> <ID TYPE=\"Name\">"+partyName+"</ID>\n\n</HEADER>\n\n<BODY>\n\n<DESC>\n\n<STATICVARIABLES>\n\t<SVFROMDATE TYPE=\"Date\">1-1-2017</SVFROMDATE>   <SVTODATE TYPE=\"Date\">1-1-2018</SVTODATE>\n\t<SVCURRENTCOMPANY>"+companyName+"</SVCURRENTCOMPANY><SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT></STATICVARIABLES>\n\n<FETCHLIST>\n\n<FETCH>Name</FETCH>\n\n<FETCH>TNetBalance</FETCH>\n\n<FETCH>Parent</FETCH>\n\n</FETCHLIST>\n\n<TDL>\n\n<TDLMESSAGE>\n\n<OBJECT NAME=\"Ledger\" ISINITIALIZE=\"Yes\">\n\n<LOCALFORMULA>\n\nTNetBalance: $$AsPositive:$$AmountSubtract:$ClosingBalance:$OpeningBalance\n\n</LOCALFORMULA>\n\n</OBJECT>\n\n</TDLMESSAGE>\n\n</TDL>\n\n</DESC>\n\n</BODY>\n\n</ENVELOPE>";
return xmlforLedgerOnly;
}
static  ProfinAndLoss getLedger(String json)
{
    String parentName="";
    String ledgerAmount="";
    String ledgerName="";
    if(json.contains("ENVELOPE"))
    {
        JSONObject main = new JSONObject(json);
        JSONObject envlop = main.getJSONObject("ENVELOPE");
        JSONObject body = envlop.getJSONObject("BODY");
        JSONObject data = body.getJSONObject("DATA");
        JSONObject tallymsg = data.getJSONObject("TALLYMESSAGE");
        if(String.valueOf(tallymsg).contains("GROUP"))
        {
            JSONObject group=tallymsg.getJSONObject("GROUP");
            JSONObject parent=group.getJSONObject("PARENT");
            parentName= String.valueOf(parent.get("content"));

            JSONObject amount=group.getJSONObject("TNETBALANCE");
            ledgerAmount= String.valueOf(amount.get("content"));


            ledgerName= String.valueOf(group.get("NAME"));
            return new ProfinAndLoss(ledgerName,parentName,ledgerAmount,"");

        }else if(String.valueOf(tallymsg).contains("LEDGER"))
        {
            JSONObject group=tallymsg.getJSONObject("LEDGER");
            JSONObject parent=group.getJSONObject("PARENT");
            parentName= String.valueOf(parent.get("content"));

            JSONObject amount=group.getJSONObject("TNETBALANCE");
            ledgerAmount= String.valueOf(amount.get("content"));


            ledgerName= String.valueOf(group.get("NAME"));

            return new ProfinAndLoss(ledgerName,parentName,ledgerAmount,"");
        }
    }

    return null;

}
    static List<String> getListOFBill(String json)
    {
        ArrayList<String> billsRecevieables=new ArrayList<>();
        JSONObject main = new JSONObject(json);
        JSONObject envlop = main.getJSONObject("ENVELOPE");
        JSONArray billoverdue=envlop.getJSONArray("BSNAME");

        for(int i=0;i<billoverdue.length();i++)
        {
            JSONObject bfv=billoverdue.getJSONObject(i);
            JSONObject job=bfv.getJSONObject("DSPACCNAME");
            System.out.println(String.valueOf(job.get("DSPDISPNAME")));
            billsRecevieables.add(String.valueOf(job.get("DSPDISPNAME")));
           // billsRecevieables.add(new BillsRecevieables(String.valueOf(bfv.get("BILLDATE")), String.valueOf(bfv.get("BILLREF")), String.valueOf(bfv.get("BILLPARTY")), String.valueOf(billcl.get(i)), String.valueOf(billdue.get(i)), String.valueOf(billoverdue.get(i))));
        }
//        billsRecevieables.forEach((bill)->{
//            System.out.println(bill.getBillCl()+"   "+bill.getBillDate()+"   "+bill.getBillDue()+"   "+bill.getBillOverDue()+"   "+bill.getBillParty()+"   "+bill.getBillRef());});
//        System.out.println(billsRecevieables.size());
//        test(billsRecevieables);
        return  billsRecevieables;
    }

}
