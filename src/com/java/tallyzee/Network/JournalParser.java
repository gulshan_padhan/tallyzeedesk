package com.java.tallyzee.Network;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import com.java.tallyzee.Models.*;
import com.java.tallyzee.Utility.AppParserUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JournalParser implements Runnable {
    String companyName,username;
    String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    String[] monthsCount = {"31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"};
    String[] daysList = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
    ArrayList<SalesVoucher> arrayList = new ArrayList<>();
    SalesCallBcak salesCallBcak;
    JournalParser(String username,String companyName,ParserController parserController)
    {
        salesCallBcak=parserController;
        this.companyName=companyName;
        this.username=username;
    }


    public String test(String responseXml) {
        JSONObject obj = null;
        try {
            obj = XML.toJSONObject(responseXml);
            //System.out.println("json" + obj.toString());
            //printJson(obj.toString());
        } catch (Exception e) {
            //System.out.println(e);
        }
        return obj.toString();
    }

    public String getPayRoll(String fromdate, String todate) {
        String payroll = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">" + fromdate + "</SVFROMDATE>   <SVTODATE TYPE=\"Date\">" + todate + "</SVTODATE>          <SVCURRENTCOMPANY>"+companyName+"</SVCURRENTCOMPANY>       </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsJournal:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";

        return payroll;
    }


    @Override
    public void run()
    {
       getPaymentVoucherAsList();
    }

    public ArrayList<JournalVoucher> getPaymentVoucherAsList() {

        String fromDay, toDay;
        String year = "2016";
        int count = 0;
        ArrayList<String> enteredVouchers=new ArrayList<>();
        ArrayList<JournalVoucher> paymentVoucherArrayList=new ArrayList<>();

        ArrayList<JournalVoucher> listJour=new ArrayList<>();

                    LocalDate bday = LocalDate.of(Integer.valueOf(AppParserUtil.getCompany_date().substring(0,4)), Month.of(Integer.valueOf(AppParserUtil.getCompany_date().substring(4,6))),Integer.valueOf(AppParserUtil.getCompany_date().substring(6,AppParserUtil.getCompany_date().length())));
                    LocalDate today=  LocalDate.now().minusDays(30);
                    for (LocalDate date1 = bday; date1.isBefore(today); date1 = date1.plusDays(30)) {
                        LocalDate localDate=date1.plusDays(30);
                        ArrayList<JournalVoucher> paymentList= (ArrayList<JournalVoucher>) jsonTOPaymentVoucher(AppParserUtil.getConvertToJson(AppParserUtil.getResponse(getPayRoll(date1.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy")), localDate.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy"))))));
                        for(int i=0;i<paymentList.size();i++)
                        {
                            listJour.add(paymentList.get(i));
                            JournalVoucher paymentVoucher=paymentList.get(i);
                            System.out.println(paymentVoucher);
                            count++;
                        }
                    }




        System.out.println(enteredVouchers.size());
return  listJour;
    }

    List<JournalVoucher> jsonTOPaymentVoucher(String json) {
        String voucherDate;
        String voucherPartyName;
        String voucherNumber;
        String voucherNarration;
        String voucherTypeName;

        List<JournalLedger> items = null;

        List<JournalVoucher> salesVouchers = new ArrayList<>();
        List<Inventories> inventoriesList = new ArrayList<>();
        try {
            JSONObject main = new JSONObject(json);
            JSONObject envlop = main.getJSONObject("ENVELOPE");

            JSONObject body = envlop.getJSONObject("BODY");
            JSONObject desc = body.getJSONObject("DESC");
            JSONObject stavar = desc.getJSONObject("STATICVARIABLES");
            companyName = String.valueOf(stavar.get("SVCURRENTCOMPANY"));
            JSONObject data = body.getJSONObject("DATA");
            salesVouchers = new ArrayList<>();
            if (!data.equals(null) && data.isNull("DATA")) {
                if(String.valueOf(data.get("TALLYMESSAGE")).equalsIgnoreCase(""))
                {

                }
                else if(String.valueOf(data.get("TALLYMESSAGE")).charAt(0)=='{')
                {
                    JSONObject tamsg = data.getJSONObject("TALLYMESSAGE");
                    JSONObject voucher = tamsg.getJSONObject("VOUCHER");
                    if(String.valueOf(voucher).contains("DATE"))
                        voucherDate = String.valueOf(voucher.get("DATE"));
                    else
                        voucherDate=null;
                    if(String.valueOf(voucher).contains("PARTYLEDGERNAME"))
                        voucherPartyName = String.valueOf(voucher.get("PARTYLEDGERNAME"));
                    else
                        voucherPartyName=null;
                    if(String.valueOf(voucher).contains("VOUCHERNUMBER"))
                        voucherNumber = String.valueOf(voucher.get("VOUCHERNUMBER"));
                    else
                        voucherNumber=null;
                    if(String.valueOf(voucher).contains("VOUCHERTYPENAME"))
                        voucherTypeName = String.valueOf(voucher.get("VOUCHERTYPENAME"));
                    else
                        voucherTypeName=null;
                    if(String.valueOf(voucher).contains("NARRATION"))
                        voucherNarration = String.valueOf(voucher.get("NARRATION"));
                    else
                        voucherNarration=null;
                    //System.out.println(voucherPartyName + " " + voucherDate + " " + voucherNumber + " " + voucherTypeName + " " + voucherNarration);
                    items = getAllLedgerItem(voucher);
                    salesVouchers.add(new JournalVoucher(voucherDate, voucherPartyName, voucherTypeName, "Journal", voucherNumber, voucherNarration, items));
                }else if(String.valueOf(data.get("TALLYMESSAGE")).charAt(0)=='[')
                {
                    JSONArray tallymsg = data.getJSONArray("TALLYMESSAGE");
                    //System.out.println(tallymsg.length());
                    for (int i = 0; i < tallymsg.length(); i++) {
                        JSONObject tamsg = tallymsg.getJSONObject(i);
                        JSONObject voucher = tamsg.getJSONObject("VOUCHER");
                        if(String.valueOf(voucher).contains("DATE"))
                            voucherDate = String.valueOf(voucher.get("DATE"));
                        else
                            voucherDate=null;
                        if(String.valueOf(voucher).contains("PARTYLEDGERNAME"))
                            voucherPartyName = String.valueOf(voucher.get("PARTYLEDGERNAME"));
                        else
                            voucherPartyName=null;
                        if(String.valueOf(voucher).contains("VOUCHERNUMBER"))
                            voucherNumber = String.valueOf(voucher.get("VOUCHERNUMBER"));
                        else
                            voucherNumber=null;
                        if(String.valueOf(voucher).contains("VOUCHERTYPENAME"))
                            voucherTypeName = String.valueOf(voucher.get("VOUCHERTYPENAME"));
                        else
                            voucherTypeName=null;
                        if(String.valueOf(voucher).contains("NARRATION"))
                            voucherNarration = String.valueOf(voucher.get("NARRATION"));
                        else
                            voucherNarration=null;
                        //System.out.println(voucherPartyName + " " + voucherDate + " " + voucherNumber + " " + voucherTypeName + " " + voucherNarration);
                        items = getAllLedgerItem(voucher);
                        salesVouchers.add(new JournalVoucher(voucherDate, voucherPartyName, voucherTypeName, "Journal", voucherNumber, voucherNarration, items));
                    }
                }




            }


        } catch (Exception e) {
            e.getMessage();
        }
        if (salesVouchers.size() == 0) {
            salesVouchers = new ArrayList<>();
        }
        return salesVouchers;
    }

    List<JournalLedger> getAllLedgerItem(JSONObject voucher) {
        String amount;

        List<PaymentBank> paymentBanks = null;
        List<JournalItem> paymentBills = null;
        List<CostCenter> costCenters=null;
        String ledgerName;
        String billName;
        String billAmount;
        String billType;
        String billCreditPeriod;
        ArrayList<JournalLedger> listItes = new ArrayList<>();
        JSONArray list = voucher.getJSONArray("ALLLEDGERENTRIES.LIST");

        for (int j = 0; j < list.length(); j++) {
            JSONObject bill = list.getJSONObject(j);
            paymentBanks=new ArrayList<>();
            paymentBills=new ArrayList<>();
            costCenters=new ArrayList<>();

            amount = String.valueOf(bill.get("AMOUNT"));
            ledgerName = String.valueOf(bill.get("LEDGERNAME"));
            if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST"));
                if(op.equalsIgnoreCase(""))
                {

                }
                else if (op.charAt(0) == '{') {

                    paymentBills=new ArrayList<>();
                    JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                    billAmount = String.valueOf(billl.get("AMOUNT"));
                    billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                    billType = String.valueOf(billl.get("BILLTYPE"));
                    billName = String.valueOf(billl.get("NAME"));
                    //System.out.println(billAmount + " " + billCreditPeriod + " " + billType + " " + billName);
                    paymentBills.add(new JournalItem(billName, billAmount, billType));
                } else if (op.charAt(0) == '[') {
                    //System.out.println("ok tasted 2");
                    paymentBills=new ArrayList<>();
                    JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");

                    for (int b = 0; b < billlist.length(); b++) {
                        JSONObject billItems = billlist.getJSONObject(b);
                        billAmount = String.valueOf(billItems.get("AMOUNT"));
                        billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                        billType = String.valueOf(billItems.get("BILLTYPE"));
                        billName = String.valueOf(billItems.get("NAME"));

                        paymentBills.add(new JournalItem(billName, billAmount, billType));
                        //System.out.println("bill" + " " + billName + " " + billType + " " + billAmount + " " + billCreditPeriod + " ");
                    }
                } else {
                    //System.out.println("ok null");
                }
            }
            ArrayList<BankItem> bankItems=new ArrayList<BankItem>();
            if(String.valueOf(bill).contains("BANKALLOCATIONS.LIST")) {
                String ba = String.valueOf(bill.get("BANKALLOCATIONS.LIST"));

                if(ba.equalsIgnoreCase(""))
                {

                }
                else if (ba.charAt(0) == '{') {
                    paymentBanks=new ArrayList<>();
                    JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                    String bankPaqartyName = String.valueOf(billl.get("BANKPARTYNAME"));
                    String bankamount = String.valueOf(billl.get("AMOUNT"));
                    String transcationType = String.valueOf(billl.get("TRANSACTIONTYPE"));
                    String date = String.valueOf(billl.get("DATE"));
                    String accNumber = String.valueOf(billl.get("ACCOUNTNUMBER"));
                    String email = String.valueOf(billl.get("EMAIL"));
                    // public PaymentBank(String bankName, String bankamount, String bankTypeTransction, String bankDate, String bankAccNumber)
                    paymentBanks.add(new PaymentBank( bankPaqartyName, amount,accNumber, date, transcationType));
                    //System.out.println(bankamount + " " + bankPaqartyName + " " + date + " " + accNumber + " " + email + " " + transcationType);
                } else if (ba.charAt(0) == '[') {
                    System.out.print("ok tasted 2");
                    paymentBanks=new ArrayList<>();
                    String reString = bill.toString();
                    JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                    for (int b = 0; b < billlist.length(); b++) {
                        JSONObject billl = billlist.getJSONObject(b);
                        String bankPaqartyName = String.valueOf(billl.get("BANKPARTYNAME"));
                        String bankamount = String.valueOf(billl.get("AMOUNT"));
                        String transcationType = String.valueOf(billl.get("TRANSACTIONTYPE"));
                        String date = String.valueOf(billl.get("DATE"));
                        String accNumber = String.valueOf(billl.get("ACCOUNTNUMBER"));
                        String email = String.valueOf(billl.get("EMAIL"));
                        paymentBanks.add(new PaymentBank( bankPaqartyName, amount,accNumber, date, transcationType));
                        //System.out.println(bankamount + " " + bankPaqartyName + " " + date + " " + accNumber + " " + email + " " + transcationType);
                    }
                } else {
                    //System.out.println("ok null");
                }
            }
            if(String.valueOf(bill.toString()).contains("CATEGORYALLOCATIONS.LIST")) {
                JSONObject bi = bill.getJSONObject("CATEGORYALLOCATIONS.LIST");
                String categ = String.valueOf(bi.get("CATEGORY"));
                //System.out.println("cost categ:" + categ);
                if (String.valueOf(bill.get("CATEGORYALLOCATIONS.LIST")).equalsIgnoreCase("")) {
                }
                else if (String.valueOf(bill.get("CATEGORYALLOCATIONS.LIST")).charAt(0) == '{') {
                    costCenters=new ArrayList<>();
                    JSONObject obj=bi.getJSONObject("COSTCENTREALLOCATIONS.LIST");
                    String costname= String.valueOf(obj.get("NAME"));
                    String costamount= String.valueOf(obj.get("AMOUNT"));
                    String costbillQty= String.valueOf(obj.get("BILLEDQTY"));
                    String costactualQty= String.valueOf(obj.get("ACTUALQTY"));
                    //System.out.println(costname+" "+costamount+" "+costactualQty+" "+costbillQty+" "+categ);
                    costCenters.add(new CostCenter(costname,costamount,costbillQty,costactualQty));
                }
                else if (String.valueOf(bill.get("CATEGORYALLOCATIONS.LIST")).charAt(0) == '[') {
                    JSONArray obj=bi.getJSONArray("COSTCENTREALLOCATIONS.LIST");
                    for(int i=0;i<obj.length();i++)
                    {
                        costCenters=new ArrayList<>();
                        JSONObject single=obj.getJSONObject(i);
                        String costname= String.valueOf(single.get("NAME"));
                        String costamount= String.valueOf(single.get("AMOUNT"));
                        String costbillQty= String.valueOf(single.get("BILLEDQTY"));
                        String costactualQty= String.valueOf(single.get("ACTUALQTY"));
                        //System.out.println(costname+" "+costamount+" "+costactualQty+" "+costbillQty+" "+categ);
                        costCenters.add(new CostCenter(costname,costamount,costbillQty,costactualQty));
                    }
                }
            }
            listItes.add(new JournalLedger(amount, ledgerName,paymentBills));
        }

        return listItes;
    }
}