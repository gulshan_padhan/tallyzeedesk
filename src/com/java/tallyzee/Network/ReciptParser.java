package com.java.tallyzee.Network;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.java.tallyzee.Utility.AppParserUtil;
import net.projectmonkey.object.mapper.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import com.java.tallyzee.Models.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReciptParser implements Runnable{
    String companyName,userName;
    String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    String[] monthsCount = {"31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"};
    String[] daysList = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
    ArrayList<SalesVoucher> arrayList = new ArrayList<>();
SalesCallBcak salesCallBcak;
ReciptParser(String userName,String companyName,ParserController parserController)
{
    salesCallBcak=parserController;
    this.userName=userName;
    this.companyName=companyName;
}

    public String test(String responseXml) {
        JSONObject obj = null;
        try {
            obj = XML.toJSONObject(responseXml);
            System.out.println("json" + obj.toString());
            //printJson(obj.toString());
        } catch (Exception e) {
            System.out.println(e);
        }
        return obj.toString();
    }

    public String getPayRoll(String fromdate, String todate) {
        String payroll = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">" + fromdate + "</SVFROMDATE>   <SVTODATE TYPE=\"Date\">" + todate + "</SVTODATE>         <SVCURRENTCOMPANY>"+companyName+"</SVCURRENTCOMPANY>        </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsReceipt:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";

        return payroll;
    }

    public List<PaymentVoucher> actionPerformed() {
        String test = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">20170404</SVFROMDATE>   <SVTODATE TYPE=\"Date\">20170404</SVTODATE>                 </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsReceipt:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";

        String name = "", email;



//        ObjectMapper mapper = new ObjectMapper();
//        // As an admin, the app has access to read and write all data, regardless of Security Rules
//        DatabaseReference ref = FirebaseDatabase.getInstance()
//                .getReferenceFromUrl("https://tallyfy-364a0.firebaseio.com");
        String fromDay, toDay;
        String year = "2016";
        int count = 0;
        ArrayList<String> vouchernumber = new ArrayList<>();
        ArrayList<String> vouchernumber1 = new ArrayList<>();
//        int yr= Integer.valueOf(AppParserUtil.getCompany_date().substring(0,3));
//        int crYr;
//        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//        Date date = new Date();
//        crYr=Integer.valueOf(dateFormat.format(date).substring(0,dateFormat.format(date).indexOf("/")));
//        int lengthYaer=1;
//        if((crYr-yr)>0)
//        {
//            lengthYaer=crYr-yr;
//        }
//        else
//        {
//            lengthYaer=1;
//        }
//        System.out.println(lengthYaer);
        ArrayList<PaymentVoucher> listReciept=new ArrayList<>();

                    ArrayList<PaymentVoucher> salesVouchers = new ArrayList<>();
                    //System.out.println(getPayRoll(fromDay, fromDay));
        //AppParserUtil.getCompany_date()
                    LocalDate bday = LocalDate.of(Integer.valueOf(AppParserUtil.getCompany_date().substring(0,4)), Month.of(Integer.valueOf(AppParserUtil.getCompany_date().substring(4,6))),Integer.valueOf(AppParserUtil.getCompany_date().substring(6,AppParserUtil.getCompany_date().length())));
                    LocalDate today=  LocalDate.now().minusDays(30);
                    for (LocalDate date1 = bday; date1.isBefore(today); date1 = date1.plusDays(30))
                    {
                        System.out.println(date1);
                        LocalDate localDate=date1.plusDays(30);
                        salesVouchers = (ArrayList<PaymentVoucher>) jsonTOPaymentVoucher(test(NetworkServices.getResponse(getPayRoll(date1.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy")), localDate.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy"))))));
                        // System.out.println("the" + count + "response size is:" + salesVouchers.size());
                        if (salesVouchers.size() > 0) {
//
                            for (int i = 0; i < salesVouchers.size(); i++) {
                                listReciept.add(salesVouchers.get(i));
                                vouchernumber1.add(salesVouchers.get(i).getVoucherNumber());
                                final int temp = i;
                                final PaymentVoucher sales = salesVouchers.get(i);
//                       ref.child("Users").child(userName).child("companyListData").child(companyName.replaceAll("[\\-\\+\\.\\^:,]","")).child("Receipt_Vouchers").child(String.valueOf(count)).setValue(salesVouchers.get(i), new DatabaseReference.CompletionListener() {
//                      //      ref.child("Demo Company").child("Receipt_Vouchers").child(String.valueOf(count)).setValue(salesVouchers.get(i), new DatabaseReference.CompletionListener() {
//                                @Override
//                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                                    vouchernumber.add(sales.getVoucherNumber());
//                                }
//                            });
                                count++;
                            }
                        }
                    }

                    try {
                        //  Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }





        vouchernumber.forEach((response) -> {
            System.out.println(response);
        });
        System.out.println(vouchernumber.size() + " " + vouchernumber1.size());
        return  listReciept;
    }

    @Override
    public void run() {
    ArrayList<PaymentVoucher> list=new ArrayList<>();
        list= (ArrayList<PaymentVoucher>) actionPerformed();
        salesCallBcak.getRecieptResponse(list);
    }

    //    public Object InitializeControls()
//    {
//        SalesVoucher sales = new SalesVoucher();
//        String address = sales.getAddress();
//        String buyerName = sales.getBasicBuyerName();
//        String partyName = sales.getPartyName();
//        String amount = sales.getAmount();
//        String voucherType = sales.getVoucherType();
//        String parentVoucherType = sales.getParentVoucherType();
//        String partyLedgerName = sales.getPartyLedgerName();
//        String reference = sales.getReference();
//        String basicBasePartyName = sales.getBasicBasePartyName();
//         object_sales = new SalesVoucher(address,buyerName,partyName,amount,voucherType,parentVoucherType,partyLedgerName
//        ,reference,basicBasePartyName);
//
//        return object_sales;
//    }
    public static class PC {
        // Prints a string and waits for consume()
        public void produce() throws InterruptedException {
            // synchronized block ensures only one thread
            // running at a time.
            synchronized (this) {
                System.out.println("producer thread running");

                // releases the lock on shared resource
                wait();

                // and waits till some other method invokes notify().
                System.out.println("Resumed");
            }
        }

        // Sleeps for some time and waits for a key press. After key
        // is pressed, it notifies produce().
        public void consume() throws InterruptedException {
            // this makes the produce thread to run first.
            Thread.sleep(1000);
            // Scanner s = new Scanner(System.in);

            // synchronized block ensures only one thread
            // running at a time.
            synchronized (this) {
                System.out.println("Waiting for return key.");
                // s.nextLine();

                System.out.println("Return key pressed");

                // notifies the produce thread that it
                // can wake up.
                notify();

                // Sleep
                Thread.sleep(2000);
            }
        }
    }

    List<PaymentVoucher> jsonTOPaymentVoucher(String json) {

        String voucherDate;
        String voucherPartyName;
        String voucherNumber;
        String voucherNarration;
        String voucherTypeName;

        List<PaymentLedger> items = null;

        List<PaymentVoucher> salesVouchers = new ArrayList<>();
        List<Inventories> inventoriesList = new ArrayList<>();
        try {
            JSONObject main = new JSONObject(json);
            JSONObject envlop = main.getJSONObject("ENVELOPE");

            JSONObject body = envlop.getJSONObject("BODY");
            JSONObject desc = body.getJSONObject("DESC");
            JSONObject stavar = desc.getJSONObject("STATICVARIABLES");
            companyName = String.valueOf(stavar.get("SVCURRENTCOMPANY"));
            JSONObject data = body.getJSONObject("DATA");
            salesVouchers = new ArrayList<>();
            if (!data.equals(null) && data.isNull("DATA")) {
                if(String.valueOf(data.get("TALLYMESSAGE")).charAt(0)=='{')
                {
                    items=new ArrayList<>();
                    JSONObject tamsg = data.getJSONObject("TALLYMESSAGE");
                    JSONObject voucher = tamsg.getJSONObject("VOUCHER");
                    if(String.valueOf(voucher).contains("DATE"))
                        voucherDate = String.valueOf(voucher.get("DATE"));
                    else
                        voucherDate=null;
                    if(String.valueOf(voucher).contains("PARTYLEDGERNAME"))
                        voucherPartyName = String.valueOf(voucher.get("PARTYLEDGERNAME"));
                    else
                        voucherPartyName=null;
                    if(String.valueOf(voucher).contains("VOUCHERNUMBER"))
                        voucherNumber = String.valueOf(voucher.get("VOUCHERNUMBER"));
                    else
                        voucherNumber=null;
                    if(String.valueOf(voucher).contains("VOUCHERTYPENAME"))
                        voucherTypeName = String.valueOf(voucher.get("VOUCHERTYPENAME"));
                    else
                        voucherTypeName=null;
                    if(String.valueOf(voucher).contains("NARRATION"))
                        voucherNarration = String.valueOf(voucher.get("NARRATION"));
                    else
                        voucherNarration=null;
                    System.out.println(voucherPartyName + " " + voucherDate + " " + voucherNumber + " " + voucherTypeName + " " + voucherNarration);
                    items = getAllLedgerItem(voucher);
                    salesVouchers.add(new PaymentVoucher(voucherDate, voucherPartyName, voucherTypeName, "Reciept", voucherNumber, voucherNarration, items));

                }else if (String.valueOf(data.get("TALLYMESSAGE")).charAt(0)=='[')
                {
                    items=new ArrayList<>();
                    JSONArray tallymsg = data.getJSONArray("TALLYMESSAGE");
                    for (int i = 0; i < tallymsg.length(); i++) {
                        JSONObject tamsg = tallymsg.getJSONObject(i);
                        JSONObject voucher = tamsg.getJSONObject("VOUCHER");
                        if(String.valueOf(voucher).contains("DATE"))
                            voucherDate = String.valueOf(voucher.get("DATE"));
                        else
                            voucherDate=null;
                        if(String.valueOf(voucher).contains("PARTYLEDGERNAME"))
                            voucherPartyName = String.valueOf(voucher.get("PARTYLEDGERNAME"));
                        else
                            voucherPartyName=null;
                        if(String.valueOf(voucher).contains("VOUCHERNUMBER"))
                            voucherNumber = String.valueOf(voucher.get("VOUCHERNUMBER"));
                        else
                            voucherNumber=null;
                        if(String.valueOf(voucher).contains("VOUCHERTYPENAME"))
                            voucherTypeName = String.valueOf(voucher.get("VOUCHERTYPENAME"));
                        else
                            voucherTypeName=null;
                        if(String.valueOf(voucher).contains("NARRATION"))
                            voucherNarration = String.valueOf(voucher.get("NARRATION"));
                        else
                            voucherNarration=null;
                        System.out.println(voucherPartyName + " " + voucherDate + " " + voucherNumber + " " + voucherTypeName + " " + voucherNarration);
                        items = getAllLedgerItem(voucher);
                        salesVouchers.add(new PaymentVoucher(voucherDate, voucherPartyName, voucherTypeName, "Reciept", voucherNumber, voucherNarration, items));
                    }
                }
                //  System.out.println(tallymsg.length());


            }


        } catch (Exception e) {
            e.getMessage();
        }
        if (salesVouchers.size() == 0) {
            salesVouchers = new ArrayList<>();
        }
        return salesVouchers;
    }

    List<PaymentLedger> getAllLedgerItem(JSONObject voucher) {
        String amount;

        List<PaymentBank> paymentBanks = null;
        List<PaymentBill> paymentBills = null;
        List<CostCenter> costCenters=null;
        String ledgerName;
        String billName;
        String billAmount;
        String billType;
        String billCreditPeriod;
        String bankPaqartyName ;
        String bankamount ;
        String transcationType ;
        String date;
        String accNumber;
        String email;
        ArrayList<PaymentLedger> listItes = new ArrayList<>();
        if(String.valueOf(voucher).contains("ALLLEDGERENTRIES.LIST"))
        {
            if(String.valueOf(voucher.get("ALLLEDGERENTRIES.LIST")).equalsIgnoreCase(""))
            {

            }
            else if(String.valueOf(voucher.get("ALLLEDGERENTRIES.LIST")).charAt(0)=='{')
            {

                JSONObject bill = voucher.getJSONObject("ALLLEDGERENTRIES.LIST");
                paymentBanks=new ArrayList<>();
                paymentBills=new ArrayList<>();
                costCenters=new ArrayList<>();

                amount = String.valueOf(bill.get("AMOUNT"));
                ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                    String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST"));
                    if(op.equalsIgnoreCase(""))
                    {

                    }
                    else if (op.charAt(0) == '{') {

                        paymentBills=new ArrayList<>();
                        JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                        if(String.valueOf(billl).contains("AMOUNT"))
                            billAmount = String.valueOf(billl.get("AMOUNT"));
                        else
                            billAmount=null;
                        if(String.valueOf(billl).contains("BILLCREDITPERIOD"))
                            billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                        else
                            billCreditPeriod=null;
                        if(String.valueOf(billl).contains("BILLTYPE"))
                            billType = String.valueOf(billl.get("BILLTYPE"));
                        else
                            billType=null;
                        if(String.valueOf(billl).contains("NAME"))
                            billName = String.valueOf(billl.get("NAME"));
                        else
                            billName=null;
                        System.out.println(billAmount + " " + billCreditPeriod + " " + billType + " " + billName);
                        paymentBills.add(new PaymentBill(billName, billAmount, billType, billCreditPeriod));
                    } else if (op.charAt(0) == '[') {
                        System.out.println("ok tasted 2");
                        paymentBills=new ArrayList<>();
                        JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");

                        for (int b = 0; b < billlist.length(); b++) {
                            JSONObject billItems = billlist.getJSONObject(b);
                            if(String.valueOf(billItems).contains("AMOUNT"))
                                billAmount = String.valueOf(billItems.get("AMOUNT"));
                            else
                                billAmount=null;
                            if(String.valueOf(billItems).contains("BILLCREDITPERIOD"))
                                billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                            else
                                billCreditPeriod=null;
                            if(String.valueOf(billItems).contains("BILLTYPE"))
                                billType = String.valueOf(billItems.get("BILLTYPE"));
                            else
                                billType=null;
                            if(String.valueOf(billItems).contains("NAME"))
                                billName = String.valueOf(billItems.get("NAME"));
                            else
                                billName=null;

                            paymentBills.add(new PaymentBill(billName, billAmount, billType, billCreditPeriod));
                            System.out.println("bill" + " " + billName + " " + billType + " " + billAmount + " " + billCreditPeriod + " ");
                        }
                    } else {
                        System.out.println("ok null");
                    }
                }
                ArrayList<BankItem> bankItems=new ArrayList<BankItem>();
                if(String.valueOf(bill).contains("BANKALLOCATIONS.LIST")) {
                    String ba = String.valueOf(bill.get("BANKALLOCATIONS.LIST")).toString();

                    if(ba.equalsIgnoreCase(""))
                    {

                    }
                    else if (ba.charAt(0) == '{') {
                        paymentBanks=new ArrayList<>();
                        JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                        if(String.valueOf(billl).contains("BANKPARTYNAME"))
                            bankPaqartyName = String.valueOf(billl.get("BANKPARTYNAME"));
                        else
                            bankPaqartyName=null;
                        if(String.valueOf(billl).contains("AMOUNT"))
                            bankamount = String.valueOf(billl.get("AMOUNT"));
                        else
                            bankamount=null;
                        if(String.valueOf(billl).contains("TRANSACTIONTYPE"))
                            transcationType = String.valueOf(billl.get("TRANSACTIONTYPE"));
                        else
                            transcationType=null;
                        if(String.valueOf(billl).contains("DATE"))
                            date = String.valueOf(billl.get("DATE"));
                        else
                            date=null;
                        if(String.valueOf(billl).contains("ACCOUNTNUMBER"))
                            accNumber = String.valueOf(billl.get("ACCOUNTNUMBER"));
                        else
                            accNumber=null;
                        if(String.valueOf(billl).contains("EMAIL"))
                            email = String.valueOf(billl.get("EMAIL"));
                        else
                            email=null;
                        paymentBanks.add(new PaymentBank(accNumber, bankPaqartyName, amount, transcationType, date));
                        System.out.println(bankamount + " " + bankPaqartyName + " " + date + " " + accNumber + " " + email + " " + transcationType);
                    } else if (ba.charAt(0) == '[') {
                        System.out.print("ok tasted 2");
                        paymentBanks=new ArrayList<>();
                        // String reString = bill.toString();
                        JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                        for (int b = 0; b < billlist.length(); b++) {
                            JSONObject billl = billlist.getJSONObject(b);
                            if(String.valueOf(billl).contains("BANKPARTYNAME"))
                                bankPaqartyName = String.valueOf(billl.get("BANKPARTYNAME"));
                            else
                                bankPaqartyName=null;
                            if(String.valueOf(billl).contains("AMOUNT"))
                                bankamount = String.valueOf(billl.get("AMOUNT"));
                            else
                                bankamount=null;
                            if(String.valueOf(billl).contains("TRANSACTIONTYPE"))
                                transcationType = String.valueOf(billl.get("TRANSACTIONTYPE"));
                            else
                                transcationType=null;
                            if(String.valueOf(billl).contains("DATE"))
                                date = String.valueOf(billl.get("DATE"));
                            else
                                date=null;
                            if(String.valueOf(billl).contains("ACCOUNTNUMBER"))
                                accNumber = String.valueOf(billl.get("ACCOUNTNUMBER"));
                            else
                                accNumber=null;
                            if(String.valueOf(billl).contains("EMAIL"))
                                email = String.valueOf(billl.get("EMAIL"));
                            else
                                email=null;
                            paymentBanks.add(new PaymentBank(accNumber, bankPaqartyName, amount, transcationType, date));
                            System.out.println(bankamount + " " + bankPaqartyName + " " + date + " " + accNumber + " " + email + " " + transcationType);
                        }
                    } else {
                        System.out.println("ok null");
                    }
                }
                if(String.valueOf(bill.toString()).contains("CATEGORYALLOCATIONS.LIST")) {
                    JSONObject bi = bill.getJSONObject("CATEGORYALLOCATIONS.LIST");
                    String categ = String.valueOf(bi.get("CATEGORY"));
                    System.out.println("cost categ:" + categ);
                    if (String.valueOf(bill.get("CATEGORYALLOCATIONS.LIST")).equalsIgnoreCase("")) {
                    }
                    else if (String.valueOf(bill.get("CATEGORYALLOCATIONS.LIST")).charAt(0) == '{') {
                        costCenters=new ArrayList<>();
                        JSONObject obj=bi.getJSONObject("COSTCENTREALLOCATIONS.LIST");
                        String costname=String.valueOf(obj.get("NAME"));
                        String costamount=String.valueOf(obj.get("AMOUNT"));
                        String costbillQty=String.valueOf(obj.get("BILLEDQTY"));
                        String costactualQty=String.valueOf(obj.get("ACTUALQTY"));
                        System.out.println(costname+" "+costamount+" "+costactualQty+" "+costbillQty+" "+categ);
                        costCenters.add(new CostCenter(costname,costamount,costbillQty,costactualQty));
                    }
                    else if (String.valueOf(bill.get("CATEGORYALLOCATIONS.LIST")).charAt(0) == '[') {
                        JSONArray obj=bi.getJSONArray("COSTCENTREALLOCATIONS.LIST");
                        costCenters=new ArrayList<>();
                        for(int i=0;i<obj.length();i++)
                        {

                            JSONObject single=obj.getJSONObject(i);
                            String costname=String.valueOf(single.get("NAME"));
                            String costamount=String.valueOf(single.get("AMOUNT"));
                            String costbillQty=String.valueOf(single.get("BILLEDQTY"));
                            String costactualQty=String.valueOf(single.get("ACTUALQTY"));
                            System.out.println(costname+" "+costamount+" "+costactualQty+" "+costbillQty+" "+categ);
                            costCenters.add(new CostCenter(costname,costamount,costbillQty,costactualQty));
                        }
                    }
                }
                listItes.add(new PaymentLedger(amount, ledgerName, paymentBills,paymentBanks,costCenters));
            }
            else  if(String.valueOf(voucher.get("ALLLEDGERENTRIES.LIST")).charAt(0)=='[')
            {
                JSONArray list = voucher.getJSONArray("ALLLEDGERENTRIES.LIST");
                paymentBanks=new ArrayList<>();
                paymentBills=new ArrayList<>();
                costCenters=new ArrayList<>();
                for (int j = 0; j < list.length(); j++) {
                    JSONObject bill = list.getJSONObject(j);


                    amount = String.valueOf(bill.get("AMOUNT"));
                    ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                    if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                        String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST")).toString();
                        if(op.equalsIgnoreCase(""))
                        {

                        }
                        else if (op.charAt(0) == '{') {

                            paymentBills=new ArrayList<>();
                            JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                            if(String.valueOf(billl).contains("AMOUNT"))
                                billAmount = String.valueOf(billl.get("AMOUNT"));
                            else
                                billAmount=null;
                            if(String.valueOf(billl).contains("BILLCREDITPERIOD"))
                                billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                            else
                                billCreditPeriod=null;
                            if(String.valueOf(billl).contains("BILLTYPE"))
                                billType = String.valueOf(billl.get("BILLTYPE"));
                            else
                                billType=null;
                            if(String.valueOf(billl).contains("NAME"))
                                billName = String.valueOf(billl.get("NAME"));
                            else
                                billName=null;
                            System.out.println(billAmount + " " + billCreditPeriod + " " + billType + " " + billName);
                            paymentBills.add(new PaymentBill(billName, billAmount, billType, billCreditPeriod));
                        } else if (op.charAt(0) == '[') {
                            System.out.println("ok tasted 2");
                            paymentBills=new ArrayList<>();
                            JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");

                            for (int b = 0; b < billlist.length(); b++) {
                                JSONObject billItems = billlist.getJSONObject(b);
                                if(String.valueOf(billItems).contains("AMOUNT"))
                                    billAmount = String.valueOf(billItems.get("AMOUNT"));
                                else
                                    billAmount=null;
                                if(String.valueOf(billItems).contains("BILLCREDITPERIOD"))
                                    billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                                else
                                    billCreditPeriod=null;
                                if(String.valueOf(billItems).contains("BILLTYPE"))
                                    billType = String.valueOf(billItems.get("BILLTYPE"));
                                else
                                    billType=null;
                                if(String.valueOf(billItems).contains("NAME"))
                                    billName = String.valueOf(billItems.get("NAME"));
                                else
                                    billName=null;

                                paymentBills.add(new PaymentBill(billName, billAmount, billType, billCreditPeriod));
                                System.out.println("bill" + " " + billName + " " + billType + " " + billAmount + " " + billCreditPeriod + " ");
                            }
                        } else {
                            System.out.println("ok null");
                        }
                    }
                    ArrayList<BankItem> bankItems=new ArrayList<BankItem>();
                    if(String.valueOf(bill).contains("BANKALLOCATIONS.LIST")) {
                        String ba = String.valueOf(bill.get("BANKALLOCATIONS.LIST")).toString();

                        if(ba.equalsIgnoreCase(""))
                        {

                        }
                        else if (ba.charAt(0) == '{') {
                            paymentBanks=new ArrayList<>();
                            JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                            if(String.valueOf(billl).contains("BANKPARTYNAME"))
                                bankPaqartyName = String.valueOf(billl.get("BANKPARTYNAME"));
                            else
                                bankPaqartyName=null;
                            if(String.valueOf(billl).contains("AMOUNT"))
                                bankamount = String.valueOf(billl.get("AMOUNT"));
                            else
                                bankamount=null;
                            if(String.valueOf(billl).contains("TRANSACTIONTYPE"))
                                transcationType = String.valueOf(billl.get("TRANSACTIONTYPE"));
                            else
                                transcationType=null;
                            if(String.valueOf(billl).contains("DATE"))
                                date = String.valueOf(billl.get("DATE"));
                            else
                                date=null;
                            if(String.valueOf(billl).contains("ACCOUNTNUMBER"))
                                accNumber = String.valueOf(billl.get("ACCOUNTNUMBER"));
                            else
                                accNumber=null;
                            if(String.valueOf(billl).contains("EMAIL"))
                                email = String.valueOf(billl.get("EMAIL"));
                            else
                                email=null;
                            paymentBanks.add(new PaymentBank(accNumber, bankPaqartyName, amount, transcationType, date));
                            System.out.println(bankamount + " " + bankPaqartyName + " " + date + " " + accNumber + " " + email + " " + transcationType);
                        } else if (ba.charAt(0) == '[') {
                            System.out.print("ok tasted 2");
                            paymentBanks=new ArrayList<>();
                            // String reString = bill.toString();
                            JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                            for (int b = 0; b < billlist.length(); b++) {
                                JSONObject billl = billlist.getJSONObject(b);
                                if(String.valueOf(billl).contains("BANKPARTYNAME"))
                                    bankPaqartyName = String.valueOf(billl.get("BANKPARTYNAME"));
                                else
                                    bankPaqartyName=null;
                                if(String.valueOf(billl).contains("AMOUNT"))
                                    bankamount = String.valueOf(billl.get("AMOUNT"));
                                else
                                    bankamount=null;
                                if(String.valueOf(billl).contains("TRANSACTIONTYPE"))
                                    transcationType = String.valueOf(billl.get("TRANSACTIONTYPE"));
                                else
                                    transcationType=null;
                                if(String.valueOf(billl).contains("DATE"))
                                    date = String.valueOf(billl.get("DATE"));
                                else
                                    date=null;
                                if(String.valueOf(billl).contains("ACCOUNTNUMBER"))
                                    accNumber = String.valueOf(billl.get("ACCOUNTNUMBER"));
                                else
                                    accNumber=null;
                                if(String.valueOf(billl).contains("EMAIL"))
                                    email = String.valueOf(billl.get("EMAIL"));
                                else
                                    email=null;
                                paymentBanks.add(new PaymentBank(accNumber, bankPaqartyName, amount, transcationType, date));
                                System.out.println(bankamount + " " + bankPaqartyName + " " + date + " " + accNumber + " " + email + " " + transcationType);
                            }
                        } else {
                            System.out.println("ok null");
                        }
                    }
                    if(String.valueOf(bill).contains("CATEGORYALLOCATIONS.LIST")) {
                        JSONObject bi = bill.getJSONObject("CATEGORYALLOCATIONS.LIST");
                        String categ = String.valueOf(bi.get("CATEGORY"));
                        System.out.println("cost categ:" + categ);
                        if (String.valueOf(bill.get("CATEGORYALLOCATIONS.LIST")).equalsIgnoreCase("")) {
                        }
                        else if (String.valueOf(bill.get("CATEGORYALLOCATIONS.LIST")).charAt(0) == '{') {
                            costCenters=new ArrayList<>();
                            JSONObject obj=bi.getJSONObject("COSTCENTREALLOCATIONS.LIST");
                            String costname=String.valueOf(obj.get("NAME"));
                            String costamount=String.valueOf(obj.get("AMOUNT"));
                            String costbillQty=String.valueOf(obj.get("BILLEDQTY"));
                            String costactualQty=String.valueOf(obj.get("ACTUALQTY"));
                            System.out.println(costname+" "+costamount+" "+costactualQty+" "+costbillQty+" "+categ);
                            costCenters.add(new CostCenter(costname,costamount,costbillQty,costactualQty));
                        }
                        else if (String.valueOf(bill.get("CATEGORYALLOCATIONS.LIST")).charAt(0) == '[') {
                            JSONArray obj=bi.getJSONArray("COSTCENTREALLOCATIONS.LIST");
                            costCenters=new ArrayList<>();
                            for(int i=0;i<obj.length();i++)
                            {

                                JSONObject single=obj.getJSONObject(i);
                                String costname=String.valueOf(single.get("NAME"));
                                String costamount=String.valueOf(single.get("AMOUNT"));
                                String costbillQty=String.valueOf(single.get("BILLEDQTY"));
                                String costactualQty=String.valueOf(single.get("ACTUALQTY"));
                                System.out.println(costname+" "+costamount+" "+costactualQty+" "+costbillQty+" "+categ);
                                costCenters.add(new CostCenter(costname,costamount,costbillQty,costactualQty));
                            }
                        }
                    }
                    listItes.add(new PaymentLedger(amount, ledgerName, paymentBills,paymentBanks,costCenters));
                }
            }
        }


        return listItes;
    }
}