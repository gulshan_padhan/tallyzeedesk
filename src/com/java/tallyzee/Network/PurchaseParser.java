package com.java.tallyzee.Network;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.java.tallyzee.Utility.AppParserUtil;
import net.projectmonkey.object.mapper.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import com.java.tallyzee.Models.*;

import javax.swing.plaf.synth.SynthTextAreaUI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PurchaseParser implements Runnable {
    String companyName,userName;
    String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    String[] monthsCount = {"31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"};
    String[] daysList = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
    ArrayList<SalesVoucher> arrayList = new ArrayList<>();
    // static SalesParser n = new SalesParser();
SalesCallBcak salesCallBcak;
    @Override
    public void run() {
System.out.println(((ArrayList<SalesVoucher>) actionPerformed()).size());
        salesCallBcak.getPurchaseResponse((ArrayList<SalesVoucher>) actionPerformed());;
    }
    PurchaseParser(String userName,String companyName,ParserController parserController)
    {
        salesCallBcak=parserController;
        this.companyName=companyName;
        this.userName=userName;
    }

    public String test(String responseXml) {
        JSONObject obj = null;
        try {
            obj = XML.toJSONObject(responseXml);
            //System.out.println("json" + obj.toString());
            //printJson(obj.toString());
        } catch (Exception e) {
            //System.out.println(e);
        }
        return obj.toString();
    }

    public String getPayRoll(String fromdate, String todate) {
        String payroll = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">" + fromdate + "</SVFROMDATE>   <SVTODATE TYPE=\"Date\">" + todate + "</SVTODATE>        <SVCURRENTCOMPANY>"+companyName+"</SVCURRENTCOMPANY>         </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsPurchase:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";

        return payroll;
    }

    public List<SalesVoucher> actionPerformed() {
        String test = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">20170404</SVFROMDATE>   <SVTODATE TYPE=\"Date\">20170404</SVTODATE>                 </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsPurchase:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";

        String name = "", email;

        //System.out.println("OUT OF EXCEPTION");

        //System.out.println("OUT OF EXCEPTION");
//        ObjectMapper mapper = new ObjectMapper();

//        SalesVoucher object_sales = new SalesVoucher(address,buyerName,partyName,amount,voucherType,parentVoucherType,partyLedgerName
//                ,reference,basicBasePartyName);
//            //System.out.println(object_sales.getAddress());
        // As an admin, the app has access to read and write all data, regardless of Security Rules
//        DatabaseReference ref = FirebaseDatabase.getInstance()
//                .getReferenceFromUrl("https://tallyfy-364a0.firebaseio.com");
//        String fromDay, toDay;
//        String year = "2016";
        int count = 0;
        ArrayList<String> vouchernumber = new ArrayList<>();
        ArrayList<String> vouchernumber1 = new ArrayList<>();
//        int yr=2016;
//        int crYr;
//        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//        Date date = new Date();
//        //System.out.println();
//        crYr=Integer.valueOf(dateFormat.format(date).substring(0,dateFormat.format(date).indexOf("/")));
//        int lengthYaer=1;
//        if((crYr-yr)>0)
//        {
//            lengthYaer=crYr-yr;
//        }
//        else
//        {
//            lengthYaer=1;
//        }
        ArrayList<SalesVoucher> listPur=new ArrayList<>();
//        for(int ped=0;ped<lengthYaer+1;ped++)
//        {
//            String rt=String.valueOf(yr);
//            for (int days = 0; days < monthsCount.length; days++) {
//                for (int j = 0; j < Integer.valueOf(monthsCount[days]); j++) {
//                    fromDay = daysList[j] + "" + months[days] + "" + rt;
                    ArrayList<SalesVoucher> salesVouchers = new ArrayList<>();
                    ////System.out.println(getPayRoll(fromDay, fromDay));
                    LocalDate bday = LocalDate.of(Integer.valueOf(AppParserUtil.getCompany_date().substring(0,4)), Month.of(Integer.valueOf(AppParserUtil.getCompany_date().substring(4,6))),Integer.valueOf(AppParserUtil.getCompany_date().substring(6,AppParserUtil.getCompany_date().length())));
                    LocalDate today=  LocalDate.now().minusDays(30);
                    for (LocalDate date1 = bday; date1.isBefore(today); date1 = date1.plusDays(30)) {
                        LocalDate localDate=date1.plusDays(30);
                        String payload=test(NetworkServices.getResponse(getPayRoll(date1.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy")), localDate.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy")))));
                        System.out.println(payload);
                        salesVouchers = (ArrayList<SalesVoucher>) printJson(payload);
                        System.out.println("the" + count + "response size is:" + salesVouchers.size());
                        if (salesVouchers.size() > 0) {
                            for (int i = 0; i < salesVouchers.size(); i++) {
                                listPur.add(salesVouchers.get(i));
                                vouchernumber1.add(salesVouchers.get(i).getVoucherNumber());
                                final int temp = i;
                                final SalesVoucher sales = salesVouchers.get(i);
                                count++;
                            }
                        }
                    }

                    try {
                        //  Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }





        return  listPur;
        //System.out.println(vouchernumber.size() + " " + vouchernumber1.size());
    }

    //    public Object InitializeControls()
//    {
//        SalesVoucher sales = new SalesVoucher();
//        String address = sales.getAddress();
//        String buyerName = sales.getBasicBuyerName();
//        String partyName = sales.getPartyName();
//        String amount = sales.getAmount();
//        String voucherType = sales.getVoucherType();
//        String parentVoucherType = sales.getParentVoucherType();
//        String partyLedgerName = sales.getPartyLedgerName();
//        String reference = sales.getReference();
//        String basicBasePartyName = sales.getBasicBasePartyName();
//         object_sales = new SalesVoucher(address,buyerName,partyName,amount,voucherType,parentVoucherType,partyLedgerName
//        ,reference,basicBasePartyName);
//
//        return object_sales;
//    }


    List<SalesVoucher> printJson(String json) {

        String voucherDate;
        String voucherPartyName;
        String voucherNumber;
        String voucherNarration;
        String voucherTypeName;
        String basicUserDesc;
        String rate="";
        String amount;
        int edqty;
        int actualQty;

        String stockName="";
        String gowdownName;
        String batchName;
        Double batchamount;
        int batchactualQty;
        int billQty;
        Date orderDueDate;
        List<LedgerItem> items = null;
        List<Item> inventories = null;
        Double basicOrderRate;
        String orderType;
        String basicPurchaseOrderNumber;
        Double orderamount;
        String ledgerName;
        String billName;
        String billAmount;
        String billType;
        String billCreditPeriod;
        List<SalesVoucher> salesVouchers = new ArrayList<>();
        List<InvoiceOrderList> invoiceOrderLists = null;
        List<Inventories> inventoriesList = new ArrayList<>();
        try {
            JSONObject main = new JSONObject(json);
            JSONObject envlop = main.getJSONObject("ENVELOPE");

            JSONObject body = envlop.getJSONObject("BODY");
            JSONObject desc = body.getJSONObject("DESC");
            JSONObject stavar = desc.getJSONObject("STATICVARIABLES");
            // //System.out.println(stavar.get("SVCURRENTCOMPANY"));
            companyName = String.valueOf(stavar.get("SVCURRENTCOMPANY"));
            JSONObject data = body.getJSONObject("DATA");
            salesVouchers = new ArrayList<>();
            if (!data.equals(null) && data.isNull("DATA")) {
                if (String.valueOf(data.get("TALLYMESSAGE")).charAt(0) == '{') {

                    invoiceOrderLists = new ArrayList<>();
                    JSONObject tamsg = data.getJSONObject("TALLYMESSAGE");
                    ////System.out.println(tamsg);
                    JSONObject voucher = tamsg.getJSONObject("VOUCHER");
                    inventoriesList = new ArrayList<>();
                    //  System.out.print("Voucher" + " " + voucher.get("DATE") + " " + voucher.getString("PARTYLEDGERNAME") + " " + voucher.getString("VOUCHERTYPENAME") + " " + voucher.getString("VOUCHERNUMBER"));
                    //  System.out.print("Voucher" + " " + voucher.get("DATE") + " " + voucher.getString("PARTYLEDGERNAME") + " " + voucher.getString("VOUCHERTYPENAME") + " " + voucher.getString("VOUCHERNUMBER"));
                    if(String.valueOf(voucher).contains("DATE"))
                        voucherDate = String.valueOf(voucher.get("DATE"));
                    else
                        voucherDate=null;
                    if(String.valueOf(voucher).contains("PARTYLEDGERNAME"))
                        voucherPartyName = String.valueOf(voucher.get("PARTYLEDGERNAME"));
                    else
                        voucherPartyName=null;
                    if(String.valueOf(voucher).contains("VOUCHERNUMBER"))
                        voucherNumber = String.valueOf(voucher.get("VOUCHERNUMBER"));
                    else
                        voucherNumber=null;
                    if(String.valueOf(voucher).contains("VOUCHERTYPENAME"))
                        voucherTypeName = String.valueOf(voucher.get("VOUCHERTYPENAME"));
                    else
                        voucherTypeName=null;
                    if(String.valueOf(voucher).contains("NARRATION"))
                        voucherNarration = String.valueOf(voucher.get("NARRATION"));
                    else
                        voucherNarration=null;
                    //System.out.println(voucherPartyName + " " + voucherDate + " " + voucherNumber + " " + voucherTypeName + " " + voucherNarration);
                    //voucherType v;
                    //System.out.print(" " + voucher.get("NARRATION"));
                    String var = String.valueOf(voucher.get("ALLINVENTORYENTRIES.LIST"));
                    if (var.equalsIgnoreCase("")) {


                    } else if (var.charAt(0) == '{') {
                        JSONObject inventory = voucher.getJSONObject("ALLINVENTORYENTRIES.LIST");

                        // JSONObject bsc = inventory.getJSONObject("BASICUSERDESCRIPTION.LIST");
//                        //System.out.println(bsc.get("BASICUSERDESCRIPTION") + " " + inventory.get("STOCKITEMNAME") + " " + inventory.get("RATE") + " " + inventory.get("AMOUNT") + " " + inventory.get("BILLEDQTY") + " " + inventory.get("ACTUALQTY"));

                        //  basicUserDesc = String.valueOf(bsc.get("BASICUSERDESCRIPTION"));
                        List<BatchAllocation> batchAllocationList = new ArrayList<>();
                        if (String.valueOf(inventory.get("BATCHALLOCATIONS.LIST")).equalsIgnoreCase("")) {

                        } else if (String.valueOf(inventory.get("BATCHALLOCATIONS.LIST")).charAt(0) == '{') {
                            JSONObject batch = inventory.getJSONObject("BATCHALLOCATIONS.LIST");
                            //  //System.out.println(batch.getString("GODOWNNAME") + " " + batch.get("BATCHNAME") + batch.get("ORDERNO") + batch.get("AMOUNT") + " " + batch.get("ACTUALQTY") + " " + batch.get("BILLEDQTY"));

                            String overduevalue = String.valueOf(batch.get("ORDERDUEDATE"));
                            JSONObject overdue;
                            String p = null;
                            if (overduevalue.equalsIgnoreCase("")) {
                                p = null;
                            } else if (overduevalue.charAt(0) == '{') {
                                overdue = batch.getJSONObject("ORDERDUEDATE");
                                p = String.valueOf(overdue.get("P"));
                            } else {
                                p = null;
                            }

                            gowdownName = String.valueOf(batch.get("GODOWNNAME"));
                            batchName = String.valueOf(batch.get("BATCHNAME"));

                            basicPurchaseOrderNumber = String.valueOf(batch.get("ORDERNO"));
                            String batchamount1 = String.valueOf(batch.get("AMOUNT"));
                            String batchactualQty1 = String.valueOf(batch.get("ACTUALQTY"));
                            //System.out.println(gowdownName + " " + batchName + " " + basicPurchaseOrderNumber + " " + batchamount1 + " " + batchactualQty1);

//                    ////System.out.println(overdue.get("P"));
                            String batchbillqty = String.valueOf(batch.get("BILLEDQTY"));
                            batchAllocationList.add(new BatchAllocation(gowdownName, batchName, String.valueOf(basicPurchaseOrderNumber), String.valueOf(batchamount1), String.valueOf(batchactualQty1), String.valueOf(batchbillqty), p));
                            stockName = String.valueOf(inventory.get("STOCKITEMNAME"));
                            rate = String.valueOf(inventory.get("RATE"));
                            String stockAmount = String.valueOf(inventory.get("AMOUNT"));
                            String stockBillQty = String.valueOf(inventory.get("BILLEDQTY"));
                            String stockActualQty = String.valueOf(inventory.get("ACTUALQTY"));
                            inventoriesList.add(new Inventories(stockName, "", rate, stockAmount, stockBillQty, stockActualQty, batchAllocationList));

                        }


                        // DashType type = DashType.valueOf(voucher.getString("VOUCHERTYPENAME"));
                        String invenString = String.valueOf(voucher.get("INVOICEORDERLIST.LIST"));
                        if (invenString.equalsIgnoreCase("")) {

                        } else if (invenString.charAt(0) == '{') {
                            JSONObject invoicebill = voucher.getJSONObject("INVOICEORDERLIST.LIST");
                            String invoicebrt = String.valueOf(invoicebill.get("BASICORDERDATE"));
                            orderType = String.valueOf(invoicebill.get("ORDERTYPE"));
                            String bsrd = String.valueOf(invoicebill.get("BASICPURCHASEORDERNO"));
                            //System.out.println("invoicebill" + invoicebrt + " " + orderType + " " + bsrd);
                        } else {

                        }


                    } else if (var.charAt(0) == '[') {

                        JSONArray inventory = voucher.getJSONArray("ALLINVENTORYENTRIES.LIST");
                        List<BatchAllocation> batchAllocationList = new ArrayList<>();
                        for(int l=0;l<inventory.length();l++)
                        {
                            JSONObject tr=inventory.getJSONObject(l);
                            String stockAmount="" ;
                            String stockBillQty ="";
                            String batchamount1 ="";
                            String batchactualQty1="" ;
                            String batchbillqty ="";
                            String stockActualQty="";
                            String p = null;
                            // JSONObject bsc = inventory.getJSONObject("BASICUSERDESCRIPTION.LIST");
//                        //System.out.println(bsc.get("BASICUSERDESCRIPTION") + " " + inventory.get("STOCKITEMNAME") + " " + inventory.get("RATE") + " " + inventory.get("AMOUNT") + " " + inventory.get("BILLEDQTY") + " " + inventory.get("ACTUALQTY"));

                            //  basicUserDesc = String.valueOf(bsc.get("BASICUSERDESCRIPTION"));

                            if (String.valueOf(tr.get("BATCHALLOCATIONS.LIST")).equalsIgnoreCase("")) {

                            } else if (String.valueOf(tr.get("BATCHALLOCATIONS.LIST")).charAt(0) == '{') {
                                JSONObject batch = tr.getJSONObject("BATCHALLOCATIONS.LIST");
                                //  //System.out.println(batch.getString("GODOWNNAME") + " " + batch.get("BATCHNAME") + batch.get("ORDERNO") + batch.get("AMOUNT") + " " + batch.get("ACTUALQTY") + " " + batch.get("BILLEDQTY"));

                                String overduevalue = String.valueOf(batch.get("ORDERDUEDATE"));
                                JSONObject overdue;

                                if (overduevalue.equalsIgnoreCase("")) {
                                    p = null;
                                } else if (overduevalue.charAt(0) == '{') {
                                    overdue = batch.getJSONObject("ORDERDUEDATE");
                                    p = String.valueOf(overdue.get("P"));
                                } else {
                                    p = null;
                                }

                                gowdownName = String.valueOf(batch.get("GODOWNNAME"));
                                batchName = String.valueOf(batch.get("BATCHNAME"));

                                basicPurchaseOrderNumber = String.valueOf(batch.get("ORDERNO"));
                                batchamount1 = String.valueOf(batch.get("AMOUNT"));
                                batchactualQty1 = String.valueOf(batch.get("ACTUALQTY"));
                                //System.out.println(gowdownName + " " + batchName + " " + basicPurchaseOrderNumber + " " + batchamount1 + " " + batchactualQty1);

//                    ////System.out.println(overdue.get("P"));
                                batchbillqty = String.valueOf(batch.get("BILLEDQTY"));
                                batchAllocationList.add(new BatchAllocation(gowdownName, batchName, String.valueOf(basicPurchaseOrderNumber), String.valueOf(batchamount1), String.valueOf(batchactualQty1), String.valueOf(batchbillqty), p));
                                stockName = String.valueOf(tr.get("STOCKITEMNAME"));
                                rate = String.valueOf(tr.get("RATE"));
                                stockAmount = String.valueOf(tr.get("AMOUNT"));
                                stockBillQty = String.valueOf(tr.get("BILLEDQTY"));
                                stockActualQty = String.valueOf(tr.get("ACTUALQTY"));

                            }
                            inventoriesList.add(new Inventories(stockName, "", rate, stockAmount, stockBillQty, stockActualQty, batchAllocationList));

                            // DashType type = DashType.valueOf(voucher.getString("VOUCHERTYPENAME"));
                            String invenString = String.valueOf(voucher.get("INVOICEORDERLIST.LIST"));
                            if (invenString.equalsIgnoreCase("")) {

                            } else if (invenString.charAt(0) == '{') {
                                JSONObject invoicebill = voucher.getJSONObject("INVOICEORDERLIST.LIST");
                                String invoicebrt = String.valueOf(invoicebill.get("BASICORDERDATE"));
                                orderType = String.valueOf(invoicebill.get("ORDERTYPE"));
                                String bsrd = String.valueOf(invoicebill.get("BASICPURCHASEORDERNO"));
                                //System.out.println("invoicebill" + invoicebrt + " " + orderType + " " + bsrd);
                            } else {

                            }
                        }



                    }
                    if (String.valueOf(voucher.get("LEDGERENTRIES.LIST")).equalsIgnoreCase("")) {

                    } else if (String.valueOf(voucher.get("LEDGERENTRIES.LIST")).charAt(0) == '{') {


                        items = new ArrayList<>();

                        JSONObject bill = voucher.getJSONObject("LEDGERENTRIES.LIST");
                        ////System.out.println(bill);
                        inventories = new ArrayList<>();
                        amount = String.valueOf(bill.get("AMOUNT"));
                        ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                        //  //System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");
                        if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                            String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST"));
                            ////System.out.println("the String is Object:"+op);
                            if (!op.equals(null) && !op.isEmpty())
                                if (op.equalsIgnoreCase("")) {

                                } else if (op.charAt(0) == '{') {

                                    inventories = new ArrayList<>();
                                    // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                    ////System.out.println("the String is Object:"+op);
                                    JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                    // //System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                    ////System.out.println("ok tasted 1");
                                    billAmount = String.valueOf(billl.get("AMOUNT"));
                                    billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                                    billType = String.valueOf(billl.get("BILLTYPE"));
                                    billName = String.valueOf(billl.get("NAME"));
                                    //System.out.println(billAmount + " " + bill + " " + billType + " " + billName);
                                    inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                } else if (op.charAt(0) == '[') {
                                    //System.out.println("ok tasted 2");
                                    // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                    inventories = new ArrayList<>();
                                    ////System.out.println("the String is Array:"+op);
                                    String reString = bill.toString();
                                    ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                    JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                    ////System.out.println("ok tasted 3");
                                    for (int b = 0; b < billlist.length(); b++) {
                                        JSONObject billItems = billlist.getJSONObject(b);
                                        billAmount = String.valueOf(billItems.get("AMOUNT"));
                                        billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                                        billType = String.valueOf(billItems.getString("BILLTYPE"));
                                        billName = String.valueOf(billItems.get("NAME"));

                                        inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                        //System.out.println("bill" + " " + billName + " " + billType + " " + bill + " " + billCreditPeriod + " ");
                                    }
                                    ////System.out.println("ok tasted 4");
                                    //e.printStackTrace();

                                } else {
                                    items = new ArrayList<>();
                                    //System.out.println("ok null");
                                }
                        }
                        if (String.valueOf(bill).contains("BANKALLOCATIONS.LIST")) {
                            String ba = String.valueOf(bill.getString("BANKALLOCATIONS.LIST"));
                            ////System.out.println("the String is Object:"+op);
                            if (!ba.equals(null) && !ba.isEmpty())
                                if (ba.equalsIgnoreCase("")) {

                                } else if (ba.charAt(0) == '{') {
                                    // ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                    //\//System.out.println("the String is Object:"+op);
                                    JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                    ////System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                    ////System.out.println("ok tasted 1");

                                } else if (ba.charAt(0) == '[') {
                                    System.out.print("ok tasted 2");
                                    // ba = bill.getString("BANKALLOCATIONS.LIST").toString();

                                    ////System.out.println("the String is Array:"+op);
                                    String reString = bill.toString();
                                    ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                    JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                    ////System.out.println("ok tasted 3");
                                    for (int b = 0; b < billlist.length(); b++) {
                                        JSONObject billItems = billlist.getJSONObject(b);
                                        // //System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                                    }
                                    ////System.out.println("ok tasted 4");
                                    //e.printStackTrace();

                                } else {
                                    //System.out.println("ok null");
                                }

                        }
                        items.add(new LedgerItem(amount, ledgerName, inventories, null));


                    } else if (String.valueOf(voucher.get("LEDGERENTRIES.LIST")).charAt(0) == '[') {
                        JSONArray list = voucher.getJSONArray("LEDGERENTRIES.LIST");

                        items = new ArrayList<>();
                        //System.out.println(list.length());
                        for (int j = 0; j < list.length(); j++) {
                            JSONObject bill = list.getJSONObject(j);
                            ////System.out.println(bill);
                            inventories = new ArrayList<>();
                            amount = String.valueOf(bill.get("AMOUNT"));
                            ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                            //  //System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");
                            if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                                String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST"));
                                ////System.out.println("the String is Object:"+op);
                                if (!op.equals(null) && !op.isEmpty())
                                    if (op.equalsIgnoreCase("")) {

                                    } else if (op.charAt(0) == '{') {

                                        inventories = new ArrayList<>();
                                        // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                        ////System.out.println("the String is Object:"+op);
                                        JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                        // //System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                        ////System.out.println("ok tasted 1");
                                        billAmount = String.valueOf(billl.get("AMOUNT"));
                                        billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                                        billType = String.valueOf(billl.get("BILLTYPE"));
                                        billName = String.valueOf(billl.get("NAME"));
                                        //System.out.println(billAmount + " " + bill + " " + billType + " " + billName);
                                        inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                    } else if (op.charAt(0) == '[') {
                                        //System.out.println("ok tasted 2");
                                        // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                        inventories = new ArrayList<>();
                                        ////System.out.println("the String is Array:"+op);
                                        String reString = bill.toString();
                                        ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                        JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                        ////System.out.println("ok tasted 3");
                                        for (int b = 0; b < billlist.length(); b++) {
                                            JSONObject billItems = billlist.getJSONObject(b);
                                            billAmount = String.valueOf(billItems.get("AMOUNT"));
                                            billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                                            billType = String.valueOf(billItems.getString("BILLTYPE"));
                                            billName = String.valueOf(billItems.get("NAME"));

                                            inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                            //System.out.println("bill" + " " + billName + " " + billType + " " + bill + " " + billCreditPeriod + " ");
                                        }
                                        ////System.out.println("ok tasted 4");
                                        //e.printStackTrace();

                                    } else {
                                        items = new ArrayList<>();
                                        //System.out.println("ok null");
                                    }
                            }
                            if (String.valueOf(bill).contains("BANKALLOCATIONS.LIST")) {
                                String ba = String.valueOf(bill.getString("BANKALLOCATIONS.LIST"));
                                ////System.out.println("the String is Object:"+op);
                                if (!ba.equals(null) && !ba.isEmpty())
                                    if (ba.equalsIgnoreCase("")) {

                                    } else if (ba.charAt(0) == '{') {
                                        // ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                        //\//System.out.println("the String is Object:"+op);
                                        JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                        ////System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                        ////System.out.println("ok tasted 1");

                                    } else if (ba.charAt(0) == '[') {
                                        System.out.print("ok tasted 2");
                                        // ba = bill.getString("BANKALLOCATIONS.LIST").toString();

                                        ////System.out.println("the String is Array:"+op);
                                        String reString = bill.toString();
                                        ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                        JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                        ////System.out.println("ok tasted 3");
                                        for (int b = 0; b < billlist.length(); b++) {
                                            JSONObject billItems = billlist.getJSONObject(b);
                                            // //System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                                        }
                                        ////System.out.println("ok tasted 4");
                                        //e.printStackTrace();

                                    } else {
                                        //System.out.println("ok null");
                                    }

                            }
                            items.add(new LedgerItem(amount, ledgerName, inventories, null));
                        }


                    }


                    salesVouchers.add(new SalesVoucher(voucherDate, voucherPartyName, voucherTypeName, "Purchase", voucherNumber, voucherNarration, inventoriesList, null, items));


                } else if (String.valueOf(data.get("TALLYMESSAGE")).charAt(0) == '[') {
                    JSONArray tallymsg = data.getJSONArray("TALLYMESSAGE");
                    ////System.out.println(tallymsg);
                    //System.out.println(tallymsg.length());
                    for (int i = 0; i < tallymsg.length(); i++) {
                        invoiceOrderLists = new ArrayList<>();
                        JSONObject tamsg = tallymsg.getJSONObject(i);
                        ////System.out.println(tamsg);
                        JSONObject voucher = tamsg.getJSONObject("VOUCHER");
                        inventoriesList = new ArrayList<>();
                        //  System.out.print("Voucher" + " " + voucher.get("DATE") + " " + voucher.getString("PARTYLEDGERNAME") + " " + voucher.getString("VOUCHERTYPENAME") + " " + voucher.getString("VOUCHERNUMBER"));
                        //  System.out.print("Voucher" + " " + voucher.get("DATE") + " " + voucher.getString("PARTYLEDGERNAME") + " " + voucher.getString("VOUCHERTYPENAME") + " " + voucher.getString("VOUCHERNUMBER"));
                        if(String.valueOf(voucher).contains("DATE"))
                            voucherDate = String.valueOf(voucher.get("DATE"));
                        else
                            voucherDate=null;
                        if(String.valueOf(voucher).contains("PARTYLEDGERNAME"))
                            voucherPartyName = String.valueOf(voucher.get("PARTYLEDGERNAME"));
                        else
                            voucherPartyName=null;
                        if(String.valueOf(voucher).contains("VOUCHERNUMBER"))
                            voucherNumber = String.valueOf(voucher.get("VOUCHERNUMBER"));
                        else
                            voucherNumber=null;
                        if(String.valueOf(voucher).contains("VOUCHERTYPENAME"))
                            voucherTypeName = String.valueOf(voucher.get("VOUCHERTYPENAME"));
                        else
                            voucherTypeName=null;
                        if(String.valueOf(voucher).contains("NARRATION"))
                            voucherNarration = String.valueOf(voucher.get("NARRATION"));
                        else
                            voucherNarration=null;
                        //System.out.println(voucherPartyName + " " + voucherDate + " " + voucherNumber + " " + voucherTypeName + " " + voucherNarration);
                        //voucherType v;
                        //System.out.print(" " + voucher.get("NARRATION"));
                        String var = String.valueOf(voucher.get("ALLINVENTORYENTRIES.LIST"));
                        if (var.equalsIgnoreCase("")) {


                        } else if (var.charAt(0) == '{') {
                            JSONObject inventory = voucher.getJSONObject("ALLINVENTORYENTRIES.LIST");

                            // JSONObject bsc = inventory.getJSONObject("BASICUSERDESCRIPTION.LIST");
//                        //System.out.println(bsc.get("BASICUSERDESCRIPTION") + " " + inventory.get("STOCKITEMNAME") + " " + inventory.get("RATE") + " " + inventory.get("AMOUNT") + " " + inventory.get("BILLEDQTY") + " " + inventory.get("ACTUALQTY"));

                            //  basicUserDesc = String.valueOf(bsc.get("BASICUSERDESCRIPTION"));
                            List<BatchAllocation> batchAllocationList = new ArrayList<>();
                            if (String.valueOf(inventory.get("BATCHALLOCATIONS.LIST")).equalsIgnoreCase("")) {

                            } else if (String.valueOf(inventory.get("BATCHALLOCATIONS.LIST")).charAt(0) == '{') {
                                JSONObject batch = inventory.getJSONObject("BATCHALLOCATIONS.LIST");
                                //  //System.out.println(batch.getString("GODOWNNAME") + " " + batch.get("BATCHNAME") + batch.get("ORDERNO") + batch.get("AMOUNT") + " " + batch.get("ACTUALQTY") + " " + batch.get("BILLEDQTY"));

                                String overduevalue = String.valueOf(batch.get("ORDERDUEDATE"));
                                JSONObject overdue;
                                String p = null;
                                if (overduevalue.equalsIgnoreCase("")) {
                                    p = null;
                                } else if (overduevalue.charAt(0) == '{') {
                                    overdue = batch.getJSONObject("ORDERDUEDATE");
                                    p = String.valueOf(overdue.get("P"));
                                } else {
                                    p = null;
                                }

                                gowdownName = String.valueOf(batch.get("GODOWNNAME"));
                                batchName = String.valueOf(batch.get("BATCHNAME"));

                                basicPurchaseOrderNumber = String.valueOf(batch.get("ORDERNO"));
                                String batchamount1 = String.valueOf(batch.get("AMOUNT"));
                                String batchactualQty1 = String.valueOf(batch.get("ACTUALQTY"));
                                //System.out.println(gowdownName + " " + batchName + " " + basicPurchaseOrderNumber + " " + batchamount1 + " " + batchactualQty1);

//                    ////System.out.println(overdue.get("P"));
                                String batchbillqty = String.valueOf(batch.get("BILLEDQTY"));
                                batchAllocationList.add(new BatchAllocation(gowdownName, batchName, String.valueOf(basicPurchaseOrderNumber), String.valueOf(batchamount1), String.valueOf(batchactualQty1), String.valueOf(batchbillqty), p));
                                stockName = String.valueOf(inventory.get("STOCKITEMNAME"));
                                rate = String.valueOf(inventory.get("RATE"));
                                String stockAmount = String.valueOf(inventory.get("AMOUNT"));
                                String stockBillQty = String.valueOf(inventory.get("BILLEDQTY"));
                                String stockActualQty = String.valueOf(inventory.get("ACTUALQTY"));
                                inventoriesList.add(new Inventories(stockName, "", rate, stockAmount, stockBillQty, stockActualQty, batchAllocationList));
                            }


                            // DashType type = DashType.valueOf(voucher.getString("VOUCHERTYPENAME"));
                            String invenString = String.valueOf(voucher.get("INVOICEORDERLIST.LIST"));
                            if (invenString.equalsIgnoreCase("")) {

                            } else if (invenString.charAt(0) == '{') {
                                JSONObject invoicebill = voucher.getJSONObject("INVOICEORDERLIST.LIST");
                                String invoicebrt = String.valueOf(invoicebill.get("BASICORDERDATE"));
                                orderType = String.valueOf(invoicebill.get("ORDERTYPE"));
                                String bsrd = String.valueOf(invoicebill.get("BASICPURCHASEORDERNO"));
                                //System.out.println("invoicebill" + invoicebrt + " " + orderType + " " + bsrd);
                            } else {

                            }


                        } else if (var.charAt(0) == '[') {

                            JSONArray inventory = voucher.getJSONArray("ALLINVENTORYENTRIES.LIST");
                            List<BatchAllocation> batchAllocationList = new ArrayList<>();
                            for(int l=0;l<inventory.length();l++)
                            {
                                JSONObject tr=inventory.getJSONObject(l);
                                String stockAmount="" ;
                                String stockBillQty ="";
                                String batchamount1 ="";
                                String batchactualQty1="" ;
                                String batchbillqty ="";
                                String stockActualQty="";
                                String p = null;
                                // JSONObject bsc = inventory.getJSONObject("BASICUSERDESCRIPTION.LIST");
//                        //System.out.println(bsc.get("BASICUSERDESCRIPTION") + " " + inventory.get("STOCKITEMNAME") + " " + inventory.get("RATE") + " " + inventory.get("AMOUNT") + " " + inventory.get("BILLEDQTY") + " " + inventory.get("ACTUALQTY"));

                                //  basicUserDesc = String.valueOf(bsc.get("BASICUSERDESCRIPTION"));

                                if (String.valueOf(tr.get("BATCHALLOCATIONS.LIST")).equalsIgnoreCase("")) {

                                } else if (String.valueOf(tr.get("BATCHALLOCATIONS.LIST")).charAt(0) == '{') {
                                    JSONObject batch = tr.getJSONObject("BATCHALLOCATIONS.LIST");
                                    //  //System.out.println(batch.getString("GODOWNNAME") + " " + batch.get("BATCHNAME") + batch.get("ORDERNO") + batch.get("AMOUNT") + " " + batch.get("ACTUALQTY") + " " + batch.get("BILLEDQTY"));

                                    String overduevalue = String.valueOf(batch.get("ORDERDUEDATE"));
                                    JSONObject overdue;

                                    if (overduevalue.equalsIgnoreCase("")) {
                                        p = null;
                                    } else if (overduevalue.charAt(0) == '{') {
                                        overdue = batch.getJSONObject("ORDERDUEDATE");
                                        p = String.valueOf(overdue.get("P"));
                                    } else {
                                        p = null;
                                    }

                                    gowdownName = String.valueOf(batch.get("GODOWNNAME"));
                                    batchName = String.valueOf(batch.get("BATCHNAME"));

                                    basicPurchaseOrderNumber = String.valueOf(batch.get("ORDERNO"));
                                     batchamount1 = String.valueOf(batch.get("AMOUNT"));
                                     batchactualQty1 = String.valueOf(batch.get("ACTUALQTY"));
                                    //System.out.println(gowdownName + " " + batchName + " " + basicPurchaseOrderNumber + " " + batchamount1 + " " + batchactualQty1);

//                    ////System.out.println(overdue.get("P"));
                                     batchbillqty = String.valueOf(batch.get("BILLEDQTY"));
                                    batchAllocationList.add(new BatchAllocation(gowdownName, batchName, String.valueOf(basicPurchaseOrderNumber), String.valueOf(batchamount1), String.valueOf(batchactualQty1), String.valueOf(batchbillqty), p));
                                    stockName = String.valueOf(tr.get("STOCKITEMNAME"));
                                    rate = String.valueOf(tr.get("RATE"));
                                    stockAmount = String.valueOf(tr.get("AMOUNT"));
                                    stockBillQty = String.valueOf(tr.get("BILLEDQTY"));
                                    stockActualQty = String.valueOf(tr.get("ACTUALQTY"));

                                }
                                inventoriesList.add(new Inventories(stockName, "", rate, stockAmount, stockBillQty, stockActualQty, batchAllocationList));

                                // DashType type = DashType.valueOf(voucher.getString("VOUCHERTYPENAME"));
                                String invenString = String.valueOf(voucher.get("INVOICEORDERLIST.LIST"));
                                if (invenString.equalsIgnoreCase("")) {

                                } else if (invenString.charAt(0) == '{') {
                                    JSONObject invoicebill = voucher.getJSONObject("INVOICEORDERLIST.LIST");
                                    String invoicebrt = String.valueOf(invoicebill.get("BASICORDERDATE"));
                                    orderType = String.valueOf(invoicebill.get("ORDERTYPE"));
                                    String bsrd = String.valueOf(invoicebill.get("BASICPURCHASEORDERNO"));
                                    //System.out.println("invoicebill" + invoicebrt + " " + orderType + " " + bsrd);
                                } else {

                                }
                            }



                        }
                        if (String.valueOf(voucher.get("LEDGERENTRIES.LIST")).equalsIgnoreCase("")) {

                        } else if (String.valueOf(voucher.get("LEDGERENTRIES.LIST")).charAt(0) == '{') {


                            items = new ArrayList<>();

                            JSONObject bill = voucher.getJSONObject("LEDGERENTRIES.LIST");
                            ////System.out.println(bill);
                            inventories = new ArrayList<>();
                            amount = String.valueOf(bill.get("AMOUNT"));
                            ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                            //  //System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");
                            if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                                String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST"));
                                ////System.out.println("the String is Object:"+op);
                                if (!op.equals(null) && !op.isEmpty())
                                    if (op.equalsIgnoreCase("")) {

                                    } else if (op.charAt(0) == '{') {

                                        inventories = new ArrayList<>();
                                        // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                        ////System.out.println("the String is Object:"+op);
                                        JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                        // //System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                        ////System.out.println("ok tasted 1");
                                        billAmount = String.valueOf(billl.get("AMOUNT"));
                                        billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                                        billType = String.valueOf(billl.get("BILLTYPE"));
                                        billName = String.valueOf(billl.get("NAME"));
                                        //System.out.println(billAmount + " " + bill + " " + billType + " " + billName);
                                        inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                    } else if (op.charAt(0) == '[') {
                                        //System.out.println("ok tasted 2");
                                        // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                        inventories = new ArrayList<>();
                                        ////System.out.println("the String is Array:"+op);
                                        String reString = bill.toString();
                                        ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                        JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                        ////System.out.println("ok tasted 3");
                                        for (int b = 0; b < billlist.length(); b++) {
                                            JSONObject billItems = billlist.getJSONObject(b);
                                            billAmount = String.valueOf(billItems.get("AMOUNT"));
                                            billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                                            billType = String.valueOf(billItems.getString("BILLTYPE"));
                                            billName = String.valueOf(billItems.get("NAME"));

                                            inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                            //System.out.println("bill" + " " + billName + " " + billType + " " + bill + " " + billCreditPeriod + " ");
                                        }
                                        ////System.out.println("ok tasted 4");
                                        //e.printStackTrace();

                                    } else {
                                        items = new ArrayList<>();
                                        //System.out.println("ok null");
                                    }
                            }
                            if (String.valueOf(bill).contains("BANKALLOCATIONS.LIST")) {
                                String ba = String.valueOf(bill.getString("BANKALLOCATIONS.LIST"));
                                ////System.out.println("the String is Object:"+op);
                                if (!ba.equals(null) && !ba.isEmpty())
                                    if (ba.equalsIgnoreCase("")) {

                                    } else if (ba.charAt(0) == '{') {
                                        // ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                        //\//System.out.println("the String is Object:"+op);
                                        JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                        ////System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                        ////System.out.println("ok tasted 1");

                                    } else if (ba.charAt(0) == '[') {
                                        System.out.print("ok tasted 2");
                                        // ba = bill.getString("BANKALLOCATIONS.LIST").toString();

                                        ////System.out.println("the String is Array:"+op);
                                        String reString = bill.toString();
                                        ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                        JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                        ////System.out.println("ok tasted 3");
                                        for (int b = 0; b < billlist.length(); b++) {
                                            JSONObject billItems = billlist.getJSONObject(b);
                                            // //System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                                        }
                                        ////System.out.println("ok tasted 4");
                                        //e.printStackTrace();

                                    } else {
                                        //System.out.println("ok null");
                                    }

                            }
                            items.add(new LedgerItem(amount, ledgerName, inventories, null));


                        } else if (String.valueOf(voucher.get("LEDGERENTRIES.LIST")).charAt(0) == '[') {
                            JSONArray list = voucher.getJSONArray("LEDGERENTRIES.LIST");

                            items = new ArrayList<>();
                            //System.out.println(list.length());
                            for (int j = 0; j < list.length(); j++) {
                                JSONObject bill = list.getJSONObject(j);
                                ////System.out.println(bill);
                                inventories = new ArrayList<>();
                                amount = String.valueOf(bill.get("AMOUNT"));
                                ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                                //  //System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");
                                if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                                    String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST"));
                                    ////System.out.println("the String is Object:"+op);
                                    if (!op.equals(null) && !op.isEmpty())
                                        if (op.equalsIgnoreCase("")) {

                                        } else if (op.charAt(0) == '{') {

                                            inventories = new ArrayList<>();
                                            // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                            ////System.out.println("the String is Object:"+op);
                                            JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                            // //System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                            ////System.out.println("ok tasted 1");
                                            billAmount = String.valueOf(billl.get("AMOUNT"));
                                            billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                                            billType = String.valueOf(billl.get("BILLTYPE"));
                                            billName = String.valueOf(billl.get("NAME"));
                                            //System.out.println(billAmount + " " + bill + " " + billType + " " + billName);
                                            inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                        } else if (op.charAt(0) == '[') {
                                            //System.out.println("ok tasted 2");
                                            // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                            inventories = new ArrayList<>();
                                            ////System.out.println("the String is Array:"+op);
                                            String reString = bill.toString();
                                            ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                            JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                            ////System.out.println("ok tasted 3");
                                            for (int b = 0; b < billlist.length(); b++) {
                                                JSONObject billItems = billlist.getJSONObject(b);
                                                billAmount = String.valueOf(billItems.get("AMOUNT"));
                                                billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                                                billType = String.valueOf(billItems.getString("BILLTYPE"));
                                                billName = String.valueOf(billItems.get("NAME"));

                                                inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                                //System.out.println("bill" + " " + billName + " " + billType + " " + bill + " " + billCreditPeriod + " ");
                                            }
                                            ////System.out.println("ok tasted 4");
                                            //e.printStackTrace();

                                        } else {
                                            items = new ArrayList<>();
                                            //System.out.println("ok null");
                                        }
                                }
                                if (String.valueOf(bill).contains("BANKALLOCATIONS.LIST")) {
                                    String ba = String.valueOf(bill.getString("BANKALLOCATIONS.LIST"));
                                    ////System.out.println("the String is Object:"+op);
                                    if (!ba.equals(null) && !ba.isEmpty())
                                        if (ba.equalsIgnoreCase("")) {

                                        } else if (ba.charAt(0) == '{') {
                                            // ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                            //\//System.out.println("the String is Object:"+op);
                                            JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                            ////System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                            ////System.out.println("ok tasted 1");

                                        } else if (ba.charAt(0) == '[') {
                                            System.out.print("ok tasted 2");
                                            //ba = bill.getString("BANKALLOCATIONS.LIST").toString();

                                            ////System.out.println("the String is Array:"+op);
                                            String reString = bill.toString();
                                            ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                            JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                            ////System.out.println("ok tasted 3");
                                            for (int b = 0; b < billlist.length(); b++) {
                                                JSONObject billItems = billlist.getJSONObject(b);
                                                // //System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                                            }
                                            ////System.out.println("ok tasted 4");
                                            //e.printStackTrace();

                                        } else {
                                            //System.out.println("ok null");
                                        }

                                }
                                items.add(new LedgerItem(amount, ledgerName, inventories, null));
                            }


                        }


                        salesVouchers.add(new SalesVoucher(voucherDate, voucherPartyName, voucherTypeName, "Purchase", voucherNumber, voucherNarration, inventoriesList, null, items));
                    }

                }

            }


        } catch (Exception e) {
            e.getMessage();
        }
        if (salesVouchers.size() == 0) {
            salesVouchers = new ArrayList<>();
        }
        return salesVouchers;
    }
    List<LedgerItem> getAllLedgerItem(JSONObject voucher) {
        String voucherDate;
        String voucherPartyName;
        String voucherNumber;
        String voucherNarration;
        String voucherTypeName;
        String basicUserDesc;
        Double rate;
        String amount;
        int edqty;
        int actualQty;

        String stockName;
        String gowdownName;
        String batchName;
        Double batchamount;
        int batchactualQty;
        int billQty;
        Date orderDueDate;
        List<LedgerItem> items = null;
        List<Item> inventories = null;
        Double basicOrderRate;
        String orderType;
        String basicPurchaseOrderNumber;
        Double orderamount;
        String ledgerName;
        String billName;
        String billAmount;
        String billType;
        String billCreditPeriod;
        List<SalesVoucher> salesVouchers = new ArrayList<>();
        List<InvoiceOrderList> invoiceOrderLists = null;
        List<Inventories> inventoriesList = null;
        ArrayList<LedgerItem> listItes = new ArrayList<>();

        if (String.valueOf(voucher).contains("ALLLEDGERENTRIES.LIST")) {
            if (String.valueOf(voucher.get("ALLLEDGERENTRIES.LIST")).equalsIgnoreCase("")) {

            } else if (String.valueOf(voucher.get("ALLLEDGERENTRIES.LIST")).charAt(0) == '{') {
                items = new ArrayList<>();

                JSONObject bill = voucher.getJSONObject("ALLLEDGERENTRIES.LIST");
                ////System.out.println(bill);
                inventories = new ArrayList<>();
                amount = String.valueOf(bill.get("AMOUNT"));
                ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                // //System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");
                if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                    String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST"));
                    ////System.out.println("the String is Object:"+op);
                    if (!op.equals(null) && !op.isEmpty())
                        if (op.equalsIgnoreCase("")) {

                        } else if (op.charAt(0) == '{') {

                            inventories = new ArrayList<>();
                            // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                            ////System.out.println("the String is Object:"+op);
                            JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                            // //System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                            ////System.out.println("ok tasted 1");
                            billAmount = String.valueOf(billl.get("AMOUNT"));
                            billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                            billType = String.valueOf(billl.get("BILLTYPE"));
                            billName = String.valueOf(billl.get("NAME"));
                            //System.out.println(billAmount + " " + bill + " " + billType + " " + billName);
                            inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                        } else if (op.charAt(0) == '[') {
                            //System.out.println("ok tasted 2");
                            // op=bill.get("BILLALLOCATIONS.LIST").toString();
                            inventories = new ArrayList<>();
                            ////System.out.println("the String is Array:"+op);
                            String reString = bill.toString();
                            ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                            JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                            ////System.out.println("ok tasted 3");
                            for (int b = 0; b < billlist.length(); b++) {
                                JSONObject billItems = billlist.getJSONObject(b);
                                billAmount = String.valueOf(billItems.get("AMOUNT"));
                                billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                                billType = String.valueOf(billItems.getString("BILLTYPE"));
                                billName = String.valueOf(billItems.get("NAME"));

                                inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                //System.out.println("bill" + " " + billName + " " + billType + " " + bill + " " + billCreditPeriod + " ");
                            }
                            ////System.out.println("ok tasted 4");
                            //e.printStackTrace();

                        } else {
                            items = new ArrayList<>();
                            //System.out.println("ok null");
                        }
                }
                if (String.valueOf(bill).contains("BANKALLOCATIONS.LIST")) {
                    String ba = String.valueOf(bill.getString("BANKALLOCATIONS.LIST"));
                    ////System.out.println("the String is Object:"+op);
                    if (!ba.equals(null) && !ba.isEmpty())
                        if (ba.equalsIgnoreCase("")) {

                        } else if (ba.charAt(0) == '{') {
                            // ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                            //\//System.out.println("the String is Object:"+op);
                            JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                            ////System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                            ////System.out.println("ok tasted 1");

                        } else if (ba.charAt(0) == '[') {
                            System.out.print("ok tasted 2");
                            //ba = bill.getString("BANKALLOCATIONS.LIST").toString();

                            ////System.out.println("the String is Array:"+op);
                            String reString = bill.toString();
                            ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                            JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                            ////System.out.println("ok tasted 3");
                            for (int b = 0; b < billlist.length(); b++) {
                                JSONObject billItems = billlist.getJSONObject(b);
                                // //System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                            }
                            ////System.out.println("ok tasted 4");
                            //e.printStackTrace();

                        } else {
                            //System.out.println("ok null");
                        }
                    listItes.add(new LedgerItem(amount, ledgerName, inventories));
                }


            } else if (String.valueOf(voucher.get("ALLLEDGERENTRIES.LIST")).charAt(0) == '[') {
                JSONArray list = voucher.getJSONArray("ALLLEDGERENTRIES.LIST");
                items = new ArrayList<>();
                for (int j = 0; j < list.length(); j++) {
                    JSONObject bill = list.getJSONObject(j);
                    //System.out.println(String.valueOf(list.get(j)));
                    ////System.out.println(bill);
                    inventories = new ArrayList<>();
                    amount = String.valueOf(bill.get("AMOUNT"));
                    ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                    // //System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");
                    if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                        String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST"));
                        ////System.out.println("the String is Object:"+op);
                        if (!op.equals(null) && !op.isEmpty())
                            if (op.equalsIgnoreCase("")) {

                            } else if (op.charAt(0) == '{') {

                                inventories = new ArrayList<>();
                                // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                ////System.out.println("the String is Object:"+op);
                                JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                // //System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                ////System.out.println("ok tasted 1");
                                billAmount = String.valueOf(billl.get("AMOUNT"));
                                billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                                billType = String.valueOf(billl.get("BILLTYPE"));
                                billName = String.valueOf(billl.get("NAME"));
                                //System.out.println(billAmount + " " + bill + " " + billType + " " + billName);
                                inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                            } else if (op.charAt(0) == '[') {
                                //System.out.println("ok tasted 2");
                                // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                inventories = new ArrayList<>();
                                ////System.out.println("the String is Array:"+op);
                                // String reString = bill.toString();
                                ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                ////System.out.println("ok tasted 3");
                                for (int b = 0; b < billlist.length(); b++) {
                                    JSONObject billItems = billlist.getJSONObject(b);
                                    billAmount = String.valueOf(billItems.get("AMOUNT"));
                                    billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                                    billType = String.valueOf(billItems.getString("BILLTYPE"));
                                    billName = String.valueOf(billItems.get("NAME"));

                                    inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                    //System.out.println("bill" + " " + billName + " " + billType + " " + bill + " " + billCreditPeriod + " ");
                                }
                                ////System.out.println("ok tasted 4");
                                //e.printStackTrace();

                            } else {
                                items = new ArrayList<>();
                                //System.out.println("ok null");
                            }
                    }
                    if (String.valueOf(bill).contains("BANKALLOCATIONS.LIST")) {
                        String ba = String.valueOf(bill.getString("BANKALLOCATIONS.LIST"));
                        ////System.out.println("the String is Object:"+op);
                        if (!ba.equals(null) && !ba.isEmpty())
                            if (ba.equalsIgnoreCase("")) {

                            } else if (ba.charAt(0) == '{') {
                                //ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                //\//System.out.println("the String is Object:"+op);
                                JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                ////System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                ////System.out.println("ok tasted 1");

                            } else if (ba.charAt(0) == '[') {
                                System.out.print("ok tasted 2");
                                //ba = bill.getString("BANKALLOCATIONS.LIST").toString();

                                ////System.out.println("the String is Array:"+op);
                                //String reString = bill.toString();
                                ////System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                ////System.out.println("ok tasted 3");
                                for (int b = 0; b < billlist.length(); b++) {
                                    JSONObject billItems = billlist.getJSONObject(b);
                                    // //System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                                }
                                ////System.out.println("ok tasted 4");
                                //e.printStackTrace();

                            } else {
                                //System.out.println("ok null");
                            }

                    }
                    listItes.add(new LedgerItem(amount, ledgerName, inventories));
                }
            }


        }
        return listItes;
    }
}