package com.java.tallyzee.Network;

import com.java.tallyzee.Models.Company;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import java.util.ArrayList;

public class CompanyParser{
     public static final String  urlParameters= "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">01/01/2017</SVFROMDATE>   <SVTODATE TYPE=\"Date\">01/01/2017</SVTODATE>                 </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsSales:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>";
    String userName,companyName;


    public static String test(String responseXml) {
    JSONObject obj = null;
    try {
        obj = XML.toJSONObject(responseXml);
        System.out.println("json" + obj.toString());
        //printJson(obj.toString());
    } catch (Exception e) {
        System.out.println(e);
    }
    return obj.toString();
}


    public static String getListOFBill(String json)
    {
        JSONObject main = new JSONObject(json);
        JSONObject envlop = main.getJSONObject("ENVELOPE");
        JSONObject body = envlop.getJSONObject("BODY");
        JSONObject desc = body.getJSONObject("DESC");
        JSONObject stavar = desc.getJSONObject("STATICVARIABLES");
        String companyName = String.valueOf(stavar.get("SVCURRENTCOMPANY"));

        return  companyName;
    }

    static Company getCompanyObject(String json,String cName,String userName,String gst) {
        int id=0;
        String companyDate=null;
        String name=null;
        String address=null;
        String stateName=null;
        String userId=userName;
        String cuntryName=null;
        String phoneNumber=null;
        String mobileNumber=null;
        String email=null;
        String cin=null;
        String pan=null;
        String gstn=null;
        gstn=gst;
        userId=userName;
       // ArrayList<GstRates> list = new ArrayList();
        System.out.println(json);
        if (json.contains("ENVELOPE")) {
            JSONObject main = new JSONObject(json);
            JSONObject envlop = main.getJSONObject("ENVELOPE");
            JSONObject body = envlop.getJSONObject("BODY");
            JSONObject data = body.getJSONObject("DATA");
            JSONObject tally = data.getJSONObject("TALLYMESSAGE");
            if (String.valueOf(tally).contains("COMPANY")) {
                JSONObject tallymsg=tally.getJSONObject("COMPANY");
                JSONObject cinObj=null;
                if(String.valueOf(tallymsg).contains("UDF:CORPORATEIDENTITYNO"))
                {
                    cinObj = tallymsg.getJSONObject("UDF:CORPORATEIDENTITYNO");
                    cin=String.valueOf(cinObj.get("content"));
                }
                JSONObject PHONENUMBER=null;
if(String.valueOf(tallymsg).contains("PHONENUMBER"))
{
    PHONENUMBER=tallymsg.getJSONObject("PHONENUMBER");
    if(String.valueOf(PHONENUMBER).contains("content"))
    phoneNumber=String.valueOf(PHONENUMBER.get("content"));
}

               if(String.valueOf(tallymsg).contains("EMAIL"))
               {
                   JSONObject EMAIL=tallymsg.getJSONObject("EMAIL");
                   if(String.valueOf(EMAIL).contains("content"))
                   email= String.valueOf(EMAIL.get("content"));
               }
               if(String.valueOf(tallymsg).contains("INCOMETAXNUMBER"))
                {
                    JSONObject panObj=tallymsg.getJSONObject("INCOMETAXNUMBER");
                    if(String.valueOf(panObj).contains("content"))
                    {
                        pan= String.valueOf(panObj.get("content"));
                    }
                }

                if(String.valueOf(tallymsg).contains("MOBILENUMBERS.LIST"))
                {
                    JSONObject Name=tallymsg.getJSONObject("MOBILENUMBERS.LIST");
                    if(String.valueOf(Name).contains("MOBILENUMBERS"))
                    {
                        mobileNumber= String.valueOf(Name.get("MOBILENUMBERS"));
                    }

                }
                name=cName;
                if(String.valueOf(tallymsg).contains("STATENAME"))
                {
                    JSONObject STATENAME=tallymsg.getJSONObject("STATENAME");
                    if(String.valueOf(STATENAME).contains("content"))
                    {
                        stateName= String.valueOf(STATENAME.get("content"));
                    }
                }
                if(String.valueOf(tallymsg).contains("ADDRESS.LIST"))
                {
                    JSONObject ADDRESSLIST=tallymsg.getJSONObject("ADDRESS.LIST");
                    if(String.valueOf(ADDRESSLIST).charAt(0)=='{')
                    {
                        String st=String.valueOf(ADDRESSLIST.get("ADDRESS"));
                        if(st.equalsIgnoreCase("")) {

                        }
                        else if(st.charAt(0)=='{')
                        {
                            JSONObject adrObj=ADDRESSLIST.getJSONObject("ADDRESS");
                            if(String.valueOf(adrObj).contains("content"))
                            {
                                address=String.valueOf(adrObj.get("content"));
                            }
                        }else if(st.charAt(0)=='[')
                        {
                            JSONArray  ADDRESS=ADDRESSLIST.getJSONArray("ADDRESS");
                            for(int i=0;i<ADDRESS.length();i++)
                            {
                                address+=String.valueOf(ADDRESS.get(i));
                            }
                        }else
                        {

                        }

                    }
                    else if(String.valueOf(ADDRESSLIST).charAt(0)=='[')
                    {

                    }else
                    {
                        address=null;
                    }

                }

                if(String.valueOf(tallymsg).contains("COUNTRYNAME"))
                {
                    JSONObject country=tallymsg.getJSONObject("COUNTRYNAME");
                    if(String.valueOf(country).contains("content")) {
                        cuntryName = String.valueOf(country.get("content"));
                    }
                }

            }
        }
        return new Company(0,companyDate,name,address,stateName,userId,cuntryName,phoneNumber,mobileNumber,email,cin,pan,gstn, null, null, null, null, null, null, null,null,null,null);
    }
    static String getGstnObject(String json,String cName) {
        int id=0;

        String gstn=null;
        // ArrayList<GstRates> list = new ArrayList();
        System.out.println(json);
        if (json.contains("ENVELOPE")) {
            JSONObject main = new JSONObject(json);
            JSONObject envlop = main.getJSONObject("ENVELOPE");
            JSONObject body = envlop.getJSONObject("BODY");
            JSONObject data = body.getJSONObject("DATA");
            JSONObject tallymsg = data.getJSONObject("COLLECTION");
            if (String.valueOf(tallymsg).contains("TAXUNIT")) {
                JSONObject TAXUNIT=tallymsg.getJSONObject("TAXUNIT");
                JSONObject GSTREGNUMBER=null;
                if(String.valueOf(TAXUNIT).contains("GSTREGNUMBER"))
                {
                   GSTREGNUMBER=TAXUNIT.getJSONObject("GSTREGNUMBER");
                }
               if(String.valueOf(GSTREGNUMBER).contains("content"))
                gstn= String.valueOf(GSTREGNUMBER.get("content"));

            }
        }
        return gstn;
    }
}
