package com.java.tallyzee.Network;

import com.java.tallyzee.Models.Company;
import com.java.tallyzee.Models.SalesVoucher;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.ArrayList;

public class JsonConverterClass {


    public static String convertGsonSales(ArrayList<Company> mylist)
    {
        ObjectMapper mapper = new ObjectMapper();
        String payload="";
        /**
         * Write object to file
         */
        try {

            payload=mapper.writeValueAsString(mylist);
            System.out.println(payload+" "+mylist);
            return payload;
            //mapper.writerWithDefaultPrettyPrinter().writeValue(new File("result.json"), carFleet);//Prettified JSON
        } catch (Exception e) {
            e.printStackTrace();
        }

//        String payload=new Gson().toJson(mylist);
//        System.out.println("before"+payload);
        return payload;

    }
    public static String convertGsonCompany(Company mylist)
    {
        ObjectMapper mapper = new ObjectMapper();
        String payload="";
        /**
         * Write object to file
         */
        try {

            payload=mapper.writeValueAsString(mylist);
            System.out.println(payload+" "+mylist);
            return payload;
            //mapper.writerWithDefaultPrettyPrinter().writeValue(new File("result.json"), carFleet);//Prettified JSON
        } catch (Exception e) {
            e.printStackTrace();
        }

//        String payload=new Gson().toJson(mylist);
//        System.out.println("before"+payload);
        return payload;

    }

}
