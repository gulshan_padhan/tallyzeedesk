package com.java.tallyzee.Network;

import com.java.tallyzee.Utility.AppParserUtil;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public  class NetworkServices {

    public static String getResponse(String test){

       String urlParameters="<ENVELOPE><HEADER><VERSION>1</VERSION><TALLYREQUEST>Export</TALLYREQUEST><TYPE>Data</TYPE><ID>BillsPayable</ID></HEADER><BODY><DESC><STATICVARIABLES><EXPLODEFLAG>Yes</EXPLODEFLAG><SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVCURRENTCOMPANY>##SVCurrentCompany</SVCURRENTCOMPANY></STATICVARIABLES></DESC></BODY></ENVELOPE>";
//        String urlt="<ENVELOPE><HEADER><VERSION>1</VERSION><TALLYREQUEST>Export</TALLYREQUEST><TYPE>Data</TYPE><ID>Ledger OutStandings</ID></HEADER><BODY><DESC><STATICVARIABLES><SVCURRENTCOMPANY>AIM INFOCOM SERVICES PVT.LTD (15-16)</SVCURRENTCOMPANY></STATICVARIABLES></DESC><DATA><PARTYLEDGERNAME>100 Sign & Display</PARTYLEDGERNAME></DATA></BODY></ENVELOPE>";
//        String lastUrl="<ENVELOPE><HEADER><VERSION>1</VERSION><TALLYREQUEST>Export</TALLYREQUEST><TYPE>Collection</TYPE><ID>Collection of Ledgers</ID></HEADER><BODY><DESC><STATICVARIABLES><SVCurrentCompany>AIM INFOCOM SERVICES PVT.LTD (15-16)</SVCurrentCompany></STATICVARIABLES><TDL><TDLMESSAGE><COLLECTION NAME='Collection of Ledgers' ISMODIFY='No'><TYPE>Ledger</TYPE></COLLECTION></TDLMESSAGE></TDL></DESC></BODY></ENVELOPE>";
//        String testUrls="<ENVELOPE><HEADER><VERSION> 1 </VERSION><TALLYREQUEST> Export </TALLYREQUEST><TYPE> DATA </TYPE><ID>Voucher Register</ID></HEADER> <BODY> <DESC> <STATICVARIABLES><SVCurrentCompany>##SVCURRENTCOMPANY</SVCurrentCompany><SVVoucherNumber></SVVoucherNumber><SVEXPORTFORMAT>$$SysName: XML </SVEXPORTFORMAT><SVFROMDATE TYPE='Date'>20170401</SVFROMDATE><SVTODATE TYPE='Date'>20170401</SVTODATE></STATICVARIABLES> <EXPLODEFLAG>Yes</EXPLODEFLAG></DESC></BODY></ENVELOPE>";
        StringBuffer response = null;
        System.out.println(test
        );
        try {
            String url = "http://"+ AppParserUtil.ipAdress +":9000";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type",
                    "application/xml;charset=utf-8");
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(test);
            wr.flush();
            wr.close();
            String responseStatus = con.getResponseMessage();
           // System.out.println("error" + responseStatus);
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (IOException e) {
            System.out.println("error" + e.getMessage());
        }
        //System.out.println(response);
        //XMLToJSON.test(response.toString());
        return response.toString();
    }

}