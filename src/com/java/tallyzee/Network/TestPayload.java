package com.java.tallyzee.Network;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import net.projectmonkey.object.mapper.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import com.java.tallyzee.Models.*;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestPayload extends Thread {
    String companyName;
    String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    String[] monthsCount = {"31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"};
    String[] daysList = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
    ArrayList<SalesVoucher> arrayList = new ArrayList<>();
    // static SalesParser n = new SalesParser();



    @Override
    public void run() {
        actionPerformed();
    }

    public String test(String responseXml) {
        JSONObject obj = null;
        try {
            obj = XML.toJSONObject(responseXml);
            System.out.println("json" + obj.toString());
            //printJson(obj.toString());
        } catch (Exception e) {
            System.out.println(e);
        }
        return obj.toString();
    }

    public String getPayRoll(String fromdate, String todate) {
        String payroll = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">" + fromdate + "</SVFROMDATE>   <SVTODATE TYPE=\"Date\">" + todate + "</SVTODATE>                 </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsSales:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";

        return payroll;
    }

    public void actionPerformed() {
        String test = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">20170404</SVFROMDATE>   <SVTODATE TYPE=\"Date\">20170404</SVTODATE>                 </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsSales:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";

        String name = "", email;
        try {
            FileInputStream serviceAccount = new FileInputStream(new File(getClass().getClassLoader().getResource("com/java/tallyzee/FirebaseUtils/admin-sdk.json").getFile()));
            System.out.println(serviceAccount.getChannel());
            // Initialize the app with a service account, granting admin privileges
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://tallyfy-364a0.firebaseio.com/")
                    .build();
            System.out.println(options.getDatabaseUrl());
            FirebaseApp.initializeApp(options);
        } catch (Exception enn) {
        }
        System.out.println("OUT OF EXCEPTION");


//        SalesVoucher object_sales = new SalesVoucher(address,buyerName,partyName,amount,voucherType,parentVoucherType,partyLedgerName
//                ,reference,basicBasePartyName);
//            System.out.println(object_sales.getAddress());
        ObjectMapper mapper = new ObjectMapper();
        // As an admin, the app has access to read and write all data, regardless of Security Rules
        DatabaseReference ref = FirebaseDatabase.getInstance()
                .getReferenceFromUrl("https://tallyfy-364a0.firebaseio.com");
        String fromDay, toDay;
        String year = "2017";
        int count = 0;
        ArrayList<String> vouchernumber = new ArrayList<>();
        ArrayList<String> vouchernumber1 = new ArrayList<>();
        for (int days = 0; days < monthsCount.length; days++) {
            for (int j = 0; j < Integer.valueOf(monthsCount[days]); j++) {

                fromDay = daysList[j] + "" + months[days] + "" + year;
                ArrayList<SalesVoucher> salesVouchers = new ArrayList<>();
                //System.out.println(getPayRoll(fromDay, fromDay));
                salesVouchers = (ArrayList<SalesVoucher>) printJson(test(NetworkServices.getResponse(getPayRoll(fromDay, fromDay))));
                // System.out.println("the" + count + "response size is:" + salesVouchers.size());
                if (salesVouchers.size() > 0) {
//
                    for (int i = 0; i < salesVouchers.size(); i++) {
                        vouchernumber1.add(salesVouchers.get(i).getVoucherNumber());
                        final int temp = i;
                        final SalesVoucher sales = salesVouchers.get(i);
                        ref.child("TallFy").child("Users").child("5").child("gulshanpadhan").child("Sales_Vouchers").child(String.valueOf(count)).setValue(salesVouchers.get(i), new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                vouchernumber.add(sales.getVoucherNumber());
                            }
                        });
                        count++;
                    }
                }
                try {
                    //  Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

        }
        vouchernumber.forEach((response) -> {
            System.out.println(response);
        });
        System.out.println(vouchernumber.size() + " " + vouchernumber1.size());
    }

    //    public Object InitializeControls()
//    {
//        SalesVoucher sales = new SalesVoucher();
//        String address = sales.getAddress();
//        String buyerName = sales.getBasicBuyerName();
//        String partyName = sales.getPartyName();
//        String amount = sales.getAmount();
//        String voucherType = sales.getVoucherType();
//        String parentVoucherType = sales.getParentVoucherType();
//        String partyLedgerName = sales.getPartyLedgerName();
//        String reference = sales.getReference();
//        String basicBasePartyName = sales.getBasicBasePartyName();
//         object_sales = new SalesVoucher(address,buyerName,partyName,amount,voucherType,parentVoucherType,partyLedgerName
//        ,reference,basicBasePartyName);
//
//        return object_sales;
//    }

static List<BillsRecevieables> getListOFBill(String json)
{
    ArrayList<BillsRecevieables> billsRecevieables=new ArrayList<>();
    JSONObject main = new JSONObject(json);
    JSONObject envlop = main.getJSONObject("ENVELOPE");
    JSONArray billoverdue=envlop.getJSONArray("BILLOVERDUE");
    JSONArray billdue=envlop.getJSONArray("BILLDUE");
    JSONArray billcl=envlop.getJSONArray("BILLCL");
    JSONArray billfixed=envlop.getJSONArray("BILLFIXED");
    for(int i=0;i<billfixed.length();i++)
    {
        JSONObject bfv=billfixed.getJSONObject(i);
        billsRecevieables.add(new BillsRecevieables(String.valueOf(bfv.get("BILLDATE")), String.valueOf(bfv.get("BILLREF")), String.valueOf(bfv.get("BILLPARTY")), String.valueOf(billcl.get(i)), String.valueOf(billdue.get(i)), String.valueOf(billoverdue.get(i))));

    }
    billsRecevieables.forEach((bill)->{
        System.out.println(bill.getBillCl()+"   "+bill.getBillDate()+"   "+bill.getBillDue()+"   "+bill.getBillOverDue()+"   "+bill.getBillParty()+"   "+bill.getBillRef());});
 System.out.println(billsRecevieables.size());
    return billsRecevieables;
}
    List<SalesVoucher> printJson(String json) {

        String voucherDate;
        String voucherPartyName;
        String voucherNumber;
        String voucherNarration;
        String voucherTypeName;
        String basicUserDesc;
        String rate;
        String amount;
        int edqty;
        int actualQty;

        String stockName;
        String gowdownName;
        String batchName;
        Double batchamount;
        int batchactualQty;
        int billQty;
        Date orderDueDate;
        List<LedgerItem> items = null;
        List<Item> inventories = null;
        Double basicOrderRate;
        String orderType;
        String basicPurchaseOrderNumber;
        Double orderamount;
        String ledgerName;
        String billName;
        String billAmount;
        String billType;
        String billCreditPeriod;
        List<SalesVoucher> salesVouchers = new ArrayList<>();
        List<InvoiceOrderList> invoiceOrderLists = null;
        List<Inventories> inventoriesList = new ArrayList<>();
        ArrayList<LedgerItem> listItes = new ArrayList<>();
        try {
            JSONObject main = new JSONObject(json);
            JSONObject envlop = main.getJSONObject("ENVELOPE");

            JSONObject body = envlop.getJSONObject("BODY");
            JSONObject desc = body.getJSONObject("DESC");
            JSONObject stavar = desc.getJSONObject("STATICVARIABLES");
            // System.out.println(stavar.get("SVCURRENTCOMPANY"));
            companyName = String.valueOf(stavar.get("SVCURRENTCOMPANY"));
            JSONObject data = body.getJSONObject("DATA");
            salesVouchers = new ArrayList<>();
            if (!data.equals(null) && data.isNull("DATA")) {
                if(String.valueOf(data.get("TALLYMESSAGE")).equalsIgnoreCase(""))
                {

                }
                if(String.valueOf(data.get("TALLYMESSAGE")).charAt(0)=='[')
                {
                    JSONArray tallymsg = data.getJSONArray("TALLYMESSAGE");
                    //System.out.println(tallymsg);
                    System.out.println(tallymsg.length());
                    for (int i = 0; i < tallymsg.length(); i++) {
                        invoiceOrderLists = new ArrayList<>();
                        JSONObject tamsg = tallymsg.getJSONObject(i);
                        //System.out.println(tamsg);
                        JSONObject voucher = tamsg.getJSONObject("VOUCHER");
                        inventoriesList = new ArrayList<>();
                        //  System.out.print("Voucher" + " " + voucher.get("DATE") + " " + voucher.getString("PARTYLEDGERNAME") + " " + voucher.getString("VOUCHERTYPENAME") + " " + voucher.getString("VOUCHERNUMBER"));
                        //  System.out.print("Voucher" + " " + voucher.get("DATE") + " " + voucher.getString("PARTYLEDGERNAME") + " " + voucher.getString("VOUCHERTYPENAME") + " " + voucher.getString("VOUCHERNUMBER"));
                        voucherDate = String.valueOf(voucher.get("DATE"));
                        voucherPartyName = String.valueOf(voucher.get("PARTYLEDGERNAME"));
                        voucherNumber = String.valueOf(voucher.get("VOUCHERNUMBER"));
                        voucherTypeName = String.valueOf(voucher.get("VOUCHERTYPENAME"));
                        voucherNarration = String.valueOf(voucher.get("NARRATION"));
                        System.out.println(voucherPartyName + " " + voucherDate + " " + voucherNumber + " " + voucherTypeName + " " + voucherNarration);
                        //voucherType v;
                        //System.out.print(" " + voucher.get("NARRATION"));

                        if(String.valueOf(tamsg.get("VOUCHER")).contains("ALLLEDGERENTRIES.LIST"))
                        {
                            String var= String.valueOf(voucher.get("ALLLEDGERENTRIES.LIST"));
                            if(var.equalsIgnoreCase(""))
                            {

                            }else if (var.charAt(0)=='{') {

                                JSONObject bill = voucher.getJSONObject("ALLLEDGERENTRIES.LIST");
                                //System.out.println(String.valueOf(list.get(j)));
                                //System.out.println(bill);
                                inventories = new ArrayList<>();
                                amount = String.valueOf(bill.get("AMOUNT"));
                                ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                                // System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");

                                //System.out.println("the String is Object:"+op);
                                if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                                    String op = bill.get("BILLALLOCATIONS.LIST").toString();
                                    if (!op.equals(null) && !op.isEmpty())
                                        if (op.charAt(0) == '{') {

                                            inventories = new ArrayList<>();
                                            // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                            //System.out.println("the String is Object:"+op);
                                            JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                            // System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                            //System.out.println("ok tasted 1");
                                            billAmount = String.valueOf(billl.get("AMOUNT"));
                                            billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                                            billType = String.valueOf(billl.get("BILLTYPE"));
                                            billName = String.valueOf(billl.get("NAME"));
                                            System.out.println(billAmount + " " + bill + " " + billType + " " + billName);
                                            inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                        } else if (op.charAt(0) == '[') {
                                            System.out.println("ok tasted 2");
                                            // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                            inventories = new ArrayList<>();
                                            //System.out.println("the String is Array:"+op);
                                            String reString = bill.toString();
                                            //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                            JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                            //System.out.println("ok tasted 3");
                                            for (int b = 0; b < billlist.length(); b++) {
                                                JSONObject billItems = billlist.getJSONObject(b);
                                                billAmount = String.valueOf(billItems.get("AMOUNT"));
                                                billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                                                billType = String.valueOf(billItems.getString("BILLTYPE"));
                                                billName = String.valueOf(billItems.get("NAME"));

                                                inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                                System.out.println("bill" + " " + billName + " " + billType + " " + bill + " " + billCreditPeriod + " ");
                                            }
                                            //System.out.println("ok tasted 4");
                                            //e.printStackTrace();

                                        } else {
                                            items = new ArrayList<>();
                                            System.out.println("ok null");
                                        }
                                }
                                if(String.valueOf(bill).contains("BANKALLOCATIONS.LIST"))
                                {
                                String ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                //System.out.println("the String is Object:"+op);
                                if (!ba.equals(null) && !ba.isEmpty())
                                    if (ba.charAt(0) == '{') {
                                        ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                        //\System.out.println("the String is Object:"+op);
                                        JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                        //System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                        //System.out.println("ok tasted 1");

                                    } else if (ba.charAt(0) == '[') {
                                        System.out.print("ok tasted 2");
                                        ba = bill.getString("BANKALLOCATIONS.LIST").toString();

                                        //System.out.println("the String is Array:"+op);
                                        String reString = bill.toString();
                                        //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                        JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                        //System.out.println("ok tasted 3");
                                        for (int b = 0; b < billlist.length(); b++) {
                                            JSONObject billItems = billlist.getJSONObject(b);
                                            // System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                                        }
                                        //System.out.println("ok tasted 4");
                                        //e.printStackTrace();

                                    } else {
                                        System.out.println("ok null");
                                    }
                            }
                                    listItes.add(new LedgerItem(amount, ledgerName, inventories));

                            }else if(var.charAt(0)=='[')
                            {
                                JSONArray list = voucher.getJSONArray("ALLLEDGERENTRIES.LIST");
                                items = new ArrayList<>();
                                for (int j = 0; j < list.length(); j++) {
                                    JSONObject bill = list.getJSONObject(j);
                                    System.out.println(String.valueOf(list.get(j)));
                                    //System.out.println(bill);
                                    inventories = new ArrayList<>();
                                    amount = String.valueOf(bill.get("AMOUNT"));
                                    ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                                    // System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");

                                    //System.out.println("the String is Object:"+op);
                                    if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                                        String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST"));
                                        if (!op.equals(null) && !op.isEmpty())
                                            if (op.charAt(0) == '{') {

                                                inventories = new ArrayList<>();
                                                // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                                //System.out.println("the String is Object:"+op);
                                                JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                                // System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                                //System.out.println("ok tasted 1");
                                                billAmount = String.valueOf(billl.get("AMOUNT"));
                                               // billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                                                billType = String.valueOf(billl.get("BILLTYPE"));
                                                billName = String.valueOf(billl.get("NAME"));
                                                System.out.println(billAmount + " " + billType + " " + billName);
                                                inventories.add(new Item(billName, billAmount, billType,""));
                                            } else if (op.charAt(0) == '[') {
                                                System.out.println("ok tasted 2");
                                                // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                                inventories = new ArrayList<>();
                                                //System.out.println("the String is Array:"+op);
                                               // String reString = bill.toString();
                                                //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                                JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                                //System.out.println("ok tasted 3");
                                                for (int b = 0; b < billlist.length(); b++) {
                                                    JSONObject billItems = billlist.getJSONObject(b);
                                                    billAmount = String.valueOf(billItems.get("AMOUNT"));
                                                   // billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                                                    billType = String.valueOf(billItems.get("BILLTYPE"));
                                                    billName = String.valueOf(billItems.get("NAME"));

                                                    inventories.add(new Item(billName, billAmount, billType, ""));
                                                    System.out.println("bill" + " " + billName + " " + billType + " " + bill + " ");
                                                }
                                                //System.out.println("ok tasted 4");
                                                //e.printStackTrace();

                                            } else {
                                                items = new ArrayList<>();
                                                System.out.println("ok null");
                                            }
                                    }
                                    if(String.valueOf(bill).contains("BANKALLOCATIONS.LIST"))
                                    {
                                        String ba = String.valueOf(bill.get("BANKALLOCATIONS.LIST"));
                                        //System.out.println("the String is Object:"+op);
                                        if (!ba.equals(null) && !ba.isEmpty())
                                            if (ba.charAt(0) == '{') {
                                               // ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                                //\System.out.println("the String is Object:"+op);
                                                JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                                //System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                                //System.out.println("ok tasted 1");

                                            } else if (ba.charAt(0) == '[') {
                                                System.out.print("ok tasted 2");
                                               /// ba = bill.getString("BANKALLOCATIONS.LIST").toString();

                                                //System.out.println("the String is Array:"+op);
                                                //String reString = bill.toString();
                                                //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                                JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                                //System.out.println("ok tasted 3");
                                                for (int b = 0; b < billlist.length(); b++) {
                                                    JSONObject billItems = billlist.getJSONObject(b);
                                                    // System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                                                }
                                                //System.out.println("ok tasted 4");
                                                //e.printStackTrace();

                                            } else {
                                                System.out.println("ok null");
                                            }
                                    }
                                    listItes.add(new LedgerItem(amount, ledgerName, inventories));
                                }

                            }

                        }
                        else {
                            System.out.println("all ledger");
                        }
                            //                        if(tamsg.get("\"ALLLEDGERENTRIES.LIST\""))


                        if(String.valueOf(tamsg.get("VOUCHER")).contains("ALLINVENTORYENTRIES.LIST")) {
                            String var = String.valueOf(voucher.get("ALLINVENTORYENTRIES.LIST"));
                            if (var.equalsIgnoreCase("")) {


                            } else if (var.charAt(0) == '{') {
                                JSONObject inventory = voucher.getJSONObject("ALLINVENTORYENTRIES.LIST");

                                // JSONObject bsc = inventory.getJSONObject("BASICUSERDESCRIPTION.LIST");
//                        System.out.println(bsc.get("BASICUSERDESCRIPTION") + " " + inventory.get("STOCKITEMNAME") + " " + inventory.get("RATE") + " " + inventory.get("AMOUNT") + " " + inventory.get("BILLEDQTY") + " " + inventory.get("ACTUALQTY"));

                                //  basicUserDesc = String.valueOf(bsc.get("BASICUSERDESCRIPTION"));
                                List<BatchAllocation> batchAllocationList = new ArrayList<>();
                                if (String.valueOf(inventory.get("BATCHALLOCATIONS.LIST")).equalsIgnoreCase("")) {

                                } else if (String.valueOf(inventory.get("BATCHALLOCATIONS.LIST")).charAt(0) == '{') {
                                    JSONObject batch = inventory.getJSONObject("BATCHALLOCATIONS.LIST");
                                    //  System.out.println(batch.getString("GODOWNNAME") + " " + batch.get("BATCHNAME") + batch.get("ORDERNO") + batch.get("AMOUNT") + " " + batch.get("ACTUALQTY") + " " + batch.get("BILLEDQTY"));

                                    String overduevalue = String.valueOf(batch.get("ORDERDUEDATE"));
                                    JSONObject overdue;
                                    String p = null;
                                    if (overduevalue.equalsIgnoreCase("")) {
                                        p = null;
                                    } else if (overduevalue.charAt(0) == '{') {
                                        overdue = batch.getJSONObject("ORDERDUEDATE");
                                        p = String.valueOf(overdue.get("P"));
                                    } else {
                                        p = null;
                                    }

                                    gowdownName = String.valueOf(batch.get("GODOWNNAME"));
                                    batchName = String.valueOf(batch.get("BATCHNAME"));

                                    basicPurchaseOrderNumber = String.valueOf(batch.get("ORDERNO"));
                                    String batchamount1 = String.valueOf(batch.get("AMOUNT"));
                                    String batchactualQty1 = String.valueOf(batch.get("ACTUALQTY"));
                                    System.out.println(gowdownName + " " + batchName + " " + basicPurchaseOrderNumber + " " + batchamount1 + " " + batchactualQty1);

//                    //System.out.println(overdue.get("P"));
                                    String batchbillqty = String.valueOf(batch.get("BILLEDQTY"));
                                    batchAllocationList.add(new BatchAllocation(gowdownName, batchName, String.valueOf(basicPurchaseOrderNumber), String.valueOf(batchamount1), String.valueOf(batchactualQty1), String.valueOf(batchbillqty), p));
                                    stockName = String.valueOf(inventory.get("STOCKITEMNAME"));
                                    rate = String.valueOf(inventory.get("RATE"));
                                    String stockAmount = String.valueOf(inventory.get("AMOUNT"));
                                    String stockBillQty = String.valueOf(inventory.get("BILLEDQTY"));
                                    String stockActualQty = String.valueOf(inventory.get("ACTUALQTY"));
                                    inventoriesList.add(new Inventories(stockName, "", rate, stockAmount, stockBillQty, stockActualQty, batchAllocationList));
                                }


                                // DashType type = DashType.valueOf(voucher.getString("VOUCHERTYPENAME"));
                                String invenString = String.valueOf(voucher.get("INVOICEORDERLIST.LIST"));
                                if (invenString.equalsIgnoreCase("")) {

                                } else if (invenString.charAt(0) == '{') {
                                    JSONObject invoicebill = voucher.getJSONObject("INVOICEORDERLIST.LIST");
                                    String invoicebrt = String.valueOf(invoicebill.get("BASICORDERDATE"));
                                    orderType = String.valueOf(invoicebill.get("ORDERTYPE"));
                                    String bsrd = String.valueOf(invoicebill.get("BASICPURCHASEORDERNO"));
                                    System.out.println("invoicebill" + invoicebrt + " " + orderType + " " + bsrd);
                                } else {

                                }


                            } else {
                                System.out.println("null");
                            }
                        }
                        if(String.valueOf(voucher).contains("LEDGERENTRIES.LIST"))
                            if (String.valueOf(voucher.get("LEDGERENTRIES.LIST")).equalsIgnoreCase("")) {

                            } else if (String.valueOf(voucher.get("LEDGERENTRIES.LIST")).charAt(0) == '{') {
                                items = new ArrayList<>();

                                    JSONObject bill = voucher.getJSONObject("LEDGERENTRIES.LIST");
                                    //System.out.println(bill);
                                    inventories = new ArrayList<>();
                                    amount = String.valueOf(bill.get("AMOUNT"));
                                    ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                                    //  System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");

                                    //System.out.println("the String is Object:"+op);
                                if(String.valueOf(voucher.get("LEDGERENTRIES.LIST")).contains("BILLALLOCATIONS.LIST")) {
                                    String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST")).toString();
                                    if (!op.equals(null) && !op.isEmpty())
                                        if (op.charAt(0) == '{') {
                                            inventories = new ArrayList<>();
                                            // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                            //System.out.println("the String is Object:"+op);

                                            JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                            //  System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                            //System.out.println("ok tasted 1");
                                            billAmount = String.valueOf(billl.get("AMOUNT"));
                                            billType = String.valueOf(billl.get("BILLTYPE"));
                                            billName = String.valueOf(billl.get("NAME"));
                                            System.out.println(billAmount  + " " + billType + " " + billName);
                                            inventories.add(new Item(billName, billAmount, billType, ""));
                                        } else if (op.charAt(0) == '[') {
                                            System.out.println("ok tasted 2");
                                            // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                            inventories = new ArrayList<>();
                                            //System.out.println("the String is Array:"+op);
                                           // String reString = bill.toString();
                                            //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                            JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                            //System.out.println("ok tasted 3");
                                            for (int b = 0; b < billlist.length(); b++) {
                                                JSONObject billItems = billlist.getJSONObject(b);
                                                billAmount = String.valueOf(billItems.get("AMOUNT"));
                                                billType = String.valueOf(billItems.get("BILLTYPE"));
                                                billName = String.valueOf(billItems.get("NAME"));
                                                inventories.add(new Item(billName, billAmount, billType,""));
                                                System.out.println(billAmount + " " + billType + " " + billName);
                                                //    System.out.println("bill" + " " + billItems.get("NAME") + " " + billItems.get("BILLTYPE") + " " + billItems.get("AMOUNT") + " " + billItems.getString("BILLCREDITPERIOD") + " ");
                                            }
                                            //System.out.println("ok tasted 4");
                                            //e.printStackTrace();
                                        } else {
                                            items = new ArrayList<>();
                                            System.out.println("ok null");
                                        }
                                }

                                    if(String.valueOf(voucher.get("LEDGERENTRIES.LIST")).contains("BANKALLOCATIONS.LIST")) {
                                        String ba = String.valueOf(bill.get("BANKALLOCATIONS.LIST")).toString();
                                        //System.out.println("the String is Object:"+op);
                                        if (!ba.equals(null) && !ba.isEmpty())
                                            if (ba.charAt(0) == '{') {
                                               // ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                                //\System.out.println("the String is Object:"+op);
                                                JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                                //System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                                //System.out.println("ok tasted 1");
                                            } else if (ba.charAt(0) == '[') {
                                                System.out.print("ok tasted 2");
                                               // ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                                //System.out.println("the String is Array:"+op);
                                               // String reString = bill.toString();
                                                //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                                JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                                //System.out.println("ok tasted 3");
                                                for (int b = 0; b < billlist.length(); b++) {
                                                    JSONObject billItems = billlist.getJSONObject(b);
                                                    //  System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                                                }
                                                //System.out.println("ok tasted 4");
                                                //e.printStackTrace();
                                            } else {
                                                System.out.println("ok null");
                                            }
                                    }
                                    items.add(new LedgerItem(amount, ledgerName, inventories, null));

                            }
                            else if (String.valueOf(voucher.get("LEDGERENTRIES.LIST")).charAt(0) == '[')
                            {
                                JSONArray list = voucher.getJSONArray("LEDGERENTRIES.LIST");
                                items = new ArrayList<>();
                                for (int j = 0; j < list.length(); j++) {
                                    JSONObject bill = list.getJSONObject(j);
                                    //System.out.println(String.valueOf(list.get(j)));
                                    //System.out.println(bill);
                                    inventories = new ArrayList<>();
                                    amount = String.valueOf(bill.get("AMOUNT"));
                                    ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                                    // System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");

                                    //System.out.println("the String is Object:"+op);
                                    if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                                        String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST")).toString();
                                        if (!op.equals(null) && !op.isEmpty())
                                            if (op.charAt(0) == '{') {

                                                inventories = new ArrayList<>();
                                                // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                                //System.out.println("the String is Object:"+op);
                                                JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                                // System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                                //System.out.println("ok tasted 1");
                                                billAmount = String.valueOf(billl.get("AMOUNT"));
                                                billType = String.valueOf(billl.get("BILLTYPE"));
                                                billName = String.valueOf(billl.get("NAME"));
                                                System.out.println(billAmount + " " + billType + " " + billName);
                                                inventories.add(new Item(billName, billAmount, billType, ""));
                                            } else if (op.charAt(0) == '[') {
                                                System.out.println("ok tasted 2");
                                                // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                                inventories = new ArrayList<>();
                                                //System.out.println("the String is Array:"+op);
                                                //
                                                // String reString = bill.toString();
                                                //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                                JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                                //System.out.println("ok tasted 3");
                                                for (int b = 0; b < billlist.length(); b++) {
                                                    JSONObject billItems = billlist.getJSONObject(b);
                                                    billAmount = String.valueOf(billItems.get("AMOUNT"));
                                                    billType = String.valueOf(billItems.get("BILLTYPE"));
                                                    billName = String.valueOf(billItems.get("NAME"));

                                                    inventories.add(new Item(billName, billAmount, billType, ""));
                                                    System.out.println("bill" + " " + billName + " " + billType + " " + billAmount );
                                                }
                                                //System.out.println("ok tasted 4");
                                                //e.printStackTrace();

                                            } else {
                                                items = new ArrayList<>();
                                                System.out.println("ok null");
                                            }
                                    }
                                    if(String.valueOf(bill).contains("BANKALLOCATIONS.LIST"))
                                    {
                                        String ba = String.valueOf(bill.get("BANKALLOCATIONS.LIST"));
                                        //System.out.println("the String is Object:"+op);
                                        if (!ba.equals(null) && !ba.isEmpty())
                                            if (ba.charAt(0) == '{') {
                                                //\System.out.println("the String is Object:"+op);
                                                JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                                System.out.println("bank" + " " + String.valueOf(billl.get("DATE"))  + " " + String.valueOf(billl.get("TRANSACTIONTYPE")) + " " + String.valueOf(billl.get("PAYMENTFAVOURING")) + " " + String.valueOf(billl.get("AMOUNT")) + " " + String.valueOf(billl.get("PAYMENTFAVOURING")) + " " + String.valueOf(billl.get("PAYMENTFAVOURING")));
                                                //System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                                //System.out.println("ok tasted 1");

                                            } else if (ba.charAt(0) == '[') {
                                                System.out.print("ok tasted 2");

                                                JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                                //System.out.println("ok tasted 3");
                                                for (int b = 0; b < billlist.length(); b++) {
                                                    JSONObject billItems = billlist.getJSONObject(b);
                                                     System.out.println("bank" + " " + String.valueOf(billItems.get("DATE"))  + " " + String.valueOf(billItems.get("TRANSACTIONTYPE")) + " " + String.valueOf(billItems.get("PAYMENTFAVOURING")) + " " + String.valueOf(billItems.get("AMOUNT")) + " " + String.valueOf(billItems.get("PAYMENTFAVOURING")) + " " + String.valueOf(billItems.get("PAYMENTFAVOURING")));

                                                }
                                                //System.out.println("ok tasted 4");
                                                //e.printStackTrace();

                                            } else {
                                                System.out.println("ok null");
                                            }
                                    }
                                    items.add(new LedgerItem(amount, ledgerName, inventories));
                                }
                            }






                        salesVouchers.add(new SalesVoucher(voucherDate, voucherPartyName, voucherTypeName, "Sales", voucherNumber, voucherNarration, inventoriesList, null, items));
                    }

                }else if(String.valueOf(data.get("TALLYMESSAGE")).charAt(0)=='{')
                {

                    invoiceOrderLists = new ArrayList<>();
                    JSONObject tamsg = data.getJSONObject("TALLYMESSAGE");
                    //System.out.println(tamsg);
                    JSONObject voucher = tamsg.getJSONObject("VOUCHER");
                    inventoriesList = new ArrayList<>();
                    //  System.out.print("Voucher" + " " + voucher.get("DATE") + " " + voucher.getString("PARTYLEDGERNAME") + " " + voucher.getString("VOUCHERTYPENAME") + " " + voucher.getString("VOUCHERNUMBER"));
                    //  System.out.print("Voucher" + " " + voucher.get("DATE") + " " + voucher.getString("PARTYLEDGERNAME") + " " + voucher.getString("VOUCHERTYPENAME") + " " + voucher.getString("VOUCHERNUMBER"));
                    voucherDate = String.valueOf(voucher.get("DATE"));
                    voucherPartyName = String.valueOf(voucher.get("PARTYLEDGERNAME"));
                    voucherNumber = String.valueOf(voucher.get("VOUCHERNUMBER"));
                    voucherTypeName = String.valueOf(voucher.get("VOUCHERTYPENAME"));
                    voucherNarration = String.valueOf(voucher.get("NARRATION"));
                    System.out.println(voucherPartyName + " " + voucherDate + " " + voucherNumber + " " + voucherTypeName + " " + voucherNarration);
                    //voucherType v;
                    //System.out.print(" " + voucher.get("NARRATION"));

                    if(String.valueOf(tamsg.get("VOUCHER")).contains("ALLLEDGERENTRIES.LIST"))
                    {
                        String var= String.valueOf(voucher.get("ALLLEDGERENTRIES.LIST"));
                        if(var.equalsIgnoreCase(""))
                        {

                        }else if (var.charAt(0)=='{') {

                            JSONObject bill = voucher.getJSONObject("ALLLEDGERENTRIES.LIST");
                            //System.out.println(String.valueOf(list.get(j)));
                            //System.out.println(bill);
                            inventories = new ArrayList<>();
                            amount = String.valueOf(bill.get("AMOUNT"));
                            ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                            // System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");

                            //System.out.println("the String is Object:"+op);
                            if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                                String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST"));
                                if (!op.equals(null) && !op.isEmpty())
                                    if (op.charAt(0) == '{') {

                                        inventories = new ArrayList<>();
                                        // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                        //System.out.println("the String is Object:"+op);
                                        JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                        // System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                        //System.out.println("ok tasted 1");
                                        billAmount = String.valueOf(billl.get("AMOUNT"));
                                        billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                                        billType = String.valueOf(billl.get("BILLTYPE"));
                                        billName = String.valueOf(billl.get("NAME"));
                                        System.out.println(billAmount + " " + bill + " " + billType + " " + billName);
                                        inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                    } else if (op.charAt(0) == '[') {
                                        System.out.println("ok tasted 2");
                                        // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                        inventories = new ArrayList<>();
                                        //System.out.println("the String is Array:"+op);
                                       // String reString = bill.toString();
                                        //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                        JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                        //System.out.println("ok tasted 3");
                                        for (int b = 0; b < billlist.length(); b++) {
                                            JSONObject billItems = billlist.getJSONObject(b);
                                            billAmount = String.valueOf(billItems.get("AMOUNT"));
                                            billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                                            billType = String.valueOf(billItems.get("BILLTYPE"));
                                            billName = String.valueOf(billItems.get("NAME"));

                                            inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                            System.out.println("bill" + " " + billName + " " + billType + " " + bill + " " + billCreditPeriod + " ");
                                        }
                                        //System.out.println("ok tasted 4");
                                        //e.printStackTrace();

                                    } else {
                                        items = new ArrayList<>();
                                        System.out.println("ok null");
                                    }
                            }
                            if(String.valueOf(bill).contains("BANKALLOCATIONS.LIST"))
                            {
                                String ba = String.valueOf(bill.get("BANKALLOCATIONS.LIST"));
                                //System.out.println("the String is Object:"+op);
                                if (!ba.equals(null) && !ba.isEmpty())
                                    if (ba.charAt(0) == '{') {
                                       // ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                        //\System.out.println("the String is Object:"+op);
                                        JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                        System.out.println("bank" + " " + String.valueOf(billl.get("DATE"))  + " " + String.valueOf(billl.get("TRANSACTIONTYPE")) + " " + String.valueOf(billl.get("PAYMENTFAVOURING")) + " " + String.valueOf(billl.get("AMOUNT")) + " " + String.valueOf(billl.get("PAYMENTFAVOURING")) + " " + String.valueOf(billl.get("PAYMENTFAVOURING")));

                                        //System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                        //System.out.println("ok tasted 1");

                                    } else if (ba.charAt(0) == '[') {
                                        System.out.print("ok tasted 2");
                                       // ba = bill.getString("BANKALLOCATIONS.LIST").toString();

                                        //System.out.println("the String is Array:"+op);
                                        //String reString = bill.toString();
                                        //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                        JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                        //System.out.println("ok tasted 3");
                                        for (int b = 0; b < billlist.length(); b++) {
                                            JSONObject billItems = billlist.getJSONObject(b);
                                            System.out.println("bank" + " " + String.valueOf(billItems.get("DATE"))  + " " + String.valueOf(billItems.get("TRANSACTIONTYPE")) + " " + String.valueOf(billItems.get("PAYMENTFAVOURING")) + " " + String.valueOf(billItems.get("AMOUNT")) + " " + String.valueOf(billItems.get("PAYMENTFAVOURING")) + " " + String.valueOf(billItems.get("PAYMENTFAVOURING")));

                                            // System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                                        }
                                        //System.out.println("ok tasted 4");
                                        //e.printStackTrace();

                                    } else {
                                        System.out.println("ok null");
                                    }
                            }
                            listItes.add(new LedgerItem(amount, ledgerName, inventories));

                        }else if(var.charAt(0)=='[')
                        {
                            JSONArray list = voucher.getJSONArray("ALLLEDGERENTRIES.LIST");
                            items = new ArrayList<>();
                            for (int j = 0; j < list.length(); j++) {
                                JSONObject bill = list.getJSONObject(j);
                                System.out.println(String.valueOf(list.get(j)));
                                //System.out.println(bill);
                                inventories = new ArrayList<>();
                                amount = String.valueOf(bill.get("AMOUNT"));
                                ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                                // System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");

                                //System.out.println("the String is Object:"+op);
                                if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                                    String op = bill.get("BILLALLOCATIONS.LIST").toString();
                                    if (!op.equals(null) && !op.isEmpty())
                                        if (op.charAt(0) == '{') {

                                            inventories = new ArrayList<>();
                                            // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                            //System.out.println("the String is Object:"+op);
                                            JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                            // System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                            //System.out.println("ok tasted 1");
                                            billAmount = String.valueOf(billl.get("AMOUNT"));
                                            billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                                            billType = String.valueOf(billl.get("BILLTYPE"));
                                            billName = String.valueOf(billl.get("NAME"));
                                            System.out.println(billAmount + " " + bill + " " + billType + " " + billName);
                                            inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                        } else if (op.charAt(0) == '[') {
                                            System.out.println("ok tasted 2");
                                            // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                            inventories = new ArrayList<>();
                                            //System.out.println("the String is Array:"+op);
                                            //String reString = bill.toString();
                                            //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                            JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                            //System.out.println("ok tasted 3");
                                            for (int b = 0; b < billlist.length(); b++) {
                                                JSONObject billItems = billlist.getJSONObject(b);
                                                billAmount = String.valueOf(billItems.get("AMOUNT"));
                                                billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                                                billType = String.valueOf(billItems.getString("BILLTYPE"));
                                                billName = String.valueOf(billItems.get("NAME"));

                                                inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                                                System.out.println("bill" + " " + billName + " " + billType + " " + bill + " " + billCreditPeriod + " ");
                                            }
                                            //System.out.println("ok tasted 4");
                                            //e.printStackTrace();

                                        } else {
                                            items = new ArrayList<>();
                                            System.out.println("ok null");
                                        }
                                }
                                if(String.valueOf(bill).contains("BANKALLOCATIONS.LIST"))
                                {
                                    String ba = String.valueOf(bill.getString("BANKALLOCATIONS.LIST"));
                                    //System.out.println("the String is Object:"+op);
                                    if (!ba.equals(null) && !ba.isEmpty())
                                        if (ba.charAt(0) == '{') {
                                           // ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                            //\System.out.println("the String is Object:"+op);
                                            JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                            //System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                            //System.out.println("ok tasted 1");

                                        } else if (ba.charAt(0) == '[') {
                                            System.out.print("ok tasted 2");
                                           // ba = bill.getString("BANKALLOCATIONS.LIST").toString();

                                            //System.out.println("the String is Array:"+op);
                                            //String reString = bill.toString();
                                            //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                            JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                            //System.out.println("ok tasted 3");
                                            for (int b = 0; b < billlist.length(); b++) {
                                                JSONObject billItems = billlist.getJSONObject(b);
                                                // System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                                            }
                                            //System.out.println("ok tasted 4");
                                            //e.printStackTrace();

                                        } else {
                                            System.out.println("ok null");
                                        }
                                }
                                listItes.add(new LedgerItem(amount, ledgerName, inventories));
                            }

                        }

                    }
                    else {
                        System.out.println("all ledger");
                    }
                    //                        if(tamsg.get("\"ALLLEDGERENTRIES.LIST\""))


                    if(String.valueOf(tamsg.get("VOUCHER")).contains("ALLINVENTORYENTRIES.LIST")) {
                        String var = String.valueOf(voucher.get("ALLINVENTORYENTRIES.LIST"));
                        if (var.equalsIgnoreCase("")) {


                        } else if (var.charAt(0) == '{') {
                            JSONObject inventory = voucher.getJSONObject("ALLINVENTORYENTRIES.LIST");

                            // JSONObject bsc = inventory.getJSONObject("BASICUSERDESCRIPTION.LIST");
//                        System.out.println(bsc.get("BASICUSERDESCRIPTION") + " " + inventory.get("STOCKITEMNAME") + " " + inventory.get("RATE") + " " + inventory.get("AMOUNT") + " " + inventory.get("BILLEDQTY") + " " + inventory.get("ACTUALQTY"));

                            //  basicUserDesc = String.valueOf(bsc.get("BASICUSERDESCRIPTION"));
                            List<BatchAllocation> batchAllocationList = new ArrayList<>();
                            if (String.valueOf(inventory.get("BATCHALLOCATIONS.LIST")).equalsIgnoreCase("")) {

                            } else if (String.valueOf(inventory.get("BATCHALLOCATIONS.LIST")).charAt(0) == '{') {
                                JSONObject batch = inventory.getJSONObject("BATCHALLOCATIONS.LIST");
                                //  System.out.println(batch.getString("GODOWNNAME") + " " + batch.get("BATCHNAME") + batch.get("ORDERNO") + batch.get("AMOUNT") + " " + batch.get("ACTUALQTY") + " " + batch.get("BILLEDQTY"));

                                String overduevalue = String.valueOf(batch.get("ORDERDUEDATE"));
                                JSONObject overdue;
                                String p = null;
                                if (overduevalue.equalsIgnoreCase("")) {
                                    p = null;
                                } else if (overduevalue.charAt(0) == '{') {
                                    overdue = batch.getJSONObject("ORDERDUEDATE");
                                    p = String.valueOf(overdue.get("P"));
                                } else {
                                    p = null;
                                }

                                gowdownName = String.valueOf(batch.get("GODOWNNAME"));
                                batchName = String.valueOf(batch.get("BATCHNAME"));

                                basicPurchaseOrderNumber = String.valueOf(batch.get("ORDERNO"));
                                String batchamount1 = String.valueOf(batch.get("AMOUNT"));
                                String batchactualQty1 = String.valueOf(batch.get("ACTUALQTY"));
                                System.out.println(gowdownName + " " + batchName + " " + basicPurchaseOrderNumber + " " + batchamount1 + " " + batchactualQty1);

//                    //System.out.println(overdue.get("P"));
                                String batchbillqty = String.valueOf(batch.get("BILLEDQTY"));
                                batchAllocationList.add(new BatchAllocation(gowdownName, batchName, String.valueOf(basicPurchaseOrderNumber), String.valueOf(batchamount1), String.valueOf(batchactualQty1), String.valueOf(batchbillqty), p));
                                stockName = String.valueOf(inventory.get("STOCKITEMNAME"));
                                rate = String.valueOf(inventory.get("RATE"));
                                String stockAmount = String.valueOf(inventory.get("AMOUNT"));
                                String stockBillQty = String.valueOf(inventory.get("BILLEDQTY"));
                                String stockActualQty = String.valueOf(inventory.get("ACTUALQTY"));
                                inventoriesList.add(new Inventories(stockName, "", rate, stockAmount, stockBillQty, stockActualQty, batchAllocationList));
                            }


                            // DashType type = DashType.valueOf(voucher.getString("VOUCHERTYPENAME"));
                            String invenString = String.valueOf(voucher.get("INVOICEORDERLIST.LIST"));
                            if (invenString.equalsIgnoreCase("")) {

                            } else if (invenString.charAt(0) == '{') {
                                JSONObject invoicebill = voucher.getJSONObject("INVOICEORDERLIST.LIST");
                                String invoicebrt = String.valueOf(invoicebill.get("BASICORDERDATE"));
                                orderType = String.valueOf(invoicebill.get("ORDERTYPE"));
                                String bsrd = String.valueOf(invoicebill.get("BASICPURCHASEORDERNO"));
                                System.out.println("invoicebill" + invoicebrt + " " + orderType + " " + bsrd);
                            } else {

                            }


                        } else {
                            System.out.println("null");
                        }
                    }
                    if(String.valueOf(voucher).contains("LEDGERENTRIES.LIST"))
                        if (String.valueOf(voucher.get("LEDGERENTRIES.LIST")).equalsIgnoreCase("")) {

                        } else if (String.valueOf(voucher.get("LEDGERENTRIES.LIST")).charAt(0) == '{') {
                            items = new ArrayList<>();

                            JSONObject bill = voucher.getJSONObject("LEDGERENTRIES.LIST");
                            //System.out.println(bill);
                            inventories = new ArrayList<>();
                            amount = String.valueOf(bill.get("AMOUNT"));
                            ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                            //  System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");

                            //System.out.println("the String is Object:"+op);
                            if(String.valueOf(voucher.get("LEDGERENTRIES.LIST")).contains("BILLALLOCATIONS.LIST")) {
                                String op = bill.get("BILLALLOCATIONS.LIST").toString();
                                if (!op.equals(null) && !op.isEmpty())
                                    if (op.charAt(0) == '{') {

                                        inventories = new ArrayList<>();
                                        // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                        //System.out.println("the String is Object:"+op);

                                        JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                        //  System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                        //System.out.println("ok tasted 1");
                                        billAmount = String.valueOf(billl.get("AMOUNT"));
                                        billType = String.valueOf(billl.get("BILLTYPE"));
                                        billName = String.valueOf(billl.get("NAME"));
                                        System.out.println(billAmount  + " " + billType + " " + billName);
                                        inventories.add(new Item(billName, billAmount, billType, ""));
                                    } else if (op.charAt(0) == '[') {
                                        System.out.println("ok tasted 2");
                                        // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                        inventories = new ArrayList<>();
                                        //System.out.println("the String is Array:"+op);
                                        //String reString = bill.toString();
                                        //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                        JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                        //System.out.println("ok tasted 3");
                                        for (int b = 0; b < billlist.length(); b++) {
                                            JSONObject billItems = billlist.getJSONObject(b);
                                            billAmount = String.valueOf(billItems.get("AMOUNT"));
                                            billType = String.valueOf(billItems.get("BILLTYPE"));
                                            billName = String.valueOf(billItems.get("NAME"));
                                            inventories.add(new Item(billName, billAmount, billType,""));
                                            System.out.println(billAmount + " " + billType + " " + billName);
                                            //    System.out.println("bill" + " " + billItems.get("NAME") + " " + billItems.get("BILLTYPE") + " " + billItems.get("AMOUNT") + " " + billItems.getString("BILLCREDITPERIOD") + " ");
                                        }
                                        //System.out.println("ok tasted 4");
                                        //e.printStackTrace();
                                    } else {
                                        items = new ArrayList<>();
                                        System.out.println("ok null");
                                    }
                            }

                            if(String.valueOf(voucher.get("LEDGERENTRIES.LIST")).contains("BANKALLOCATIONS.LIST")) {
                                String ba = String.valueOf(bill.get("BANKALLOCATIONS.LIST"));
                                //System.out.println("the String is Object:"+op);
                                if (!ba.equals(null) && !ba.isEmpty())
                                    if (ba.charAt(0) == '{') {
                                      //  ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                        //\System.out.println("the String is Object:"+op);
                                        JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                        //System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                        //System.out.println("ok tasted 1");
                                    } else if (ba.charAt(0) == '[') {
                                        System.out.print("ok tasted 2");
                                      //  ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                                        //System.out.println("the String is Array:"+op);
                                        //String reString = bill.toString();
                                        //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                        JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                        //System.out.println("ok tasted 3");
                                        for (int b = 0; b < billlist.length(); b++) {
                                            JSONObject billItems = billlist.getJSONObject(b);
                                            //  System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                                        }
                                        //System.out.println("ok tasted 4");
                                        //e.printStackTrace();
                                    } else {
                                        System.out.println("ok null");
                                    }
                            }
                            items.add(new LedgerItem(amount, ledgerName, inventories, null));

                        }
                        else if (String.valueOf(voucher.get("LEDGERENTRIES.LIST")).charAt(0) == '[')
                        {
                            JSONArray list = voucher.getJSONArray("LEDGERENTRIES.LIST");
                            items = new ArrayList<>();
                            for (int j = 0; j < list.length(); j++) {
                                JSONObject bill = list.getJSONObject(j);
                                //System.out.println(String.valueOf(list.get(j)));
                                //System.out.println(bill);
                                inventories = new ArrayList<>();
                                amount = String.valueOf(bill.get("AMOUNT"));
                                ledgerName = String.valueOf(bill.get("LEDGERNAME"));
                                // System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");

                                //System.out.println("the String is Object:"+op);
                                if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                                    String op = String.valueOf(bill.get("BILLALLOCATIONS.LIST")).toString();
                                    if (!op.equals(null) && !op.isEmpty())
                                        if (op.charAt(0) == '{') {

                                            inventories = new ArrayList<>();
                                            // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                                            //System.out.println("the String is Object:"+op);
                                            JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                                            // System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                                            //System.out.println("ok tasted 1");
                                            billAmount = String.valueOf(billl.get("AMOUNT"));
                                            billType = String.valueOf(billl.get("BILLTYPE"));
                                            billName = String.valueOf(billl.get("NAME"));
                                            System.out.println(billAmount + " " + billType + " " + billName);
                                            inventories.add(new Item(billName, billAmount, billType, ""));
                                        } else if (op.charAt(0) == '[') {
                                            System.out.println("ok tasted 2");
                                            // op=bill.get("BILLALLOCATIONS.LIST").toString();
                                            inventories = new ArrayList<>();
                                            //System.out.println("the String is Array:"+op);
                                            //String reString = bill.toString();
                                            //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                            JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                                            //System.out.println("ok tasted 3");
                                            for (int b = 0; b < billlist.length(); b++) {
                                                JSONObject billItems = billlist.getJSONObject(b);
                                                billAmount = String.valueOf(billItems.get("AMOUNT"));
                                                billType = String.valueOf(billItems.get("BILLTYPE"));
                                                billName = String.valueOf(billItems.get("NAME"));

                                                inventories.add(new Item(billName, billAmount, billType, ""));
                                                System.out.println("bill" + " " + billName + " " + billType + " " + billAmount );
                                            }
                                            //System.out.println("ok tasted 4");
                                            //e.printStackTrace();

                                        } else {
                                            items = new ArrayList<>();
                                            System.out.println("ok null");
                                        }
                                }
                                if(String.valueOf(bill).contains("BANKALLOCATIONS.LIST"))
                                {
                                    String ba = String.valueOf(bill.get("BANKALLOCATIONS.LIST"));
                                    //System.out.println("the String is Object:"+op);
                                    if (!ba.equals(null) && !ba.isEmpty())
                                        if (ba.charAt(0) == '{') {
                                            //\System.out.println("the String is Object:"+op);
                                            JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                                            //System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                                            //System.out.println("ok tasted 1");

                                        } else if (ba.charAt(0) == '[') {
                                            System.out.print("ok tasted 2");
                                            //System.out.println("the String is Array:"+op);
                                            //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                                            JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                                            //System.out.println("ok tasted 3");
                                            for (int b = 0; b < billlist.length(); b++) {
                                                JSONObject billItems = billlist.getJSONObject(b);
                                                // System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                                            }
                                            //System.out.println("ok tasted 4");
                                            //e.printStackTrace();

                                        } else {
                                            System.out.println("ok null");
                                        }
                                }
                                items.add(new LedgerItem(amount, ledgerName, inventories));
                            }
                        }






                    salesVouchers.add(new SalesVoucher(voucherDate, voucherPartyName, voucherTypeName, "Sales", voucherNumber, voucherNarration, inventoriesList, null, items));
                }

            }


        } catch (Exception e) {
            e.getMessage();
        }
        if (salesVouchers.size() == 0) {
            salesVouchers = new ArrayList<>();
        }
        return salesVouchers;
    }

    List<LedgerItem> getAllLedgerItem(JSONObject voucher) {
        String voucherDate;
        String voucherPartyName;
        String voucherNumber;
        String voucherNarration;
        String voucherTypeName;
        String basicUserDesc;
        Double rate;
        String amount;
        int edqty;
        int actualQty;

        String stockName;
        String gowdownName;
        String batchName;
        Double batchamount;
        int batchactualQty;
        int billQty;
        Date orderDueDate;
        List<LedgerItem> items = null;
        List<Item> inventories = null;
        Double basicOrderRate;
        String orderType;
        String basicPurchaseOrderNumber;
        Double orderamount;
        String ledgerName;
        String billName;
        String billAmount;
        String billType;
        String billCreditPeriod;
        List<SalesVoucher> salesVouchers = new ArrayList<>();
        List<InvoiceOrderList> invoiceOrderLists = null;
        List<Inventories> inventoriesList = null;
        ArrayList<LedgerItem> listItes = new ArrayList<>();
        JSONArray list = voucher.getJSONArray("ALLLEDGERENTRIES.LIST");
        items = new ArrayList<>();
        for (int j = 0; j < list.length(); j++) {
            JSONObject bill = list.getJSONObject(j);
            System.out.println(String.valueOf(list.get(j)));
            //System.out.println(bill);
            inventories = new ArrayList<>();
            amount = String.valueOf(bill.get("AMOUNT"));
            ledgerName = String.valueOf(bill.get("LEDGERNAME"));
            // System.out.println(bill.get("AMOUNT") + " " + bill.getString("LEDGERNAME") + " ");
            if (String.valueOf(bill).contains("BILLALLOCATIONS.LIST")) {
                String op = bill.get("BILLALLOCATIONS.LIST").toString();
                //System.out.println("the String is Object:"+op);
                if (!op.equals(null) && !op.isEmpty())
                    if (op.charAt(0) == '{') {

                        inventories = new ArrayList<>();
                        // op=bill.getString("BILLALLOCATIONS.LIST").toString();
                        //System.out.println("the String is Object:"+op);
                        JSONObject billl = bill.getJSONObject("BILLALLOCATIONS.LIST");
                        // System.out.println(billl.get("NAME") + " " + billl.get("AMOUNT") + " " + billl.getString("BILLTYPE") + " " + billl.getString("BILLCREDITPERIOD") + " ");
                        //System.out.println("ok tasted 1");
                        billAmount = String.valueOf(billl.get("AMOUNT"));
                        billCreditPeriod = String.valueOf(billl.get("BILLCREDITPERIOD"));
                        billType = String.valueOf(billl.get("BILLTYPE"));
                        billName = String.valueOf(billl.get("NAME"));
                        System.out.println(billAmount + " " + bill + " " + billType + " " + billName);
                        inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                    } else if (op.charAt(0) == '[') {
                        System.out.println("ok tasted 2");
                        // op=bill.get("BILLALLOCATIONS.LIST").toString();
                        inventories = new ArrayList<>();
                        //System.out.println("the String is Array:"+op);
                        String reString = bill.toString();
                        //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                        JSONArray billlist = bill.getJSONArray("BILLALLOCATIONS.LIST");
                        //System.out.println("ok tasted 3");
                        for (int b = 0; b < billlist.length(); b++) {
                            JSONObject billItems = billlist.getJSONObject(b);
                            billAmount = String.valueOf(billItems.get("AMOUNT"));
                            billCreditPeriod = String.valueOf(billItems.get("BILLCREDITPERIOD"));
                            billType = String.valueOf(billItems.get("BILLTYPE"));
                            billName = String.valueOf(billItems.get("NAME"));

                            inventories.add(new Item(billName, billAmount, billType, billCreditPeriod));
                            System.out.println("bill" + " " + billName + " " + billType + " " + bill + " " + billCreditPeriod + " ");
                        }
                        //System.out.println("ok tasted 4");
                        //e.printStackTrace();

                    } else {
                        items = new ArrayList<>();
                        System.out.println("ok null");
                    }
            }
            if(String.valueOf(bill).contains("BANKALLOCATIONS.LIST")) {
                String ba = String.valueOf(bill.get("BANKALLOCATIONS.LIST")).toString();
                //System.out.println("the String is Object:"+op);
                if (!ba.equals(null) && !ba.isEmpty())
                    if (ba.charAt(0) == '{') {
                       // ba = bill.getString("BANKALLOCATIONS.LIST").toString();
                        //\System.out.println("the String is Object:"+op);
                        JSONObject billl = bill.getJSONObject("BANKALLOCATIONS.LIST");
                        //System.out.println("bank" + " " + billl.get("DATE") + " " + billl.getString("TRANSACTIONTYPE") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.get("AMOUNT") + " " + billl.getString("PAYMENTFAVOURING") + " " + billl.getString("PAYMENTFAVOURING"));
                        //System.out.println("ok tasted 1");

                    } else if (ba.charAt(0) == '[') {
                        System.out.print("ok tasted 2");
                        //ba = bill.getString("BANKALLOCATIONS.LIST").toString();

                        //System.out.println("the String is Array:"+op);
                        String reString = bill.toString();
                        //System.out.println(reString.substring(reString.indexOf("BILLALLOCATIONS.LIST")-1, reString.length()));
                        JSONArray billlist = bill.getJSONArray("BANKALLOCATIONS.LIST");
                        //System.out.println("ok tasted 3");
                        for (int b = 0; b < billlist.length(); b++) {
                            JSONObject billItems = billlist.getJSONObject(b);
                            // System.out.println("bank" + " " + billItems.get("DATE") + " " + billItems.getString("TRANSACTIONTYPE") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.get("AMOUNT") + " " + billItems.getString("PAYMENTFAVOURING") + " " + billItems.getString("PAYMENTFAVOURING"));

                        }
                        //System.out.println("ok tasted 4");
                        //e.printStackTrace();

                    } else {
                        System.out.println("ok null");
                    }
            }
            listItes.add(new LedgerItem(amount, ledgerName, inventories));
        }

        return listItes;
    }
}