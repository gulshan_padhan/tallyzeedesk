package com.java.tallyzee.DashBoard.Tabs.SupportTab;
import javafx.scene.text.Font;

public class SupportTab {


    public static void setScene(SupportInfo supportInfo)
    {
        Font font = new Font("Roboto",20);
        Font font1 = new Font("Segoe UI",16);
        supportInfo.getSupport().setText("Support");
        supportInfo.getSupport().setFont(font);
        supportInfo.getAddress().setText("COMPANY");
        supportInfo.getAddress().setFont(font1);
        supportInfo.getEmail().setText("Email");
        supportInfo.getEmail().setFont(font1);
        supportInfo.getPhone().setText("Contact");
        supportInfo.getPhone().setFont(font1);
        supportInfo.getAddress().setText("Address");
        supportInfo.getAddress().setFont(font1);
        supportInfo.getEmailOfUSer().setText("info@aiminfocom.com");
        supportInfo.getEmailOfUSer().setFont(font1);
        supportInfo.getPhoneOfuser().setText("+91-8104720621 / +91-9137591492");
        supportInfo.getPhoneOfuser().setFont(font1);
        supportInfo.getAddressOfUser().setFont(font1);
        supportInfo.getAddressOfUser().setText("12B & 12C, Mehta Chamber, Dana Bunder, Musjid Bundar (East), Kalyan St, Dana Bandar, Mandvi, Mumbai, Maharashtra 400009");
        supportInfo.getAddressOfUser().setWrapText(true);
        supportInfo.getNameOfCompany().setFont(font1);
        supportInfo.getNameOfCompany().setText("Aim Infocom Services Pvt. Ltd.");
    }


}
