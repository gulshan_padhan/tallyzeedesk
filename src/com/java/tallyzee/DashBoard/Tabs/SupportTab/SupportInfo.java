package com.java.tallyzee.DashBoard.Tabs.SupportTab;
import javafx.scene.control.Label;
public class SupportInfo {


   private Label support;
    private Label company;
    private Label email;
    private Label phone;
    private Label address;
    private Label emailOfUSer;
    private Label phoneOfuser;
    private  Label addressOfUser;
    private  Label nameOfCompany;

    public SupportInfo(Label support, Label company, Label email, Label phone, Label address, Label emailOfUSer, Label phoneOfuser, Label addressOfUser, Label nameOfCompany) {
        this.support = support;
        this.company = company;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.emailOfUSer = emailOfUSer;
        this.phoneOfuser = phoneOfuser;
        this.addressOfUser = addressOfUser;
        this.nameOfCompany = nameOfCompany;
    }

    public Label getSupport() {
        return support;
    }

    public void setSupport(Label support) {
        this.support = support;
    }

    public Label getCompany() {
        return company;
    }

    public void setCompany(Label company) {
        this.company = company;
    }

    public Label getEmail() {
        return email;
    }

    public void setEmail(Label email) {
        this.email = email;
    }

    public Label getPhone() {
        return phone;
    }

    public void setPhone(Label phone) {
        this.phone = phone;
    }

    public Label getAddress() {
        return address;
    }

    public void setAddress(Label address) {
        this.address = address;
    }

    public Label getEmailOfUSer() {
        return emailOfUSer;
    }

    public void setEmailOfUSer(Label emailOfUSer) {
        this.emailOfUSer = emailOfUSer;
    }

    public Label getPhoneOfuser() {
        return phoneOfuser;
    }

    public void setPhoneOfuser(Label phoneOfuser) {
        this.phoneOfuser = phoneOfuser;
    }

    public Label getAddressOfUser() {
        return addressOfUser;
    }

    public void setAddressOfUser(Label addressOfUser) {
        this.addressOfUser = addressOfUser;
    }

    public Label getNameOfCompany() {
        return nameOfCompany;
    }

    public void setNameOfCompany(Label nameOfCompany) {
        this.nameOfCompany = nameOfCompany;
    }
}
