package com.java.tallyzee.DashBoard.Tabs.Profile;

import com.jfoenix.controls.JFXButton;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
public class ProfileInfo {

   private Label Logo;
   private Label User_name;
    private  Label administrator;
    private Label nameLabel;
    private Label IDLabel;
    private Label emailLabel;
    private Label contactLabel;
    private Label userNameLabel;
    private Label userIdLabel;
    private Label userEmailLabel;
    private Label userPhoneLabel;
    private Button logoutButton;

    public ProfileInfo(Label logo, Label user_name, Label administrator, Label nameLabel, Label IDLabel, Label emailLabel, Label contactLabel, Label userNameLabel, Label userIdLabel, Label userEmailLabel, Label userPhoneLabel, Button logoutButton) {
        this.Logo = logo;
        this.User_name = user_name;
        this.administrator = administrator;
        this.nameLabel = nameLabel;
        this.IDLabel = IDLabel;
        this.emailLabel = emailLabel;
        this.contactLabel = contactLabel;
        this.userNameLabel = userNameLabel;
        this.userIdLabel = userIdLabel;
        this.userEmailLabel = userEmailLabel;
        this.userPhoneLabel = userPhoneLabel;
        this.logoutButton = logoutButton;
    }

    public Label getLogo() {
        return Logo;
    }

    public void setLogo(Label logo) {
        Logo = logo;
    }

    public Label getUser_name() {
        return User_name;
    }

    public void setUser_name(Label user_name) {
        User_name = user_name;
    }

    public Label getAdministrator() {
        return administrator;
    }

    public void setAdministrator(Label administrator) {
        this.administrator = administrator;
    }

    public Label getNameLabel() {
        return nameLabel;
    }

    public void setNameLabel(Label nameLabel) {
        this.nameLabel = nameLabel;
    }

    public Label getIDLabel() {
        return IDLabel;
    }

    public void setIDLabel(Label IDLabel) {
        this.IDLabel = IDLabel;
    }

    public Label getEmailLabel() {
        return emailLabel;
    }

    public void setEmailLabel(Label emailLabel) {
        this.emailLabel = emailLabel;
    }

    public Label getContactLabel() {
        return contactLabel;
    }

    public void setContactLabel(Label contactLabel) {
        this.contactLabel = contactLabel;
    }

    public Label getUserNameLabel() {
        return userNameLabel;
    }

    public void setUserNameLabel(Label userNameLabel) {
        this.userNameLabel = userNameLabel;
    }

    public Label getUserIdLabel() {
        return userIdLabel;
    }

    public void setUserIdLabel(Label userIdLabel) {
        this.userIdLabel = userIdLabel;
    }

    public Label getUserEmailLabel() {
        return userEmailLabel;
    }

    public void setUserEmailLabel(Label userEmailLabel) {
        this.userEmailLabel = userEmailLabel;
    }

    public Label getUserPhoneLabel() {
        return userPhoneLabel;
    }

    public void setUserPhoneLabel(Label userPhoneLabel) {
        this.userPhoneLabel = userPhoneLabel;
    }

    public Button getLogoutButton() {
        return logoutButton;
    }

    public void setLogoutButton(Button logoutButton) {
        this.logoutButton = logoutButton;
    }
}
