package com.java.tallyzee.DashBoard.Tabs.Profile;

import com.java.tallyzee.Database.DatabaseConfig;
import com.java.tallyzee.Database.UserConnection;
import com.java.tallyzee.Session.Session;
import com.java.tallyzee.StageLoader.LoginStage;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.Window;
import com.java.tallyzee.LoginCntr.AlertHelper;
import com.java.tallyzee.LoginCntr.FirebaseAuthUtil;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Optional;

public class ProfileTab  implements com.java.tallyzee.DashBoard.Tabs.Profile.profileInterface {
Window window;
    com.java.tallyzee.DashBoard.Tabs.Profile.ProfileInfo profileInfo;
    public ProfileTab(Window window)
    {
        this.window=window;
    }

    public  void getName(com.java.tallyzee.DashBoard.Tabs.Profile.ProfileInfo profileInfo) throws MalformedURLException, InterruptedException {
this.profileInfo=profileInfo;
        FirebaseAuthUtil firebaseAuthUtil=new FirebaseAuthUtil(this);
        firebaseAuthUtil.getData();
        Font font = new Font("Segoe UI",20);
        Font font1 = new Font("Segoe UI",13);
        Font font2 = new Font("Segoe UI",16);

        Image profileimage = new Image(Session.getProfileUri());
        ImageView profileimageView = new ImageView(profileimage);
        profileimageView.setFitHeight(55);
        profileimageView.setFitWidth(55);
        profileInfo.getLogo().setGraphic(profileimageView);
        profileInfo.getUser_name().setFont(font);
        profileInfo.getUserNameLabel().setFont(font2);
        profileInfo.getUserIdLabel().setFont(font2);
        profileInfo.getUserEmailLabel().setFont(font2);
        profileInfo.getUserPhoneLabel().setFont(font2);
        profileInfo.getAdministrator().setText("Administrator");
        profileInfo.getAdministrator().setFont(font1);
        profileInfo.getNameLabel().setText("Name");
        profileInfo.getNameLabel().setFont(font2);
        profileInfo.getIDLabel().setFont(font2);
        profileInfo.getIDLabel().setText("UserID");
        profileInfo.getEmailLabel().setText("Email");
        profileInfo.getEmailLabel().setFont(font2);
        profileInfo.getContactLabel().setText("Contact");
        profileInfo.getContactLabel().setFont(font2);

       // ArrayList<String> list = getData();
//        Thread.currentThread().sleep(300);


        profileInfo.getLogoutButton().setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Confirmation Dialog");

                alert.setContentText("Are You Want Sure Logout?");

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK){

                    UserConnection.dropTable(DatabaseConfig.getConnection());
                    try{
                        LoginStage.loadLoginStage();
                        Stage loginStage = (Stage) profileimageView.getScene().getWindow();
                        loginStage.close();
                    }catch (Exception e)

                    {

                    }

                    // ... user chose OK
                } else {
                    // ... user chose CANCEL or closed the dialog
                }
               // AlertHelper.showAlert(Alert.AlertType.INFORMATION,window,"Are You Want Sure Logout");

            }
        });



    }


    @Override
    public void getProfileResponse(ArrayList<String> list) {
        profileInfo.getUser_name().setText(list.get(0));
        profileInfo.getUserNameLabel().setText(list.get(0));
        profileInfo.getUserEmailLabel().setText(list.get(1));
        profileInfo.getUserIdLabel().setText(list.get(3));
        System.out.println(list.get(4));
        profileInfo.getUserPhoneLabel().setText("+91-"+list.get(2));
          }
}
