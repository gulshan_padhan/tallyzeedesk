package com.java.tallyzee.DashBoard.Tabs.Tutorial;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;


public class TutorialController {

    @FXML
    Button nextOne;

    @FXML
    Button nextTwo;

    @FXML
    Button doneThree;

    @FXML
    Button previousThree;

    @FXML
    Button previousTwo;
    @FXML
    Tab tabOne;
    @FXML
    Tab tabTwo;
    @FXML
    Tab tabThree;

    @FXML
    TabPane tabPane;

    @FXML
    public void initialize()
    {

    }


    public void nextOneClicked()
    {
        System.out.println("CLICKED");
        tabPane.getSelectionModel().select(1);
    }

    @FXML
    public void nextTwoClicked()
    {
        tabPane.getSelectionModel().select(2);
    }

    public void previousTwoClicked()
    {
        tabPane.getSelectionModel().select(0);
    }

    public void doneThreeClicked()
    {
        Stage stage = (Stage) doneThree.getScene().getWindow();
        stage.close();
    }

    public void previousThreeClicked()
    {
        tabPane.getSelectionModel().select(1);
    }

}
