package com.java.tallyzee.DashBoard.Tabs.AboutTab;
import javafx.scene.control.Label;
public class AboutInfo {


    private Label SystemInfoLabel;
    private Label operatingLabel;
   private Label archLabel;
   private Label hostLabel;
   private Label osLabel;
   private Label processorLabel;
   private Label speedLabel;
   private Label cores;
    private Label ram;
    private Label userOsLabel;
   private Label UserArchLabel;
    private Label userNameLabel;
   private Label userOsReleaseLabel;
   private Label userSysProcessorLabel;
   private Label userSysSpeedLabel;
    private Label UserSysCoresLabel;
   private Label UserSysRam;


    public AboutInfo(Label systemInfoLabel, Label operatingLabel, Label archLabel, Label hostLabel, Label osLabel, Label processorLabel, Label speedLabel, Label cores, Label ram, Label userOsLabel, Label userArchLabel, Label userNameLabel, Label userOsReleaseLabel, Label userSysProcessorLabel, Label userSysSpeedLabel, Label userSysCoresLabel, Label userSysRam) {
        SystemInfoLabel = systemInfoLabel;
        this.operatingLabel = operatingLabel;
        this.archLabel = archLabel;
        this.hostLabel = hostLabel;
        this.osLabel = osLabel;
        this.processorLabel = processorLabel;
        this.speedLabel = speedLabel;
        this.cores = cores;
        this.ram = ram;
        this.userOsLabel = userOsLabel;
        UserArchLabel = userArchLabel;
        this.userNameLabel = userNameLabel;
        this.userOsReleaseLabel = userOsReleaseLabel;
        this.userSysProcessorLabel = userSysProcessorLabel;
        this.userSysSpeedLabel = userSysSpeedLabel;
        UserSysCoresLabel = userSysCoresLabel;
        UserSysRam = userSysRam;
    }

    public void setSystemInfoLabel(Label SystemInfoLabel)
    {
        this.SystemInfoLabel=SystemInfoLabel;
    }

    public Label getSystemInfoLabel()
    {
        return SystemInfoLabel;
    }


    public Label getOperatingLabel() {
        return operatingLabel;
    }

    public void setOperatingLabel(Label operatingLabel) {
        this.operatingLabel = operatingLabel;
    }

    public Label getArchLabel() {
        return archLabel;
    }

    public void setArchLabel(Label archLabel) {
        this.archLabel = archLabel;
    }

    public Label getHostLabel() {
        return hostLabel;
    }

    public void setHostLabel(Label hostLabel) {
        this.hostLabel = hostLabel;
    }

    public Label getOsLabel() {
        return osLabel;
    }

    public void setOsLabel(Label osLabel) {
        this.osLabel = osLabel;
    }

    public Label getProcessorLabel() {
        return processorLabel;
    }

    public void setProcessorLabel(Label processorLabel) {
        this.processorLabel = processorLabel;
    }

    public Label getSpeedLabel() {
        return speedLabel;
    }

    public void setSpeedLabel(Label speedLabel) {
        this.speedLabel = speedLabel;
    }

    public Label getCores() {
        return cores;
    }

    public void setCores(Label cores) {
        this.cores = cores;
    }

    public Label getRam() {
        return ram;
    }

    public void setRam(Label ram) {
        this.ram = ram;
    }

    public Label getUserOsLabel() {
        return userOsLabel;
    }

    public void setUserOsLabel(Label userOsLabel) {
        this.userOsLabel = userOsLabel;
    }

    public Label getUserArchLabel() {
        return UserArchLabel;
    }

    public void setUserArchLabel(Label userArchLabel) {
        UserArchLabel = userArchLabel;
    }

    public Label getUserNameLabel() {
        return userNameLabel;
    }

    public void setUserNameLabel(Label userNameLabel) {
        this.userNameLabel = userNameLabel;
    }

    public Label getUserOsReleaseLabel() {
        return userOsReleaseLabel;
    }

    public void setUserOsReleaseLabel(Label userOsReleaseLabel) {
        this.userOsReleaseLabel = userOsReleaseLabel;
    }

    public Label getUserSysProcessorLabel() {
        return userSysProcessorLabel;
    }

    public void setUserSysProcessorLabel(Label userSysProcessorLabel) {
        this.userSysProcessorLabel = userSysProcessorLabel;
    }

    public Label getUserSysSpeedLabel() {
        return userSysSpeedLabel;
    }

    public void setUserSysSpeedLabel(Label userSysSpeedLabel) {
        this.userSysSpeedLabel = userSysSpeedLabel;
    }

    public Label getUserSysCoresLabel() {
        return UserSysCoresLabel;
    }

    public void setUserSysCoresLabel(Label userSysCoresLabel) {
        UserSysCoresLabel = userSysCoresLabel;
    }

    public Label getUserSysRam() {
        return UserSysRam;
    }

    public void setUserSysRam(Label userSysRam) {
        UserSysRam = userSysRam;
    }
}
