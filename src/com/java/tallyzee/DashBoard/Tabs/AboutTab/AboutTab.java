package com.java.tallyzee.DashBoard.Tabs.AboutTab;

import javafx.scene.text.Font;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;

public class AboutTab {


    private static ArrayList<String> getSystemInfo()
    {
        ArrayList<String> builder = new ArrayList<>();
        String windows = System.getProperty("os.name");
       builder.add(windows);
        String arch = System.getProperty("os.arch");
        builder.add(arch);
        String host = System.getProperty("user.name");
        builder.add(host);
        String os = System.getProperties().getProperty("os.version");
        builder.add(os);
        String processor = System.getenv("PROCESSOR_ARCHITEW6432");
        builder.add(processor);
        String processor_speed =  String.valueOf(((com.sun.management.OperatingSystemMXBean) ManagementFactory
                .getOperatingSystemMXBean()).getProcessCpuLoad());
        builder.add(processor_speed);
        String cores = System.getenv("NUMBER_OF_PROCESSORS");
        builder.add(cores);
        String ram = String.valueOf(((com.sun.management.OperatingSystemMXBean) ManagementFactory
                .getOperatingSystemMXBean()).getTotalPhysicalMemorySize());
        builder.add(ram);
        return builder;
    }


    public static  void setAboutScene(AboutInfo aboutInfo)
    {

        Font font = new Font("Roboto",20);
        Font font1 = new Font("Segoe UI",14);

        aboutInfo.getSystemInfoLabel().setText("System Information");
        aboutInfo.getSystemInfoLabel().setFont(font);
        aboutInfo.getOperatingLabel().setText("Operating System");
        aboutInfo.getOperatingLabel().setFont(font1);
        aboutInfo.getArchLabel().setText("Architecture");
        aboutInfo.getArchLabel().setFont(font1);
        aboutInfo.getHostLabel().setText("Host Name");
        aboutInfo.getHostLabel().setFont(font1);
        aboutInfo.getOsLabel().setText("OS Release");
        aboutInfo.getOsLabel().setFont(font1);
        aboutInfo.getProcessorLabel().setText("Processor");
        aboutInfo.getProcessorLabel().setFont(font1);
        aboutInfo.getSpeedLabel().setText("Processor Speed");
        aboutInfo.getSpeedLabel().setFont(font1);
        aboutInfo.getCores().setText("Cores");
        aboutInfo.getCores().setFont(font1);
        aboutInfo.getRam().setText("Ram");
        aboutInfo.getRam().setFont(font1);
        aboutInfo.getUserOsLabel().setFont(font1);
        aboutInfo.getUserArchLabel().setFont(font1);
        aboutInfo.getUserNameLabel().setFont(font1);
        aboutInfo.getUserOsReleaseLabel().setFont(font1);
        aboutInfo.getUserSysProcessorLabel().setFont(font1);
        aboutInfo.getUserSysSpeedLabel().setFont(font1);
        aboutInfo.getUserSysCoresLabel().setFont(font1);
        aboutInfo.getUserSysRam().setFont(font1);

        ArrayList<String> arrayList = getSystemInfo();
        aboutInfo.getUserOsLabel().setText(arrayList.get(0));
        aboutInfo.getUserArchLabel().setText(arrayList.get(1));
        aboutInfo.getUserNameLabel().setText(arrayList.get(2));
        aboutInfo.getUserOsReleaseLabel().setText(arrayList.get(3));
        aboutInfo.getUserSysProcessorLabel().setText(arrayList.get(4));
        aboutInfo.getUserSysSpeedLabel().setText(arrayList.get(5));
        aboutInfo.getUserSysCoresLabel().setText(arrayList.get(6));
        aboutInfo.getUserSysRam().setText(arrayList.get(7)+"GB");





    }


}
