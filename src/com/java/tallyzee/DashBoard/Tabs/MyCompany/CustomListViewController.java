package com.java.tallyzee.DashBoard.Tabs.MyCompany;

import com.jfoenix.controls.JFXButton;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import javax.swing.*;
import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class CustomListViewController {

@FXML
VBox vbox;

@FXML
VBox vbox2;
    HashSet<String> hashSet;

    public void initialize() throws MalformedURLException {
        List<Label> arrayList = new ArrayList<>();
         hashSet =  MyCompanyTab.getCompanyNames();
//        String name1 = "";
//        hashSet.forEach((res)->{companyName.setText(res);});
//        Date date = new Date();
//        lastSync.setText("Last Sync "+date.toString().substring(0,15));
//        Image image = new Image(new File("src/res/deleteCompany.png").toURL().toString());
//        deleteCompanyImageView.setImage(image);
//        deleteCompanyImageView.setFitWidth(20);
//        deleteCompanyImageView.setFitHeight(20);
        hashSet.forEach((res)->{ setItem(res);});
    }

    public void setCurrent(String current)
    {

    }
void setItem(String response)
{
    Label label = new Label(response);
    label.setStyle("-fx-text-fill:#964178");
    label.setFont(new Font("Segoe UI",20));
    Label label2 = new Label("Last Sync a Month Ago");
    label2.setStyle("-fx-text-fill:#964178");
    label2.setFont(new Font("Segoe UI",14));
    vbox.getChildren().addAll(label,label2);
    vbox.setSpacing(20);

   Button button = new Button("Sync");
    button.setStyle("-fx-text-fill:#964178");
    button.setFont(new Font("Segoe UI",18));
    Image image = null;
    try {
        image = new Image(new File("src/res/deleteCompany.png").toURL().toString());
    } catch (MalformedURLException e) {
        e.printStackTrace();
    }
//    final Slider slider = new Slider();
//    slider.setMin(0);
//    slider.setMax(50);

  //  final ProgressBar pb = new ProgressBar(0);
    final ProgressIndicator pi = new ProgressIndicator(0);

   // pi.setProgress(Double.valueOf(i)/50);

//    slider.valueProperty().addListener(new ChangeListener<Number>() {
//        public void changed(ObservableValue<? extends Number> ov,
//                            Number old_val, Number new_val) {
//            pb.setProgress(new_val.doubleValue()/50);
//            pi.setProgress(new_val.doubleValue()/50);
//        }
//    });


    ImageView imageView = new ImageView(image);
    imageView.setFitWidth(20);
    imageView.setFitHeight(20);
    vbox2.getChildren().addAll(pi,imageView,button);

    vbox2.setSpacing(20);
}
}
