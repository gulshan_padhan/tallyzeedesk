package com.java.tallyzee.DashBoard.Tabs.MyCompany;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import com.java.tallyzee.Network.NumberofCompanys;

import java.util.HashSet;
public class MyCompanyTab {
    public static String getStartDate(String companyName)
    {
        //"<ENVELOPE>\n\n<HEADER>\n\n<VERSION>1</VERSION>\n\n<TALLYREQUEST>EXPORT</TALLYREQUEST>\n\n<TYPE>OBJECT</TYPE> <SUBTYPE>Company</SUBTYPE> <ID TYPE=\"Name\">Test</ID>\n\n</HEADER>\n\n<BODY>\n\n<DESC>\n\n<STATICVARIABLES><SVFROMDATE TYPE=\"Date\">1-4-2010</SVFROMDATE>   <SVTODATE TYPE=\"Date\">31-3-2022</SVTODATE><SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT></STATICVARIABLES>\n\n<FETCHLIST>\n\n<FETCH>Name</FETCH>\n\n<FETCH>TNetBalance</FETCH>\n\n<FETCH>Parent</FETCH>\n\n</FETCHLIST>\n\n<TDL>\n\n<TDLMESSAGE>\n\n<OBJECT NAME=\"Company\" ISINITIALIZE=\"Yes\">\n\n<LOCALFORMULA>\n\nTNetBalance: $BooksFrom\n\n</LOCALFORMULA>\n\n</OBJECT>\n\n</TDLMESSAGE>\n\n</TDL>\n\n</DESC>\n\n</BODY>\n\n</ENVELOPE>"
        return "";
    }

    public static HashSet<String> getCompanyNames()
    {
        HashSet<String> hashSet = new HashSet<>();
        String list_of_companies = NumberofCompanys.getCompanyNames();
        String payroll = NumberofCompanys.getResponse(list_of_companies);
        System.out.println(payroll);
        try {
            JSONObject main = XML.toJSONObject(payroll);
            System.out.println(main.toString());
            JSONObject evn = main.getJSONObject("ENVELOPE");
            JSONObject companylist = evn.getJSONObject("COMPANYNAME.LIST");
            System.out.println(companylist);
            if(String.valueOf(companylist.get("COMPANYNAME")).equals(""))
            {
                System.out.println("EMPTY");
            }
             else  if(String.valueOf(companylist.get("COMPANYNAME")).charAt(0)=='[') {
                JSONArray companyname = companylist.getJSONArray("COMPANYNAME");
                for (int i = 0; i < companyname.length(); i++) {
                    String value = String.valueOf(companyname.get(i));
                    hashSet.add(value);
                }
            }
            else
            {
                String company = String.valueOf(companylist.get("COMPANYNAME"));
                hashSet.add(company);
            }

            }catch (Exception en)
        {
            System.out.println(en.getMessage());
        }
        return hashSet;
    }
}
