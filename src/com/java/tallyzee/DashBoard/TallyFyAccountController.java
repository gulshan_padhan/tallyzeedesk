package com.java.tallyzee.DashBoard;


import com.java.tallyzee.DashBoard.Tabs.Profile.ProfileInfo;
import com.java.tallyzee.Network.NumberofCompanys;
import com.java.tallyzee.Network.ParserController;
import com.java.tallyzee.Session.Session;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Window;
import com.java.tallyzee.DashBoard.NetworkStatus.Status;
import com.java.tallyzee.DashBoard.Tabs.AboutTab.AboutInfo;
import com.java.tallyzee.DashBoard.Tabs.AboutTab.AboutTab;
import com.java.tallyzee.DashBoard.Tabs.AddCompanyTab;
import com.java.tallyzee.DashBoard.Tabs.MyCompany.MyCompanyTab;
import com.java.tallyzee.DashBoard.Tabs.Profile.ProfileTab;
import com.java.tallyzee.DashBoard.Tabs.SupportTab.SupportInfo;
import com.java.tallyzee.DashBoard.Tabs.SupportTab.SupportTab;
import com.java.tallyzee.StageLoader.TutorialStage;
import com.java.tallyzee.Utility.AppParserUtil;
import javafx.util.Callback;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;

public class TallyFyAccountController {


    class ProgressCallBack implements ProgressInterface
    {

        @Override
        public void getCallBack(boolean b) {
        }
    }
    @FXML
    TextField edtIpAddress;
    @FXML
    TextField edtIpPort;
    @FXML
    Button btnSave;
    @FXML
    TextField edtTime;
    @FXML
    Button saveTime;


    @FXML
    Label topLogo;
    @FXML
    Label logoutLabel;
    @FXML
    Label userEmailLabel;
    String time;
    @FXML
    Label Logo;
    @FXML
    Label title;

    @FXML
    Label nameLabel;

    @FXML
    Label minimizeLabel;

    @FXML
    Label closeLabel;

    @FXML
    Label name_of_company;

    @FXML
    Tab myCompanyTab;

    @FXML
    TabPane tabPane;

    @FXML
    Tab addCompanyTab;

    @FXML
    Tab settingTab;

    @FXML
    Tab profileTab;

    @FXML
    StackPane anchorPane;
    @FXML
    Tab aboutTab;

    @FXML
    Label IDLabel;
    @FXML
    Label contactLabel;

    @FXML
    Label userOs;

    @FXML
    Tab tutorialTab;

    @FXML
    Tab supportTab;

    @FXML
    Label userPhoneLabel;


    @FXML
    Label companynotSync;

    static int flag;


    static HashSet<String> mList = new HashSet<>();


    @FXML
    Label timeZone;


    @FXML
    Label userNameLabel;

    @FXML
    Label operating;

    @FXML
    Label arch;

    @FXML
    Label host;

    @FXML
    Label os;

    @FXML
    Label processor;
    @FXML
    Label speed;

    @FXML
    Label cores;

    @FXML
    Label ram;

    @FXML
    Tab tallyTab;

    @FXML
    Label connect;

    @FXML
    Label tallyConfig;

    @FXML
    Label click;

    @FXML
    Label help;

    @FXML
    Label phone;
    private Service<Void> bacgroundServices;

    @FXML
    Label emailOfUser;

    @FXML
    Label administrator;


    @FXML
    Label User_name;


    @FXML
    Label Support;

    @FXML
    Label Comapny;

    @FXML
    Label emailLabel;

    @FXML
    Label companyAddress;

    @FXML
    Label address;

    @FXML
    static Label email;
    @FXML
    Label contact;

    @FXML
    Label userSysProcessor;

    @FXML
    Label userOsRelease;

    @FXML
    Label userName;

    @FXML
    Label UserArch;

    @FXML
    Label userIdLabel;

    String ip = "localhost";
    String port = "9000";

    @FXML
    Label UserSysCores;
    @FXML
    Label UserSysRam;
    @FXML
    Label SystemInfo;

    @FXML
    Label userSysSpeed;

    @FXML
    Label internetStatus;
    @FXML
    Label tallyStatus;

    @FXML
    ImageView internetImageView;

    @FXML
    Label logo2;

    @FXML
    ImageView tallyImageView;

    @FXML
    ComboBox comboBox;

    @FXML
    ListView listView;

    @FXML
    Button logoutButton;
    static HashSet<String> filterList = new HashSet<>();


    @FXML
    public void initialize() throws MalformedURLException {
        Font font = new Font("Roboto", 16);


        //Pop-up Window

        //TopLogoImage
        Image topLogoImage = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\stageLogo.png").toURL().toString());
        ImageView topLogoImageView = new ImageView(topLogoImage);
        topLogoImageView.setFitWidth(20);
        topLogoImageView.setFitHeight(20);
        topLogo.setGraphic(topLogoImageView);

        //Logout Button
        Image logoutImage = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\logout.png").toURL().toString());
        ImageView logoutImageView = new ImageView(logoutImage);
        logoutImageView.setFitWidth(15);
        logoutImageView.setFitHeight(15);
        logoutLabel.setGraphic(logoutImageView);

//"res/resources/
        //Minimize Button
        Image minimizeImage = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\Minimize.png").toURL().toString());
        ImageView minimizeImageView = new ImageView(minimizeImage);
        minimizeImageView.setFitWidth(15);
        minimizeImageView.setFitHeight(15);
        minimizeLabel.setGraphic(minimizeImageView);


        //
        Image manImage = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\man.png").toURL().toString());
        ImageView manImageView = new ImageView(manImage);
        manImageView.setFitWidth(20);
        manImageView.setFitHeight(20);
        logo2.setGraphic(manImageView);

        //Close Button
        Image closeImage = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\Close.png").toURI().toString());
        ImageView closeImageView = new ImageView(closeImage);
        closeImageView.setFitWidth(13);
        closeImageView.setFitHeight(13);
        closeLabel.setGraphic(closeImageView);

        //setting font
        title.setFont(font);
        myCompanyTab.setStyle("-fx-background-color:#964178; -fx-foreground-color:#ffffff");
        tabPane.setStyle("-fx-background-color: #964178");


        myCompanyTab.setStyle("-fx-background-color: #964178");

        Status.statusInfo(tallyStatus, internetStatus, tallyImageView, internetImageView);
        //Status Checking
//        Platform.runLater(new Runnable() {
//            @Override
//            public void run() {
//                for(int i=0;i<500;i++)
//                {
//                    try {
//                        Thread.sleep(5000);
//                        Status.statusInfo(tallyStatus, internetStatus, tallyImageView, internetImageView);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    } catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            }
//        });


        tabPane.setStyle("-fx-font-size:14pt");
        tabPane.setStyle("-fx-background-color:#fff");

        //ProfilePic
//

//        initializeTextSize();
        myCompanyTab.setStyle("-fx-background-color:#E6D1DF");
//

    }

    public void startSync() {

    }

    @FXML
    public void handleEditPort(Event event) {
    }

    @FXML
    public void handleTime(Event event) {


    }

    public void SelectAll(ActionEvent actionEvent) {
        ObservableList<String> list = FXCollections.observableArrayList();
        if (mList.size() > 0)
            for (String s : mList) {
                list.add(s);
            }
        ListView<String> lv = new ListView<>(list);
        lv.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new XCell(1);
            }
        });
        // lv.getItems().get(0).
        anchorPane.getChildren().add(lv);
        flag = 0;
    }

    public void DeSelectAll(ActionEvent actionEvent) {
        ObservableList<String> list = FXCollections.observableArrayList();
        if (mList.size() > 0)
            for (String s : mList) {
                list.add(s);
            }
        ListView<String> lv = new ListView<>(list);
        lv.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new XCell(0);
            }
        });
        flag = 0;
        anchorPane.getChildren().add(lv);
    }



    static class XCell extends ListCell<String> {
        HBox hbox = new HBox();
        Label label = new Label("(empty)");
        Pane pane = new Pane();
        //        Button button = new Button("(>)");
        CheckBox c = new CheckBox();
        //        ProgressBar pr=new ProgressBar();
////
//ProgressBar pb = new ProgressBar(0.6);
        ProgressIndicator pi = new ProgressIndicator(1);
        int count = 0;
        String lastItem;

        public XCell(int i) {
            count = i;
            check();
        }

        public XCell(String s) {

            //syncLite();
        }

        public XCell() {
            super();
            check();

        }
//        void  syncLite()
//        {
//         for(String s:filterList)
//         {
//             if(s.equalsIgnoreCase(lastItem))
//             {
//                 HashSet<String> list = new HashSet<>();
//c.setSelected(true);
//                 c.setStyle("-fx-font-size:18;-fx-text-fill: #964178;");
//                 label.setStyle("-fx-font-size:14;-fx-text-fill: #964178;");
//                 pi.setStyle("-fx-font-size:12;-fx-text-fill: #964178;");
//                 hbox.getChildren().addAll(label, pane, c, pi);
//                 HBox.setHgrow(pane, Priority.ALWAYS);
//
//                 EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
//
//                     public void handle(ActionEvent e) {
//                         if (c.isSelected()) {
//                             if(flag==1)
//                             {
//                                 pi.setVisible(true);
//                             }
//                             filterList.add(lastItem);
//                             System.out.println("select");
//                         } else {
//                             pi.setVisible(false);
//                             filterList.remove(lastItem);
//                             System.out.println(" not select");
//                         }
//
//                     }
//
//                 };
//                 c.setOnAction(event);
//             }
//         }
//
//        }
int itrt=0;
        public Task createWorker() {
            return new Task() {
                @Override
                protected Object call() throws Exception {
                    for (int i = 0; i < 97; i++) {
                        Thread.sleep(1000);
                        updateMessage("2000 milliseconds");
                        updateProgress(i , 100);
                    }
                    return true;
                }
            };
        }
        Task copyWorker;
        void check() {

            HashSet<String> list = new HashSet<>();
            if (count == 1) {
                filterList = new HashSet<>(mList);
                c.setSelected(true);
                pi.setVisible(false);
            } else if (count == 2) {
                c.setSelected(true);
                pi.setVisible(false);
            } else {
                filterList = new HashSet<>();
                c.setSelected(false);
                pi.setVisible(false);
            }
            c.setStyle("-fx-font-size:18;-fx-text-fill: #964178;");
            label.setStyle("-fx-font-size:14;-fx-text-fill: #964178;");
            pi.setStyle("-fx-font-size:12;-fx-text-fill: #964178;");
            hbox.getChildren().addAll(label, pane, c, pi);
            HBox.setHgrow(pane, Priority.ALWAYS);

            EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {

                public void handle(ActionEvent e) {
                    if (c.isSelected()) {
                        TallyFyAccountController tr = new TallyFyAccountController();
                        tr.SyncData(lastItem);


                        pi.setProgress(0);

                        copyWorker = createWorker();
                        pi.progressProperty().unbind();
                        pi.progressProperty().bind(copyWorker.progressProperty());
                        copyWorker.messageProperty().addListener(new ChangeListener<String>() {
                            public void changed(ObservableValue<? extends String> observable,
                                                String oldValue, String newValue) {
                                System.out.println(newValue);
                            }
                        });
                        new Thread(copyWorker).start();


//                        Platform.runLater(()->{ new Thread(()->{
//                            for (float i = 0.01f; i < 10; i++) {
//                                final float position = i;
//                                Platform.runLater(() -> {
//                                    try {
//                                        Thread.sleep(3000);
//                                    } catch (InterruptedException ex) {
//                                        ex.printStackTrace();
//                                    }
//                                    pi.setProgress(position);
//
//                                    System.out.println("Index: " + position);
//                                });
//
//                            }
//                        });});


                        new Thread(() -> {
                            ParserController parserController=new ParserController(new ProgressInterface() {
                                @Override
                                public void getCallBack(boolean b) {
                                   // i=1;
                                    pi.setProgress(100);
                                }
                            }, Session.getEmail(), lastItem);


                        }).start();
                        pi.setVisible(true);
                        filterList.add(lastItem);
                        System.out.println("select");
                    } else {
                        pi.setVisible(false);
                        filterList.remove(lastItem);
                        System.out.println(" not select");
                    }

                }

            };
            c.setOnAction(event);
//            button.setOnAction(new EventHandler<ActionEvent>() {
//                @Override
//                public void handle(ActionEvent event) {
//                    System.out.println(lastItem + " : " + event);
//                }
//            });
        }

        public void select(ProgressBar pb) {
            pb.setVisible(true);
        }

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            setText(null);  // No text in label of super class
            if (empty) {
                lastItem = null;
                setGraphic(null);
            } else {
                lastItem = item;
                label.setText(item != null ? item : "<null>");
                setGraphic(hbox);
            }
        }
    }

    void SyncData(String name) {

        System.out.println("the name is:" + name+" "+ Session.getEmail());
//        ParserController.
//                getSync(Session.getEmail(), name);
        new Thread(() -> {

            Platform.runLater(() -> {

                try {
                    System.out.println("the company is" + String.valueOf(email.hashCode()) + " " + name + NumberofCompanys.getCurrentCompany());
                    new ParserController(new ProgressInterface() {
                        @Override
                        public void getCallBack(boolean b) {

                        }
                    },Session.getEmail(), name);
//signInButton.textProperty().bind(bacgroundServices.messa
                } catch (Exception er) {

                }
            });
            try {
                Thread.sleep(100);
            } catch (Exception rer) {
                System.err.println(rer);
            }

        }).start();
        try {
            bacgroundServices = new Service<Void>() {
                @Override
                protected Task<Void> createTask() {
                    return new Task<Void>() {
                        @Override
                        protected Void call() throws Exception {
                            System.out.println("the company is" + String.valueOf(email.hashCode()) + " " + name + NumberofCompanys.getCurrentCompany());
                            new ParserController(new ProgressInterface() {
                                @Override
                                public void getCallBack(boolean b) {

                                }
                            }, Session.getEmail(), name);
                            //                            ParserController.
//                                    getSync(Session.getEmail(), name);


                            return null;
                        }
                    };
                }
            };

            bacgroundServices.setOnScheduled(new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent event) {

                }
            });
            bacgroundServices.restart();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    @FXML
    public void handleEditIp(Event event) {

        port = edtIpPort.getText();
    }

    @FXML
    public void onSaveConfig(Event event) {
        if (edtIpPort.getText() != null && !edtIpPort.getText().isEmpty() && !edtIpPort.getText().equalsIgnoreCase(""))
            AppParserUtil.setIpPort(edtIpPort.getText());

        if (edtIpAddress.getText() != null && !edtIpAddress.getText().isEmpty() && !edtIpAddress.getText().equalsIgnoreCase(""))
            AppParserUtil.setIpAdress(edtIpAddress.getText());
        System.out.println(AppParserUtil.getIpAdress() + " " + AppParserUtil.getIpPort() + " " + AppParserUtil.getYear());
    }

    @FXML
    public void setBtnSave(Event event) {
        if (edtTime.getText() != null && !edtTime.getText().isEmpty() && !edtTime.getText().equalsIgnoreCase(""))
            time = edtTime.getText();
        System.out.println(time);
        System.out.println(time);
//        Platform.runLater(new Runnable() {
//                                      @Override
//                                      public void run() {
//                                          for(int i=0;i<5;i++)
//                                          {
//
//                                              bacgroundServices=new Service<Void>() {
//                                                  @Override
//                                                  protected Task<Void> createTask() {
//                                                      return new Task<Void>() {
//                                                          @Override
//                                                          protected Void call() throws Exception {
//                                                              // System.out.println( "the company is"+NumberofCompanys.getCurrentCompany());
//                                                              ParserController.getObjectSyncStart(String.valueOf(email.hashCode()));
//                                                              return null;
//                                                          }
//                                                      };
//                                                  }
//                                              };
//
//                                              bacgroundServices.setOnScheduled(new EventHandler<WorkerStateEvent>() {
//                                                  @Override
//                                                  public void handle(WorkerStateEvent event) {
//
//                                                  }
//                                              });
////signInButton.textProperty().bind(bacgroundServices.messageProperty());
//                                              bacgroundServices.restart();
//                                              try {
//                                                  Thread.sleep(Integer.valueOf(time)*60000);
//                                              } catch (InterruptedException e) {
//                                                  e.printStackTrace();
//                                              }
//                                          }
//                                      }
//                                  });
//        try {
//            Thread.sleep(Integer.valueOf(time) * 60000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    @FXML
    public void handleCloseLabel(Event event) {
        ((Stage) ((Label) event.getSource()).getScene().getWindow()).close();
    }

    public void handleMinimizeLabel(Event event) {
        ((Stage) ((Label) event.getSource()).getScene().getWindow()).setIconified(true);

    }

    public void HandelTabs() {
        if (myCompanyTab.isSelected()) {
            myCompanyTab.setStyle("-fx-background-color:#E6D1DF");
            HashSet<String> hashSet = MyCompanyTab.getCompanyNames();
            mList = new HashSet<>(hashSet);
            ObservableList<String> list = FXCollections.observableArrayList();
            if (hashSet.size() > 0)
                for (String s : hashSet) {
                    list.add(s);
                }
            ListView<String> lv = new ListView<>(list);
            lv.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
                @Override
                public ListCell<String> call(ListView<String> param) {
                    return new XCell();
                }
            });
            // lv.getItems().get(0).
            anchorPane.getChildren().add(lv);
//            if(hashSet.size()>0)
//            {
//                Node[] nodes = new Node[hashSet.size()];
//                for (int i = 0; i < 1; i++) {
//                    try {
//                        nodes[i] = FXMLLoader.load(getClass().getResource("/com/java/tallyzee/DashBoard/Tabs/MyCompany/customListView.fxml"));
//
//                        anchorPane.getChildren().add(nodes[i]);
//
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//                for(int i=0;i<nodes.length;i++)
//                {
//                    if(anchorPane.getChildren().get(i).getProperties().containsKey(hashSet.iterator()))
//                    {
//
//                    }
//                }
//            }else
//            {
//            }
        } else {
            myCompanyTab.setStyle("-fx-background-color:#964178");
        }


    }

    public void saveConfig() {

    }

    @FXML
    public void handleTabComany() {
        if (addCompanyTab.isSelected()) {
            addCompanyTab.setStyle("-fx-background-color:#E6D1DF");
            AddCompanyTab.getTimeZones(comboBox);
        } else {
            addCompanyTab.setStyle("-fx-background-color:#964178");
        }
    }

    @FXML
    public void handleTabSetting() throws IOException {

        if (settingTab.isSelected()) {
            settingTab.setStyle("-fx-background-color:#E6D1DF");

        } else {
            settingTab.setStyle("-fx-background-color:#964178");
        }
    }

    @FXML
    public void handleUserTab() {

    }

    @FXML
    public void handleTabProfile() throws MalformedURLException, InterruptedException {

        if (profileTab.isSelected()) {
            profileTab.setStyle("-fx-background-color:#E6D1DF");
            Window owner = logoutButton.getScene().getWindow();
            ProfileTab profileTab = new ProfileTab(owner);
            profileTab.getName(new ProfileInfo(Logo, User_name, administrator, nameLabel, IDLabel, emailLabel, contactLabel, userNameLabel,
                    userIdLabel, userEmailLabel, userPhoneLabel, logoutButton));
        } else {
            profileTab.setStyle("-fx-background-color:#964178");
        }
    }

    @FXML
    public void handleTabAbout() {
        if (aboutTab.isSelected()) {
            aboutTab.setStyle("-fx-background-color:#E6D1DF");
            AboutTab.setAboutScene(new AboutInfo(SystemInfo, operating, arch, host, os, processor, speed, cores, ram,
                    userOs, UserArch, userName, userOsRelease, userSysProcessor, userSysSpeed, UserSysCores, UserSysRam));
        } else {
            aboutTab.setStyle("-fx-background-color:#964178");
        }
    }

    @FXML
    public void handleTabTutorial() throws IOException {
        if (tutorialTab.isSelected()) {
            tutorialTab.setStyle("-fx-background-color:#E6D1DF");
            Stage parent_stage = (Stage) topLogo.getScene().getWindow();
            TutorialStage.getTutorialStage(parent_stage);
            System.out.println("Tutorial NExt");

        } else {
            tutorialTab.setStyle("-fx-background-color :#964178");
        }
    }

    @FXML
    public void handleTabSupport() {

        if (supportTab.isSelected()) {
            supportTab.setStyle("-fx-background-color:#E6D1DF");
            SupportTab.setScene(new SupportInfo(Support, Comapny, email, contact, address, emailOfUser, phone, companyAddress, name_of_company));
        } else {
            supportTab.setStyle("-fx-background-color:#964178");
        }
    }

    static int ic = 0;

    public void syncNow(ActionEvent actionEvent) {

        flag=1;
        ObservableList<String> list = FXCollections.observableArrayList();
        if (mList.size() > 0)
            for (String s : mList) {
                list.add(s);
            }
        ArrayList<String> al=new ArrayList<>(mList);
            ArrayList<String> balst=new ArrayList<>(filterList);
        ListView<String> lv = new ListView<>(list);
        ic=0;
        lv.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                if (ic<=balst.size() ) {
                    if (al.get(ic).equalsIgnoreCase(balst.get(ic))) {
                        ic++;
                        return new XCell(2);
                    } else {
                        ic++;
                        return new XCell(0);
                    }

                }

                return new XCell(0);
            }
        });

        anchorPane.getChildren().add(lv);
       if(filterList.size()>0)
       {
         for(String s:filterList)
         {
             System.out.println(s);
         }
       }else {
           System.out.println("null list hash");
       }
        System.out.println("ok click");
        try {
            bacgroundServices = new Service<Void>() {
                @Override
                protected Task<Void> createTask() {
                    return new Task<Void>() {
                        @Override
                        protected Void call() throws Exception {
                            System.out.println("the company is" + NumberofCompanys.getCurrentCompany());
                           // ParserController.getObjectSyncStart(String.valueOf(email.hashCode(),));


                            return null;
                        }
                    };
                }
            };

            bacgroundServices.setOnScheduled(new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent event) {

                }
            });
            bacgroundServices.restart();
//signInButton.textProperty().bind(bacgroundServices.messa
        } catch (Exception e) {

        }
    }
}
