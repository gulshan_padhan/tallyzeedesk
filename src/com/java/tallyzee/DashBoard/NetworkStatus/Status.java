package com.java.tallyzee.DashBoard.NetworkStatus;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;
import com.java.tallyzee.Network.NetworkCall;

import java.io.File;
import java.net.MalformedURLException;

public class Status {
    public static void statusInfo(Label tallyStatus,Label internetStatus,ImageView tallystatusImageView,
                                  ImageView internetstatusImageView) throws MalformedURLException {
        Font font = new Font("Segoe UI", 14);
        tallyStatus.setFont(font);
        internetStatus.setFont(font);
///home/gulshan/Desktop/TallyZeeAppliCationResource
        Image tallyimageonline = new Image(new File("/home/gulshan/Desktop/TallyZeeAppliCationResource/Resource/tallyonline.png").toURL().toString());
        Image tallyimageoffline = new Image(new File("/home/gulshan/Desktop/TallyZeeAppliCationResource/Resource/tallyoffline.png").toURL().toString());
        Image internetimageonline = new Image(new File("/home/gulshan/Desktop/TallyZeeAppliCationResource/Resource/internetonline.png").toURL().toString());
        Image internetimageoffline = new Image(new File("/home/gulshan/Desktop/TallyZeeAppliCationResource/Resource/internetoffline.png").toURL().toString());
        tallystatusImageView.setFitWidth(17);
        tallystatusImageView.setFitHeight(17);
        internetstatusImageView.setFitHeight(17);
        internetstatusImageView.setFitWidth(17);




        if(getTallyStatus())
        {
            tallyStatus.setText("Tally : Connected");
            tallystatusImageView.setImage(tallyimageonline);
        }
        else
        {
            tallyStatus.setText("Tally : Not Connected");
            tallystatusImageView.setImage(tallyimageoffline);
        }
        if(getInternetStatus())
        {
            internetStatus.setText("Internet : Connected");
            internetstatusImageView.setImage(internetimageonline);
        }
        else {
            internetStatus.setText("Internet : Not Connected");
            internetstatusImageView.setImage(internetimageoffline);
        }
    }

    public static boolean getTallyStatus()
    {
        if(NetworkCall.getTallyStatus())
        {
            return true;
        }
        else
        {

            return false;
        }
    }
    public static boolean getInternetStatus()
    {
        if(NetworkCall.getInternetStatus())
        {
            return true;
        }
        else
        {

            return false;
        }
    }
}
