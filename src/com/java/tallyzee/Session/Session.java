package com.java.tallyzee.Session;

public class Session {
    private static String email;
    private static String username;
    private static String phone;
    private static String password;

    public static String getProfileUri() {
        return profileUri;
    }

    public static void setProfileUri(String profileUri) {
        Session.profileUri = profileUri;
    }

    private static String profileUri;

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        Session.email = email;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        Session.username = username;
    }

    public static String getPhone() {
        return phone;
    }

    public static void setPhone(String phone) {
        Session.phone = phone;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        Session.password = password;
    }

    public static String getConfirmpassword() {
        return confirmpassword;
    }

    public static void setConfirmpassword(String confirmpassword) {
        Session.confirmpassword = confirmpassword;
    }

    public static String getCreatedAt() {
        return createdAt;
    }

    public static void setCreatedAt(String createdAt) {
        Session.createdAt = createdAt;
    }

    public static String getCreatedBy() {
        return createdBy;
    }

    public static void setCreatedBy(String createdBy) {
        Session.createdBy = createdBy;
    }

    public static String getUserID() {
        return userID;
    }

    public static void setUserID(String userID) {
        Session.userID = userID;
    }

    public static String getUpdatedAt() {
        return updatedAt;
    }

    public static void setUpdatedAt(String updatedAt) {
        Session.updatedAt = updatedAt;
    }

    public static String getUpdatedBy() {
        return updatedBy;
    }

    public static void setUpdatedBy(String updatedBy) {
        Session.updatedBy = updatedBy;
    }

    public static String getAccountType() {
        return accountType;
    }

    public static void setAccountType(String accountType) {
        Session.accountType = accountType;
    }

    public static String getSuscriptionDate() {
        return suscriptionDate;
    }

    public static void setSuscriptionDate(String suscriptionDate) {
        Session.suscriptionDate = suscriptionDate;
    }

    public Session() {
    }

    private static String confirmpassword;
    private static String createdAt;
    private static String createdBy;
    private static String userID;
    private static String updatedAt;
    private static String updatedBy;
    private static String accountType;
    private static String suscriptionDate;
}
