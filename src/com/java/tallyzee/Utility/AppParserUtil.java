package com.java.tallyzee.Utility;

import org.json.JSONObject;
import org.json.XML;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AppParserUtil {
    public static String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static String[] monthsCount = {"31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"};
    public static String[] daysList = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
    public static String ipAdress="localhost";
    public static String ipPort="9000";

    public static String getCompany_date() {
        return company_date;
    }

    public static void setCompany_date(String company_date) {
        AppParserUtil.company_date = company_date;
    }

    public static String company_date;
    public static String jsonSdk="{\n" +
            "  \"type\": \"service_account\",\n" +
            "  \"project_id\": \"tallyfy-364a0\",\n" +
            "  \"private_key_id\": \"33a288447b6f3359924cd8d54490263dbebcbf45\",\n" +
            "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDmPai8UKW1LvpX\\noqke5gM31smvGxOa0YyxaarS2hcJJaMbaK6RTMTthfLTVcPSSyHxyd6Efo3HsYhD\\n70x3o+vMZOPFT0E/A8hOTApEJtf0/lKGw2YCcmcOBcSaRBJaOp3gfN8wZZOXMdrA\\n5nA5JpMUl8tjZxc6tmukdsUCq5u9eQa5/BebM6WRGnq56kKpXNXvvVrf/rUhDAEc\\nJ0wu58HxmrxfRlq43I10wJUeJ7dalRvS+nEUZQPm8hNWvBV4jDTOrFEiPmYwL8Jd\\nE9qSA20a1HJRKLaN5z/nzFIH8r7N2HV7UaO4U3MEzOgZ2WBkgmvcuIOC4j4CXI9E\\nK1tp5FkjAgMBAAECggEAALuEfGjjRhLKsBmv4Q3JR9WymZCD/POat3vHE9mmxhwt\\nCyrHl2W/oKPS9HIq59HeX0cNoq1GZTK+5LxvqVfHqcROZzPL2Nf5vKoIRFQ9TqW6\\nfHxFqE9V3ZkOGZGK32edVff/u8cu84WwlPmGtN/XCvM6revHiqPG7uSD1vDV04H8\\nBDRjlW4WIOi2Be2e7LRdCNYc+GUZe7tlkWjMb/aEnzDqu61P4mvpDG+HB9fqgt/x\\nqTzALUV8ulGpqwC5nqLgbfwGv4odslKAatNDGuDIUvbgNJbwgD+F4iYTh+lD/1eE\\nEf3GLQA8GicqIfGEQnJYKErQ96QkJQeFzhFytp84nQKBgQD80vhstJWMEXcgkBJV\\n8wt1e9CB8+MSu+doxB32sahupnXVt1F+qngSAmeNBolUnbrO8Qwm/iDlZYqbXtid\\nAO5sd56zoSYGjNZXZte2NEKXRGewPrRtRNoUpgQGa8QaTw+bG0Ys2gbQfBalmwkS\\nNJGjg3YGgbRFrJCry5aIGc6O1QKBgQDpIhDRzJ/ekaNverPifXwOjsF/SBvIqgxI\\nxhcHM6IPgbwEAiVIbmoLSlmv2RDATwSdsid9A6W7SklnMH/d7rgZi+dEaBd4RS2J\\nQYaCvG7v7rxbl36ci/AnXNgJUsNSGXknGt03TEyCInm+/HtWNpDilDowUXfm766f\\n1Fti4al0FwKBgEbDKcMLPDwq+0PC707dKtzkHbTxtSGeNCPPmG9exDzMHHkHySiB\\nv/+Sarc/GBr11r5rGzKiuERcilH4VvqCszjqtiGivgjhTHDEGSYnNGgwPjFHK6rV\\ntZBH8yJ08TtH259XH+oX0AZ0ZMZPQVe2HPk+Jz+k9/b0WL2syuInDvhNAoGAS30p\\nXxXYKnSULFPaDZu+YOReyX0q6fYOFj52VHGmp0jF/bJIKkT+d3tywRsTboHnp7x5\\nRuS4dIXgcNZmgRF8cWE42JCwENStowUhrhGgBncSbMIIUF6Lu+M3XQV8k4gGD+CG\\nAxLkrzrEm1AcC9WNYqDGmsC2h4PS0pQrp0AU468CgYEAupV3zxOyTqc65QSup8Dk\\nvlnv3HXpnwju62UhNmsHoRhmzJ9cEswXwiwJErrDrPw9c4QQuoGdbsDKAZbE4c5z\\n66sp+FSUQlUnvwByrSBMiyae1ikPABVW86b682sOlkQRYx6nwg/VqRXdEehS/N4A\\nDaKXy9b6GmS2PjdIa/3cPJo=\\n-----END PRIVATE KEY-----\\n\",\n" +
            "  \"client_email\": \"firebase-adminsdk-c70fu@tallyfy-364a0.iam.gserviceaccount.com\",\n" +
            "  \"client_id\": \"106363787951928874143\",\n" +
            "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
            "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
            "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
            "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-c70fu%40tallyfy-364a0.iam.gserviceaccount.com\"\n" +
            "}\n";

    public static String getIpAdress() {
        return ipAdress;
    }

    public static void setIpAdress(String ipAdress) {
        AppParserUtil.ipAdress = ipAdress;
    }

    public static String getIpPort() {
        return ipPort;
    }

    public static void setIpPort(String ipPort) {
        AppParserUtil.ipPort = ipPort;
    }

    public static String getYear()
    {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDateTime now = LocalDateTime.now();
        return String.valueOf(dtf.format(now)).substring(0,String.valueOf(dtf.format(now)).indexOf('/'));
    }
    public static String getConvertToJson(String responseXml) {
        JSONObject obj = null;
        try {
            obj = XML.toJSONObject(responseXml);
            System.out.println("json" + obj.toString());
            //printJson(obj.toString());
        } catch (Exception e) {
            System.out.println(e);
        }
        return obj.toString();
    }

    public static String getPayLoad(String fromdate, String todate) {
        String payroll = "<ENVELOPE> <HEADER>    <VERSION>1</VERSION>    <TALLYREQUEST>Export</TALLYREQUEST>    <TYPE>Data</TYPE>    <ID>VoucherRegister</ID> </HEADER><BODY><DESC><STATICVARIABLES>           <SVEXPORTFORMAT>$$SysName:XML</SVEXPORTFORMAT><SVFROMDATE TYPE=\"Date\">" + fromdate + "</SVFROMDATE>   <SVTODATE TYPE=\"Date\">" + todate + "</SVTODATE>                 </STATICVARIABLES>     <TDL>       <TDLMESSAGE><REPORT NAME=\"VoucherRegister\" ISMODIFY=\"Yes\"><LOCAL>Collection:Default:Add:Filter:test</LOCAL><LOCAL>Collection:Default:Fetch:VoucherTypeName</LOCAL>               </REPORT><SYSTEM TYPE=\"Formulae\" NAME=\"test\" ISMODIFY=\"No\" ISFIXED=\"No\" ISINTERNAL=\"No\">$$IsPayment:$VoucherTypeName</SYSTEM>        </TDLMESSAGE>      </TDL></DESC></BODY></ENVELOPE>\r\n";

        return payroll;
    }
    public static String getResponse(String test){
         StringBuffer response = null;
        try {
            String url = "http://"+ipAdress+":9000";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type",
                    "application/xml;charset=utf-8");
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(test);
            wr.flush();
            wr.close();
            String responseStatus = con.getResponseMessage();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (IOException e) {
            System.out.println("error" + e.getMessage());
        }
        return response.toString();
    }

}
