package com.java.tallyzee.SignUP;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class Account extends Application {

    private double xOffset = 0;
    private double yOffset = 0;
    public void start(Stage primaryStage) throws IOException {
        initializeControls(primaryStage);
    }


    public void initializeControls(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("CreateAccount.fxml"));
        primaryStage.initStyle(StageStyle.TRANSPARENT);
      primaryStage.setResizable(false);
      Scene scene = new Scene(root,803,597);
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
        primaryStage.getIcons().add(new Image(getClass().getResource("Logo.png").toExternalForm()));
        primaryStage.setScene(scene);
        primaryStage.show();

    }


}
