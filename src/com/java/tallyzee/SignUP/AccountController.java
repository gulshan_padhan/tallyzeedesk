package com.java.tallyzee.SignUP;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.Window;
import com.java.tallyzee.LoginCntr.AlertHelper;
import com.java.tallyzee.LoginCntr.UserInfo;
import com.java.tallyzee.Network.ParserController;
import com.java.tallyzee.Network.ResponseType;
import com.java.tallyzee.StageLoader.AccountStage;
import com.java.tallyzee.StageLoader.LoginStage;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Timestamp;


public class AccountController implements SignupInterface {

    @FXML
    ImageView LogoDesign;

    @FXML
    Label Close;
    private Service<Void> bacgroundServices;

    @FXML
    Label Minimize;

    @FXML
    ImageView icon;

    @FXML
    private   Label logoHeader;

    @FXML
    TextField name;
    @FXML
    TextField email;
    @FXML
    TextField mobile;
    @FXML
    TextField password;
    @FXML
    TextField cnpassword;
    @FXML
    Button signUp;
    @FXML
    CheckBox agreement;

    @FXML
    AnchorPane anchorPane;

    @FXML
    ProgressIndicator progressIndicator;

    @FXML
    AnchorPane anPane;

    GaussianBlur blur;
    GaussianBlur blur1;

     String getName,getEmail,getMob,getPass,getCnPass,createdBy,updatedAt,createdAt,UserID;
    @FXML
    public void initialize() throws MalformedURLException {
        Font font = new Font("Roboto", 16);
        Image image2 = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\Logo.png").toURL().toString());
        LogoDesign.setImage(image2);
        blur = new GaussianBlur(10);
         blur1 = new GaussianBlur(0);
        //Progress Indicator
        progressIndicator.setVisible(false);
        //close Image
        Image closeImage = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\Close.png").toURL().toString());
        ImageView closeImageView = new ImageView(closeImage);
        closeImageView.setFitWidth(15);
        closeImageView.setFitHeight(15);
        Close.setGraphic(closeImageView);

        //Minimize Image
        Image minimizeImage = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\Minimize.png").toURL().toString());
        ImageView minimizeImageView = new ImageView(minimizeImage);
        minimizeImageView.setFitWidth(15);
        minimizeImageView.setFitHeight(15);
        Minimize.setGraphic(minimizeImageView);

        //Design Logo
        Image designImage = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\stageLogo.png").toURL().toString());
        LogoDesign.setImage(designImage);


        //topLogo
        Image logo_image = new Image(new File("C:\\TallyZeeAppliCationResource\\Resource\\stageLogo1089.png").toURL().toString());
        ImageView headerLogo = new ImageView(logo_image);
        headerLogo.setFitHeight(20);
        headerLogo.setFitWidth(20);
        logoHeader.setGraphic(headerLogo);
       // FirebaseUtil firebaseUtil = new FirebaseUtil();


    }

    @FXML
    public void HandleCloseStage(Event event) {
        ((Stage) ((Label) event.getSource()).getScene().getWindow()).close();
    }

    @FXML
    public void HandleMinimizeStage(Event event) {
        ((Stage) ((Label) event.getSource()).getScene().getWindow()).setIconified(true);
    }




    @FXML
    public void HandleSignUpButton(Event event) {

        Window owner = signUp.getScene().getWindow();
         getName = name.getText();
         getEmail = email.getText();
         getMob = mobile.getText();
         getPass = password.getText();
         getCnPass = cnpassword.getText();
        
         if (getName.isEmpty()) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner,
                    "Please Enter Your Name");

        }
        else if(getEmail.isEmpty() || !getEmail.contains("@"))
        {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner,
                    "Please Enter Valid Email");
        }

        else if(getMob.isEmpty() || getMob.length()<10)
        {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner,
                    "Please Enter Your Mobile Number");
        }

        else if(getPass.isEmpty())
        {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner,
                    "Please Enter Your Password");
        }

        else if(getCnPass.isEmpty())
        {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner,
                    "Please Enter Your Confirm Password");
        }

        else if(!getPass.equals(getCnPass))
        {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner,
                    "Password Does not Matches");
        }

        else if(!agreement.isSelected())
        {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner,
                "Please Accept Terms And Condition ");

        }
        else {


           try {

               anchorPane.setEffect(blur);
               progressIndicator.setVisible(true);
               String createdAt= String.valueOf(new Timestamp(System.currentTimeMillis()));
               String createdBy=getEmail;
               String updatedAt=String.valueOf(new Timestamp(System.currentTimeMillis()));
               String updatedBy=getEmail;
               String accountType="Trail";
               String suscriptionDate=String.valueOf(new Timestamp(System.currentTimeMillis()));
               //UserInfo(username, email, phone, password, confirmpassword, createdAt, createdBy, userID, updatedAt, updatedBy, accountType, suscriptionDate)
               FirebaseUtil firebaseUtil=new FirebaseUtil(this,new UserInfo(getName,getEmail,getMob,getPass,getCnPass, createdAt, createdBy, updatedAt, updatedBy, accountType, suscriptionDate));
              firebaseUtil.call();


//                   progressIndicator.setProgress(100);
//                   progressIndicator.setVisible(false);
//
//
//
//
//
//                   AlertHelper.showAlert(Alert.AlertType.CONFIRMATION,owner,"Error Creating Account");
//                   progressIndicator.setVisible(false);
//                   anchorPane.setEffect(blur1);



           }catch (Exception e)
           {

           }
        }



    }

    public void getAccountResponse() throws IOException {
    LoginStage loginStage =new LoginStage();
        try {
            loginStage.loadLoginStage();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Stage stage1 =  (Stage) anPane.getScene().getWindow();
        stage1.close();

    }


    @Override
    public void getSinupResponse(ResponseType responseCode, UserInfo responseProfile) {
        Window owner = signUp.getScene().getWindow();
        switch (responseCode)
        {
            case RESPONSEOK:
                System.out.println(responseCode);
                progressIndicator.setVisible(false);
                anchorPane.setEffect(blur1);
                Alert alert= AlertHelper.showCustomAlert(Alert.AlertType.CONFIRMATION,owner,"Congrats your account is created sucessfully ");
                final Button ok = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
                ok.addEventFilter(ActionEvent.ACTION, event ->{
                            try {
                                AccountStage.loadAccountStage();
//                                bacgroundServices=new Service<Void>() {
//                                    @Override
//                                    protected Task<Void> createTask() {
//                                        return new Task<Void>() {
//                                            @Override
//                                            protected Void call() throws Exception {
//                                                // System.out.println( "the company is"+NumberofCompanys.getCurrentCompany());
//                                                ParserController.getObjectSyncStart(String.valueOf(email.hashCode()));
//
//                                                return null;
//                                            }
//                                        };
//                                    }
//                                };
//
//                                bacgroundServices.setOnScheduled(new EventHandler<WorkerStateEvent>() {
//                                    @Override
//                                    public void handle(WorkerStateEvent event) {
//
//                                    }
//                                });
////signInButton.textProperty().bind(bacgroundServices.messageProperty());
//                                for(int i=0;i<5;i++)
//                                    bacgroundServices.restart();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            Stage stage1 =  (Stage) anPane.getScene().getWindow();
                    stage1.close();


                }
                );
                final Button cancle = (Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL);
                cancle.addEventFilter(ActionEvent.ACTION, event ->{Stage stage1 =  (Stage) anPane.getScene().getWindow();
                    stage1.close();}
                );
                alert.show();
                break;
            case RESPONSENOTFOUND:
                System.out.println(responseCode);
                progressIndicator.setVisible(false);
                anchorPane.setEffect(blur1);
                Alert alert1= AlertHelper.showCustomAlert(Alert.AlertType.WARNING,owner,"Sorry we face some tecnical issue ");
                final Button ok1 = (Button) alert1.getDialogPane().lookupButton(ButtonType.OK);
                ok1.addEventFilter(ActionEvent.ACTION, event ->
                        {Stage stage1 =  (Stage) anPane.getScene().getWindow();
                            stage1.close();}
                );
                alert1.show();
                break;
        }
    }
}