package com.java.tallyzee.SignUP;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import javafx.application.Platform;
import javafx.concurrent.Task;
import org.apache.log4j.BasicConfigurator;
import com.java.tallyzee.LoginCntr.UserInfo;
import com.java.tallyzee.Network.ResponseType;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

public class FirebaseUtil extends Task<String> {
    static boolean response;
    UserInfo userInfo;
SignupInterface signupInterface;
FirebaseUtil(AccountController accountController, UserInfo userInfo)
{
    this.userInfo=userInfo;
    signupInterface= accountController;
}
    public void getResponse(UserInfo userInfo) throws Exception {
        Platform.runLater(new Runnable() {
            @Override public void run() {
                try {
                    FileInputStream serviceAccount = new FileInputStream("C:\\Users\\dev9\\Desktop\\admin-sdk.json");
                    BasicConfigurator.configure();
                    // Initialize the app with a service account, granting admin privileges
                    FirebaseOptions options = new FirebaseOptions.Builder()
                            .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                            .setDatabaseUrl("https://tallyfy-364a0.firebaseio.com/")
                            .setServiceAccountId("firebase-adminsdk-c70fu@tallyfy-364a0.iam.gserviceaccount.com")
                            .build();
                    // As an admin, the app has access to read and write all data, regardless of Security Rules
                    DatabaseReference ref = FirebaseDatabase.getInstance()
                            .getReference("Users");
                    validateAccountCreation(userInfo, ref);
                }catch (Exception e)
                {

                }

            }
        });

    }

    public void validateAccountCreation(UserInfo userInfo, DatabaseReference ref) throws FirebaseAuthException, IOException {
        String d;
        String emailId = String.valueOf(userInfo.getEmail().hashCode());
        d = String.valueOf(Math.random());
        userInfo.setUserID(d.substring(2, 12));
        userInfo.setCreatedBy(userInfo.getName());
        Date date = new Date();
        userInfo.setCreatedAt(date.toString());
        if (ref.child(String.valueOf(emailId)).setValueAsync(userInfo).isDone()) {
            System.out.println(ref.child(emailId).setValueAsync(userInfo).isDone());
            System.out.println("Not Successful");
            signupInterface.getSinupResponse(ResponseType.RESPONSENOTFOUND,userInfo);
        } else {
            UserRecord.CreateRequest createRequest = new UserRecord.CreateRequest()
                    .setEmail(userInfo.getEmail())
                    .setDisplayName(userInfo.getName())
                    .setPassword(userInfo.getPassword());

            UserRecord userRecord = FirebaseAuth.getInstance().createUser(createRequest);
            signupInterface.getSinupResponse(ResponseType.RESPONSEOK,userInfo);

        }


    }

    @Override
    protected String call() throws Exception {
    getResponse(userInfo);
        return null;
    }
}
