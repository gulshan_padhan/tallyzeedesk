package com.java.tallyzee.SignUP;

import com.java.tallyzee.LoginCntr.UserInfo;
import com.java.tallyzee.Network.ResponseType;

public interface SignupInterface {
    void getSinupResponse(ResponseType responseType, UserInfo responseProfile);
}
